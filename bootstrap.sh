#!/bin/bash
echo "Environments ...."
#sh ./ip.sh
cat .env
echo ".........................."
CMD="${1:-start}"
MODULE="${2:-gateway}"
if [[ $CMD = "init" ]]; then
  echo "Initialize Feight application..."
  rm -rf cassandra-db	
  docker-compose -f system-modules.yml -f initial-data-setup.yml up -d --build --force-recreate cassandra elasticsearch zookeeper kafka usergrid usergrid-setup usergrid-portal
elif [[ $CMD = "core" ]]; then
  echo "Initialize Core Feight application..."
  docker-compose -f system-modules.yml -f initial-data-setup.yml up -d --build --force-recreate zookeeper kafka cassandra elasticsearch usergrid usergrid-setup
elif [[ $CMD = "cas" ]]; then
  echo "Initialize Core Feight application..."
  docker-compose -f system-modules.yml -f initial-data-setup.yml up -d --build --force-recreate cassandra
elif [[ $CMD = "ug" ]]; then
  echo "Initialize Core Feight application..."
  docker-compose -f system-modules.yml -f initial-data-setup.yml up -d --build --force-recreate usergrid
elif [[ $CMD = "es" ]]; then
  echo "Initialize Core Feight application..."
  docker-compose -f system-modules.yml -f initial-data-setup.yml up -d --build --force-recreate elasticsearch
elif [[ $CMD = "base" ]]; then
  echo "Initialize Core Feight application..."
  docker-compose -f system-modules.yml -f initial-data-setup.yml up -d --build --force-recreate cassandra usergrid
elif [[ $CMD = "start" ]]; then
  echo "Starting Feight application..."
  docker-compose -f initial-data-setup.yml -f application-modules.yml -f system-modules.yml up -d --build --force-recreate $MODULE
elif [[ $CMD = "module" ]]; then
  echo "Starting module application..."
  docker-compose -f application-modules.yml up -d --build --force-recreate $MODULE
elif [[ $CMD = "clean" ]]; then
  echo "Removing container ..."
  docker rm -f $(docker ps -aq)
elif [[ $CMD = "stop" ]]; then
  echo "Stop development containers..."
  docker-compose -f application-modules.yml stop apiSpec
fi
