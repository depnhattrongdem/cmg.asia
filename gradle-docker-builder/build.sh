#!/bin/bash

set -o pipefail
IFS=$'\n\t'

DOCKER_SOCKET=/var/run/docker.sock

if [ ! -e "${DOCKER_SOCKET}" ]; then
  echo "Docker socket missing at ${DOCKER_SOCKET}"
  exit 1
fi

mkdir -p /tmp/secrets
eval `ssh-agent -s` > /dev/null
FOLDERS=/var/run/secrets/openshift.io/build/*
for folder in $FOLDERS
do
  cp $folder/* /tmp/secrets/
  SECRET_FILES=/tmp/secrets/* 
  for f in $SECRET_FILES
  do
    chmod 600 $f > /dev/null
    ssh-add $f > /dev/null
  done
done

if [[ "${SOURCE_REPOSITORY}" != "ssh://"* ]] && [[ "${SOURCE_REPOSITORY}" != "git://"* ]] && [[ "${SOURCE_REPOSITORY}" != "git@"* ]]; then
  URL="${SOURCE_REPOSITORY}"
  if [[ "${URL}" != "http://"* ]] && [[ "${URL}" != "https://"* ]]; then
    URL="https://${URL}"
  fi
  curl --head --silent --fail --location --max-time 16 $URL > /dev/null
  if [ $? != 0 ]; then
    echo "Could not access source url: ${SOURCE_REPOSITORY} ${URL}"
    exit 1
  fi
fi

BUILD_DIR=$(mktemp --directory)
git clone --quiet --recursive "${SOURCE_REPOSITORY}" "${BUILD_DIR}" > /dev/null
if [ $? != 0 ]; then
  echo "Error trying to fetch git source: ${SOURCE_REPOSITORY}"
  exit 1
fi

if [ -n "${SOURCE_REF}" ]; then  
  pushd "${BUILD_DIR}"
  git checkout --quiet "${SOURCE_REF}" > /dev/null
  if [ $? != 0 ]; then
    echo "Error trying to checkout branch: ${SOURCE_REF}"
    exit 1
  fi
  popd 
fi

if [ -z "${GRADLE_BUILD_TASKS}" ]; then
  GRADLE_BUILD_TASKS='stage'
fi

cd ${BUILD_DIR}
echo "gradle ${GRADLE_BUILD_TASKS}" > build.sh
chmod +x build.sh
./build.sh