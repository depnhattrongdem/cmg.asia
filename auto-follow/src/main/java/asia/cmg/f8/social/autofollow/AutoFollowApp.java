package asia.cmg.f8.social.autofollow;

import asia.cmg.f8.common.util.F8Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created on 4/26/17.
 */
@Configuration
@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties
@EnableAsync
@SuppressWarnings("PMD")
public class AutoFollowApp {

    public static void main(final String[] args) {
        new F8Application(AutoFollowApp.class.getSimpleName()).with(new SpringApplicationBuilder(AutoFollowApp.class).web(true)).run(args);
    }
}
