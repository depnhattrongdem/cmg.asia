package asia.cmg.f8.social.autofollow.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created on 4/26/17.
 */
@Component
@ConfigurationProperties(value = "usergrid")
public class UserGridProperties {

    private String userGridClientId;
    private String userGridClientSecret;

    public String getUserGridClientId() {
        return userGridClientId;
    }

    public void setUserGridClientId(final String userGridClientId) {
        this.userGridClientId = userGridClientId;
    }

    public String getUserGridClientSecret() {
        return userGridClientSecret;
    }

    public void setUserGridClientSecret(final String userGridClientSecret) {
        this.userGridClientSecret = userGridClientSecret;
    }
}
