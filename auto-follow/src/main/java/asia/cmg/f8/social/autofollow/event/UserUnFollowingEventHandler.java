/**
 * 
 */
package asia.cmg.f8.social.autofollow.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.stereotype.Component;

import asia.cmg.f8.common.profile.UserUnFollowingConnectionEvent;
import asia.cmg.f8.social.autofollow.service.ReFollowService;

/**
 * @author khoa.bui
 *
 */
@Component
@EnableBinding(UserUnFollowingEventStream.class)
public class UserUnFollowingEventHandler {

	public static final Logger LOGGER = LoggerFactory.getLogger(UserUnFollowingEventHandler.class);

    private final MessageConverter messageConverter;
    private final ReFollowService reFollowService;

    public UserUnFollowingEventHandler(@Qualifier("userUnFollowingEventConverter") final MessageConverter messageConverter, final ReFollowService reFollowService) {
        this.messageConverter = messageConverter;
        this.reFollowService = reFollowService;
    }

    @StreamListener(UserUnFollowingEventStream.USER_UN_FOLLOWING_INPUT)
    public void onEvent(final Message<?> message) {

        final UserUnFollowingConnectionEvent event = (UserUnFollowingConnectionEvent) messageConverter.fromMessage(message, UserUnFollowingConnectionEvent.class);
        if (event != null) {
        	
            final String userUuid = String.valueOf(event.getUserId());
            final String followerUuid = String.valueOf(event.getFollowerId());
            LOGGER.info("Processing re-follow for userId {}, follower {}", userUuid, followerUuid);
            reFollowService.connect(userUuid, followerUuid);
            
        }
    }
}
