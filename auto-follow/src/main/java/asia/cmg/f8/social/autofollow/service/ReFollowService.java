/**
 * 
 */
package asia.cmg.f8.social.autofollow.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import asia.cmg.f8.social.autofollow.client.FollowUserClient;
import asia.cmg.f8.social.autofollow.entity.PagedResponse;

/**
 * @author khoa.bui
 *
 */
@Service
public class ReFollowService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReFollowService.class);

	private static final int DEFAULT_LIMIT = 1_000;

	// maintain list of user identities in application
	private final ExecutorService executor = Executors.newFixedThreadPool(5);

	private final FollowUserClient followUserClient;

	public ReFollowService(final FollowUserClient followUserClient) {
		this.followUserClient = followUserClient;
	}

	@Async
	public void connect(final String userUuid, final String followerUuid) {
		executor.submit(new StoreFollowingUuidThread(userUuid, followerUuid));
	}

	class StoreFollowingUuidThread implements Runnable {
		private final String uuid;
		private final String followerUuid;
		private final String fileName;
		private FileWriter fileWriter;
		private FileReader fileReader;
		private BufferedReader bufferedReader;
		private BufferedWriter bufferedWriter;
		
		public StoreFollowingUuidThread(final String uuid, final String followerUuid) {
			this.uuid = uuid;
			this.followerUuid = followerUuid;
			fileName = "/opt/" + uuid + "-" + followerUuid + System.currentTimeMillis() + ".txt";

		}

		@Override
		public void run() {
			
			
			try {
				final File file = new File(fileName);
				file.createNewFile();
				fileWriter = new FileWriter(file);
				//Open buffer writer and reader
				fileWriter = new FileWriter(fileName);
				bufferedWriter = new BufferedWriter(fileWriter);
				fileReader = new FileReader(fileName);
				bufferedReader = new BufferedReader(fileReader);

				//Save all the current following users to txt file
				saveToLocalFollowing(bufferedWriter);

				//Close buffer writer
				closeBufferWriter(fileWriter, bufferedWriter);


				// Delete following Connection
				Boolean deleteFollowingStatus = followUserClient.deleteFollowingConnection(uuid, followerUuid).getContent();
				if(deleteFollowingStatus) {
					
					LOGGER.info("Deleted successfully user {} followed {}", uuid, followerUuid);
					// doing Re-Auto follow
					doingReFollowing(bufferedReader);

					//Close buffer reader
					closeBufferReader(fileReader, bufferedReader);
				}
				else {
					LOGGER.info("Deleted failed user {} followed {}", uuid, followerUuid);
				}
				
				//Remove the txt file
				Files.deleteIfExists(Paths.get(fileName));

			} catch (Exception ex) {
				LOGGER.error(ex.getMessage(), ex);
			} finally {
				try {
					closeBufferWriter(fileWriter, bufferedWriter);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					LOGGER.error(e.getMessage(), e);
				}
				try {
					closeBufferReader(fileReader, bufferedReader);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					LOGGER.error(e.getMessage(), e);
				}
			}

		}

		public void closeBufferWriter(final FileWriter fileWriter, final BufferedWriter bufferedWriter)
				throws IOException {
			if (null != bufferedWriter) {
				bufferedWriter.close();
			}
			if (null != fileWriter) {
				fileWriter.close();
			}
		}

		public void closeBufferReader(final FileReader fileReader, final BufferedReader bufferedReader)
				throws IOException {
			if (null != bufferedReader) {
				bufferedReader.close();
			}
			if (null != fileReader) {
				fileReader.close();
			}
		}

		public void doingReFollowing(final BufferedReader bufferedReader) throws IOException {
			String followerUuidLine;

			while ((followerUuidLine = bufferedReader.readLine()) != null) {
				// Re-following all users except for the user has been unfollow
				// re-create the following
				followUserClient.createFollowingConnection(uuid, followerUuidLine);
				LOGGER.info("Creating user {} followed {}", uuid, followerUuidLine);

				// this will wait until the result come then process
				// the next item.
				// don't stress the user-grid so sleep a little bit??
				try {
					TimeUnit.MILLISECONDS.sleep(200L);
				} catch (final InterruptedException exception) {
					LOGGER.error(exception.getMessage(), exception);
				}
			}
		}

		public void saveToLocalFollowing(final BufferedWriter bufferedWriter) throws IOException {
			PagedResponse<String> response = followUserClient.getFollowingConnection(uuid, DEFAULT_LIMIT, null);
			if (null != response && null != response.getEntities()) {
				for (final String followerId : response.getEntities()) {
					if(!followerUuid.equals(followerId)) {
						LOGGER.info("Save user uuid {} to txt file. Removed Followerid {}", followerId, followerUuid);
						bufferedWriter.write(followerId);
						bufferedWriter.newLine();
					}

				}
			}
		}

	}
}
