package asia.cmg.f8.social.autofollow.client;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import asia.cmg.f8.social.autofollow.entity.PagedResponse;

/**
 * Created on 4/26/17.
 */
@FeignClient(value = "users", url = "${feign.url}")
public interface UserClient {
	String LIMIT = "limit";

    String SECRET_QUERY = "client_id=${usergrid.userGridClientId}&client_secret=${usergrid.userGridClientSecret}";

    @RequestMapping(value = "/users?ql={query}&limit={limit}&cursor={cursor}&" + SECRET_QUERY, method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    PagedResponse<UserInfo> getUserByQueryWithCursor(@PathVariable("query") final String query, @PathVariable(LIMIT) final int limit, @PathVariable("cursor") final String cursor);
    
    @RequestMapping(value = "/users?ql={query}&limit={limit}&" + SECRET_QUERY, method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    PagedResponse<UserInfo> getUserByQuery(@PathVariable("query") final String query, @PathVariable(LIMIT) final int limit);

}
