/**
 * 
 */
package asia.cmg.f8.social.autofollow.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author khoa.bui
 *
 */
public interface UserUnFollowingEventStream {

	String USER_UN_FOLLOWING_INPUT = "userUnFollowingInput";

    @Input(USER_UN_FOLLOWING_INPUT)
    SubscribableChannel handleUserUnFollowingEventInput();
}
