package asia.cmg.f8.social.autofollow.event;

import asia.cmg.f8.common.message.AvRoMessageConverterLoader;
import asia.cmg.f8.common.message.EnableMessage;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.stereotype.Component;

/**
 * Created on 4/26/17.
 */
@Component
@EnableMessage
public class MessageConverterConfiguration {

    private final AvRoMessageConverterLoader messageConverterLoader;

    public MessageConverterConfiguration(final AvRoMessageConverterLoader messageConverterLoader) {
        this.messageConverterLoader = messageConverterLoader;
    }

    @Bean(name = "userActivatedEventConverter")
    public MessageConverter userActivatedEventConverter() {
        return messageConverterLoader.load("UserActivationEvent.avsc").orElseThrow(() -> new IllegalStateException("Failed to load UserActivationEvent.avsc"));
    }
    
    @Bean(name = "userUnFollowingEventConverter")
    public MessageConverter userUnFollowingEventConverter() {
        return messageConverterLoader.load("UserUnFollowingConnectionEvent.avsc").orElseThrow(() -> new IllegalStateException("Failed to load UserUnFollowingConnectionEvent.avsc"));
    }
}
