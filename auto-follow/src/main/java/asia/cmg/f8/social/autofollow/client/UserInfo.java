package asia.cmg.f8.social.autofollow.client;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created on 4/26/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo {

    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }
}
