package asia.cmg.f8.social.autofollow.config;

import asia.cmg.f8.social.autofollow.properties.FeignProperties;
import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created on 4/26/17.
 */
@Configuration
public class FeignConfig {

    private final FeignProperties feignProperties;

    public FeignConfig(final FeignProperties feignProperties) {
        this.feignProperties = feignProperties;
    }

    @Bean
    public Request.Options requestOptions() {
        return new Request.Options(feignProperties.getConnectTimeoutMillis(), feignProperties.getReadTimeoutMillis());
    }
}
