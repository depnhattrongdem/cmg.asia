package asia.cmg.f8.social.autofollow.event;

import asia.cmg.f8.common.user.UserActivationEvent;
import asia.cmg.f8.social.autofollow.service.AutoFollowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.stereotype.Component;

/**
 * Created on 4/26/17.
 */
@Component
@EnableBinding(UserActivationEventStream.class)
public class UserActivationEventHandler {

    public static final Logger LOGGER = LoggerFactory.getLogger(UserActivationEventHandler.class);

    private final MessageConverter messageConverter;
    private final AutoFollowService autoFollowService;

    public UserActivationEventHandler(@Qualifier("userActivatedEventConverter") final MessageConverter messageConverter, final AutoFollowService autoFollowService) {
        this.messageConverter = messageConverter;
        this.autoFollowService = autoFollowService;
    }

    @StreamListener(UserActivationEventStream.USER_ACTIVATED_INPUT)
    public void onEvent(final Message<?> message) {

        final UserActivationEvent event = (UserActivationEvent) messageConverter.fromMessage(message, UserActivationEvent.class);
        if (event != null) {

            final String uuid = String.valueOf(event.getUuid());
            autoFollowService.connect(uuid);

            LOGGER.info("Processing auto-follow for user {}", uuid);
        }
    }
}
