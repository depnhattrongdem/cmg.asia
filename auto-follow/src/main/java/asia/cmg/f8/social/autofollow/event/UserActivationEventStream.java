package asia.cmg.f8.social.autofollow.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created on 4/26/17.
 */
public interface UserActivationEventStream {

    String USER_ACTIVATED_INPUT = "userActivatedInput";

    @Input(USER_ACTIVATED_INPUT)
    SubscribableChannel handleUserActivatedEventInput();
}
