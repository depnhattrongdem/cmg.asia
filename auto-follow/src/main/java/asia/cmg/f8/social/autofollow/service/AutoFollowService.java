package asia.cmg.f8.social.autofollow.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import asia.cmg.f8.social.autofollow.client.FollowUserClient;
import asia.cmg.f8.social.autofollow.client.UserClient;
import asia.cmg.f8.social.autofollow.client.UserInfo;
import asia.cmg.f8.social.autofollow.entity.PagedResponse;

/**
 * Created on 4/26/17.
 */
@Service
public class AutoFollowService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoFollowService.class);

	private static final int DEFAULT_LIMIT = 1_000;


	private final UserClient userClient;
	private final FollowUserClient followUserClient;

	public AutoFollowService(final UserClient userClient,
                             final FollowUserClient followUserClient) {
		this.userClient = userClient;
		this.followUserClient = followUserClient;
	}

	@Async
	public void connect(final String userUuid) {
		PagedResponse<UserInfo> response = userClient.getUserByQuery("SELECT uuid WHERE activated=true AND level='Diamond'",
				DEFAULT_LIMIT);
		if (null != response && null != response.getEntities()) {
			for (final UserInfo item : response.getEntities()) {
				final String uuid = item.getUuid();
				followUserClient.createFollowingConnection(userUuid, uuid);
				LOGGER.info("Creating user {} followed {}", userUuid, uuid);
			}
		}
	}
}
