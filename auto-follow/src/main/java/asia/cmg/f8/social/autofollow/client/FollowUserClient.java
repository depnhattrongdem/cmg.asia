package asia.cmg.f8.social.autofollow.client;

import asia.cmg.f8.social.autofollow.entity.PagedResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@FeignClient(value = "followers", url = "${feign.socialUrl}", path = "/social/follow", decode404 = true)
@RequestMapping(produces = APPLICATION_JSON_UTF8_VALUE)
public interface FollowUserClient {

    @RequestMapping(value = "/users/{followingUser}/following/users/{followedUser}?", method = POST)
    void createFollowingConnection(@PathVariable("followingUser") String followingUser,
                                   @PathVariable("followedUser") String followedUser);

    @RequestMapping(value = "/users/{followingUser}/following/users/{followedUser}", method = DELETE)
    Resource<Boolean> deleteFollowingConnection(@PathVariable("followingUser") String followingUser,
                                                @PathVariable("followedUser") String followedUser);

    @RequestMapping(value = "/users/{userId}/following/users?limit={limit}&cursor={cursor}", method = GET)
    PagedResponse<String> getFollowingConnection(@PathVariable("userId") String followedUserId,
                                                 @RequestParam(value = "limit") int pageSize,
                                                 @RequestParam(value = "cursor") String cursor);
}
