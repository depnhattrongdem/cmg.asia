package asia.cmg.f8.user.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import asia.cmg.f8.common.dto.ApiRespObject;
import asia.cmg.f8.common.email.EmailSender;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.user.client.MailgunClient;
import asia.cmg.f8.user.client.UserClient;
import asia.cmg.f8.user.dto.ResetUserPassword;
import asia.cmg.f8.user.entity.AccountTokenEntity;
import asia.cmg.f8.user.entity.BasicUserEntity;
import asia.cmg.f8.user.entity.UserEntity;
import asia.cmg.f8.user.repository.BasicUserRepository;
import asia.cmg.f8.user.service.AccountTokenService;
import asia.cmg.f8.user.service.JwtService;
import asia.cmg.f8.user.service.UserService;

@RestController
public class ResetPassword {

	private static final String SUCCESS = "success";
	public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
	private static final Logger LOG = LoggerFactory.getLogger(ResetPassword.class);
	private static final String SENDING_EMAIL_FROM = "Leep.app <lisa@leep.app>";
	private static final String MAILGUN_URL = "https://api.mailgun.net/v3/mg.leep.app/messages";

	@Autowired
	private AccountTokenService accountTokenService;

	@Autowired
	private BasicUserRepository userRepo;

	@Autowired
	private UserClient userClient;

	@Value("${jwt.access_expiresIn}")
	private int accessExpiresIn;

	@Autowired
	private JwtService jwtService;

	@Autowired
	private MailgunClient mailgunClient;
	@Autowired
	private UserService userService;

	@Value("${mailgun.user}")
	private String MAILGUN_USER;
	@Value("${mailgun.secret_key}")
	private String MAILGUN_SECRET_KEY;

	@Value("${mail_template.forget_password_vi}")
	private String EMAIL_TEMPLATE_VI;

	@Value("${mail_template.forget_password_en}")
	private String EMAIL_TEMPLATE_EN;

	@RequestMapping(value = "/users/v1/reset-password", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<?> resetPassword(@RequestParam(value = "email", required = true) final String email,
			@RequestParam(value = "lang", required = false, defaultValue = "en") String lang) {
		ApiRespObject<Object> apiResponse = new ApiRespObject<Object>();
		apiResponse.setStatus(ErrorCode.SUCCESS);

		// check email exist in DB
		Optional<BasicUserEntity> userOptional = userRepo.findByEmail(email);
		if (!userOptional.isPresent()) {
			LOG.warn("[Reset password]: Email {} is not exist in system", email);
			return new ResponseEntity<>(
					ErrorCode.USER_NOT_EXIST.withError("EMAIL_NOT_EXIST", "User is not exist in DB"),
					HttpStatus.BAD_REQUEST);
		}
		// check email exist in usergrid
		UserGridResponse<UserEntity> response = userClient.getUserByQuery("where email = '" + email + "'");
		if (response == null || response.getEntities() == null || response.getEntities().isEmpty()) {
			LOG.error("[Reset password]: Email {} is not exist in usergrid", email);
			return new ResponseEntity<>(
					ErrorCode.USER_NOT_EXIST.withError("EMAIL_NOT_EXIST", "User is not exist in Usergrid"),
					HttpStatus.BAD_REQUEST);
		}

		// get dto AccountTokenEntity
		BasicUserEntity user = userOptional.get();
		AccountTokenEntity token = accountTokenService.findByUUID(user.getUuid());
		if (token == null) {
			token = new AccountTokenEntity();
			token.setUuid(user.getUuid());
		}

		// Generate new jwt key
		long resetPasswordExpired = System.currentTimeMillis() + accessExpiresIn;
		String resetPasswordToken = jwtService.generateToken(user.getUuid(), ACCESS_TOKEN);
		token.setResetPasswordToken(resetPasswordToken);
		token.setResetPasswordExpired(resetPasswordExpired);
		accountTokenService.saveToken(token);
		try {
			String EMAIL_TEMPLATE = (lang.equalsIgnoreCase("vi")) ? EMAIL_TEMPLATE_VI : EMAIL_TEMPLATE_EN;
			byte[] decodedBytes = Base64.getDecoder().decode(EMAIL_TEMPLATE.getBytes("UTF-8"));
			String emailTemplate = new String(decodedBytes, "UTF-8");
			emailTemplate = emailTemplate.replace("%%name%%", user.getFullName());
			emailTemplate = emailTemplate.replace("%%url%%",
					"https://leep.app/reset-password?token=" + resetPasswordToken);
			EmailSender emailSend = new EmailSender();
			Boolean result = emailSend.sendEmail(SENDING_EMAIL_FROM, email, "Reset password", emailTemplate);
			if (!result) {
				return new ResponseEntity<>(new ErrorCode(4001, "FAIL_SEND_EMAIL", "Failed")
						.withError("FAIL_SEND_EMAIL", "FAIL_SEND_EMAIL"), HttpStatus.BAD_REQUEST);
			}
		} catch (UnsupportedEncodingException e) {
			LOG.error("Can not sending email to {}. Message: {}", email, e.getMessage());
			return new ResponseEntity<>(new ErrorCode(4001, "FAIL_SEND_EMAIL", "Failed")
					.withError("FAIL_SEND_EMAIL", "FAIL_SEND_EMAIL"), HttpStatus.BAD_REQUEST);
		}

		apiResponse.setData(Boolean.TRUE);
		return new ResponseEntity<>(apiResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/users/v1/reset-password", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateResetPassword(@RequestBody final Map<String, String> dto) {
		// check token valid
		String token = dto.get("token");
		String password = dto.get("password");
		AccountTokenEntity tokenDto = accountTokenService.findByResetPasswordToken(token);

		if (tokenDto == null) {
			LOG.warn("[Reset password]: Token reset password invalid {}", token);
			return new ResponseEntity<>(
					new ErrorCode(4011, "TOKEN_INVALID", "Failed").withError("TOKEN_INVALID", "Token invalid"),
					HttpStatus.BAD_REQUEST);
		}
		if (tokenDto.getResetPasswordExpired() <= System.currentTimeMillis()) {
			LOG.warn("[Reset password]: Token reset password expire {}", token);
			return new ResponseEntity<>(
					new ErrorCode(4012, "TOKEN_EXPIRE", "Failed").withError("TOKEN_EXPIRE", "Token expire"),
					HttpStatus.BAD_REQUEST);
		}
		ResetUserPassword passwordBody = new ResetUserPassword();
		passwordBody.setNewPassword(password);
		if (!userService.resetUserPassword(tokenDto.getUuid(), passwordBody)) {
			LOG.warn("Can not change password");
			return new ResponseEntity<>(
					ErrorCode.REQUEST_INVALID.withError("INVALID_CHANGE_PASSWORD", "Can not change password"),
					HttpStatus.BAD_REQUEST);
		}
		tokenDto.setResetPasswordToken(null);
		tokenDto.setResetPasswordExpired(null);
		accountTokenService.saveToken(tokenDto);
		return new ResponseEntity<>(Collections.singletonMap(SUCCESS, Boolean.TRUE), HttpStatus.OK);
	}

}
