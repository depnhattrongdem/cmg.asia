package asia.cmg.f8.upload.service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.upload.FileUploadApp;
import asia.cmg.f8.upload.dto.PreSignedRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created on 11/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = FileUploadApp.class)
public class AwsS3ServiceITest {

    @Inject
    private AwsS3Service awsS3Service;

    @Test
    public void testGeneratePreSignedUrl() throws IOException {

        // setup mock account
        final Account account = new Account() {
            @Override
            public String uuid() {
                return UUID.randomUUID().toString();
            }

            @Override
            public String ugAccessToken() {
                return null;
            }

            @Override
            public String type() {
                return null;
            }

            @Override
            public String language() {
                return null;
            }

            @Override
            public boolean isAdmin() {
                return false;
            }
        };

        final String fileName = "image_upload_test.jpg";
        assertThat(awsS3Service).isNotNull();

        // when
        final ResponseEntity<?> entity = awsS3Service.generatePreSignedUrl(fileName, "image/png", account);

        final Object body = entity.getBody();

        // then
        assertThat(body).isNotNull();
        assertThat(body).isInstanceOf(PreSignedRequest.class);
        final URL uploadUrl = ((PreSignedRequest) body).getUrl();
        assertThat(uploadUrl).isNotNull();


        final Response response = tryUpload(uploadUrl);

        assertThat(response.code).isEqualTo(200);
    }

    private Response tryUpload(final URL url) throws IOException {
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");

        try (final InputStream inputStream = AwsS3ServiceITest.class.getResourceAsStream("/image_upload_test.jpg")) {

            final byte[] buffer = new byte[1024];
            int count;
            try (final OutputStream out = connection.getOutputStream()) {
                do {
                    count = inputStream.read(buffer);
                    if (count > 0) {
                        out.write(buffer, 0, count);
                    }
                } while (count > 0);
            }
        }

        final int responseCode = connection.getResponseCode();
        final String message = connection.getResponseMessage();

        return new Response(responseCode, message);
    }

    static class Response {
        private final int code;
        private final String message;

        public Response(final int code, final String message) {
            this.code = code;
            this.message = message;
        }
    }
}
