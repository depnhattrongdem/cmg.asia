package asia.cmg.f8.upload.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created on 11/7/16.
 */
@Component
@ConfigurationProperties(prefix = "aws", ignoreUnknownFields = false, ignoreInvalidFields = false)
public class AwsS3Properties {

    private String bucketName;
    private String clientId;
    private String clientSecret;
    private String publicDomainName;
    private Long preSignedUrlExpiredTime;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(final String bucketName) {
        this.bucketName = bucketName;
    }

    public Long getPreSignedUrlExpiredTime() {
        return preSignedUrlExpiredTime;
    }

    public void setPreSignedUrlExpiredTime(final Long preSignedUrlExpiredTime) {
        this.preSignedUrlExpiredTime = preSignedUrlExpiredTime;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(final String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getPublicDomainName() {
        return publicDomainName;
    }

    public void setPublicDomainName(final String publicDomainName) {
        this.publicDomainName = publicDomainName;
    }
}
