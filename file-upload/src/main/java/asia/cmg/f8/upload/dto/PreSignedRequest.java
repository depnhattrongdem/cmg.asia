package asia.cmg.f8.upload.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URL;

/**
 * Created on 11/7/16.
 */
public class PreSignedRequest {

    private String accessUrl;

    @JsonIgnore
    private URL url;

    public PreSignedRequest(final String accessUrl, final URL url) {
        this.accessUrl = accessUrl;
        this.url = url;
    }

    @JsonProperty("access_url")
    public String getAccessUrl() {
        return accessUrl;
    }

    public void setAccessUrl(final String accessUrl) {
        this.accessUrl = accessUrl;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    @JsonProperty("upload_url")
    public String uploadUrl() {
        return url.toExternalForm();
    }
}
