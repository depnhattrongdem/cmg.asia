package asia.cmg.f8.upload.service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.upload.config.AwsS3Properties;
import asia.cmg.f8.upload.dto.ErrorResponse;
import asia.cmg.f8.upload.dto.PreSignedRequest;
import asia.cmg.f8.upload.util.AwsUtil;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URL;
import java.util.Date;

import static asia.cmg.f8.common.exception.ApplicationException.inValid;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created on 11/7/16.
 */
@RestController
public class AwsS3Service implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwsS3Service.class);

    private final AwsS3Properties awsS3Properties;
    private AmazonS3Client s3Client;

    public AwsS3Service(final AwsS3Properties awsS3Properties) {
        this.awsS3Properties = awsS3Properties;
    }

    /**
     * Generate pre-signed url to upload files to S3.
     *
     * @return pre-signed url.
     */
    @RequestMapping(value = "/files/uploadUrl", method = POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> generatePreSignedUrl(@RequestParam("file_name") final String fileName, @RequestParam("content_type") final String contentType, final Account account) {
        try {
            // create unique name
            final String objectName = AwsUtil.createUniqueName(account, fileName);

            // create expired date
            final long expiredTime = awsS3Properties.getPreSignedUrlExpiredTime();
            final Date expiredDate = AwsUtil.calExpiredDate(expiredTime);

            // create sign request
            final GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(awsS3Properties.getBucketName(), objectName);
            request.setMethod(HttpMethod.PUT);
            request.setExpiration(expiredDate);
            request.setContentType(contentType);

            LOGGER.info("Create signed request: content type {}, file name {}", contentType, fileName);

            // generate signed url
            final URL url = s3Client.generatePresignedUrl(request);

            // create access url
            final String accessUrl = awsS3Properties.getPublicDomainName() + '/' + objectName;

            // create pre-signed request object
            final PreSignedRequest signedRequest = new PreSignedRequest(accessUrl, url);

            return new ResponseEntity<>(signedRequest, HttpStatus.OK);

        } catch (final AmazonServiceException exception) {
            LOGGER.error("Error on create S3 re-signed url", exception);
            final ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrorCode(exception.getErrorCode());
            errorResponse.setStatusCode(exception.getStatusCode());
            errorResponse.setMessage(exception.getMessage());
            return new ResponseEntity<Object>(errorResponse, HttpStatus.BAD_REQUEST);
        } catch (final AmazonClientException exception) {
            LOGGER.error("Error on create S3 re-signed url", exception);
            final ErrorResponse response = new ErrorResponse();
            response.setMessage(exception.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @SuppressWarnings("PMD")
    public void afterPropertiesSet() throws Exception {

        final String clientId = awsS3Properties.getClientId();
        final String clientSecret = awsS3Properties.getClientSecret();

        final boolean valid = awsS3Properties.getBucketName() != null
                && clientId != null
                && clientSecret != null
                && awsS3Properties.getPreSignedUrlExpiredTime() != null
                && awsS3Properties.getPublicDomainName() != null;

        if (!valid) {
            throw inValid("Invalid AWS S3 configuration. Failed to create bean");
        }

        final BasicAWSCredentials credential = new BasicAWSCredentials(clientId, clientSecret);
        s3Client = new AmazonS3Client(credential);
    }
}
