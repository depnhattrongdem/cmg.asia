package asia.cmg.f8.upload.metric;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/**
 * Created on 2/13/17.
 */
@RestController
public class HealthApi {

    @GetMapping(path = "/health")
    public Map<String, Object> health() {
        return Collections.singletonMap("status", "UP");
    }
}
