package asia.cmg.f8.upload.config;

import asia.cmg.f8.common.security.annotation.EnableClientSecurity;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import static org.springframework.http.HttpMethod.GET;

/**
 * Created on 11/8/16.
 */
@Configuration
@EnableClientSecurity
public class SecurityConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    @SuppressWarnings("PMD")
    public void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(GET, "/health").permitAll()
                .anyRequest().authenticated();
    }
}
