package asia.cmg.f8.upload;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * Created on 9/28/16.
 */
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@EnableConfigurationProperties
@SuppressWarnings("PMD")
public class FileUploadApp {

    public static void main(final String[] args) {
        new SpringApplicationBuilder(FileUploadApp.class).web(true).run(args);
    }
}

