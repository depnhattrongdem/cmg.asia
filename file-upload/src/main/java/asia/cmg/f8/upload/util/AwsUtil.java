package asia.cmg.f8.upload.util;

import asia.cmg.f8.common.security.Account;

import java.util.Date;
import java.util.UUID;

import org.springframework.util.StringUtils;

/**
 * Created on 11/7/16.
 */
public final class AwsUtil {

    private AwsUtil() {

    }

    public static Date calExpiredDate(final long amount) {
        final java.util.Date expiration = new java.util.Date();
        long milliSeconds = expiration.getTime();
        milliSeconds += amount;
        expiration.setTime(milliSeconds);
        return expiration;
    }

    /**
     * Until method to create an unique file name when upload on to S3.
     *
     * @param fileName the current file name
     * @return unique name.
     */
    public static String createUniqueName(final Account account, final String fileName) {
        final String random = UUID.randomUUID().toString(); // to handle caching on CDN
        final String removedWhitespacee = StringUtils.isEmpty(fileName) ? "" : "-" + fileName.replaceAll("\\s+", "");
        return account.uuid() + '/' + random + removedWhitespacee;
    }
}
