# File upload
The project support to upload files to AWS S3.

## Integration flow
The client call to **/files/uploadUrl/{file_name}** to create new upload url. The response includes 2 urls. One (upload_url) is used to upload file to S3 and one (access_url) is used by end client.
 
# Configuration variable

| Name  | Note |
|---|---| ----- |
| AWS_CLIENT_ID  | AWS client id |
| AWS_CLIENT_SECRET | AWS client secret |
| AWS_S3_BUCKET_NAME | S3 bucket name |
| PRE_SIGNED_URL_EXPIRED_TIME | expired time of pre-signed url |
| PUBLIC_DOMAIN_NAME | Public domain name which is used to build access url of uploaded file |
