package asia.cmg.f8.notification.service.email;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import asia.cmg.f8.common.email.EmailSender;
import asia.cmg.f8.common.message.SendingEmailEvent;

@Service
public class Sender {
	private SpringTemplateEngine templateEngine;
	private MessageSource messageSource;
	private EmailSender emailSender;
	
	@Autowired
	public Sender(SpringTemplateEngine templateEngine, MessageSource messageSource) {
		super();
		this.templateEngine = templateEngine;
		this.messageSource = messageSource;
		this.emailSender = new EmailSender();
	}

	public void send(String from, List<String> recipients, String templateName, String language, Map<String, String> params) {
		Locale locale = new Locale(language);
		final Context context = new Context();
		context.setVariables(params);
		context.setLocale(locale);
		String template = templateName + language.toUpperCase();
		final String content = templateEngine.process(template, context);
		
		String titleKey = "email." + templateName + ".title";
	    final String subject = messageSource.getMessage(titleKey, null, context.getLocale());
	    emailSender.sendEmail(from, recipients, subject, content);
	}
	
	public void send(SendingEmailEvent event) {
		List<String> recipients = event.getRecipients().stream().map(recipient -> recipient.toString()).collect(Collectors.toList());
		Map<String, String> params = new HashedMap();
		event.getParams().forEach(param -> {
			params.put(param.getOption().toString(), param.getValue().toString());
		});
		send(event.getFrom().toString(), recipients, event.getTemplateName().toString(),
				event.getLanguage().toString(), params);
	}
}
