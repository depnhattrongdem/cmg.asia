package asia.cmg.f8.notification.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.schema.avro.AvroSchemaMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import asia.cmg.f8.common.message.SendingEmailEvent;
import asia.cmg.f8.notification.service.email.Sender;

@Component
@EnableBinding(EmailEventStream.class)
public class SendingEmailEventConsumer {

	@Autowired
    private AvroSchemaMessageConverter sendingEmailEventConverter;
	
	@Autowired
	private Sender emailSender;
	
	@StreamListener(EmailEventStream.SENDING_EMAIL_TOPIC)
	public void onEvent(@SuppressWarnings("rawtypes") final Message message) {
		SendingEmailEvent emailEvent = (SendingEmailEvent) 
				sendingEmailEventConverter.fromMessage(message, SendingEmailEvent.class);
		emailSender.send(emailEvent);
	}
}
