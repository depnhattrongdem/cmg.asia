#!/bin/bash
SPEC_FILE=/usr/share/nginx/html/api-specification.yaml

sed -i "s/version: 1.0/version: ${API_VERSION}/g" $SPEC_FILE
sed -i "s/host: localhost:8080/host: ${API_HOST}/g" $SPEC_FILE
sed -i "s/basePath: \//basePath: ${API_BASE_PATH}/g" $SPEC_FILE

echo "Swagger UI is listening on port 8080"
nginx -c /usr/share/nginx/nginx.conf -g 'daemon off;'