# API Specification
Manage all of API specification of F8 projects.

The specification file is located at **api-specification.yaml**
# How to build

```
$ docker build --tag f8/api-specification .
```

# How to run
```
$ docker run -it --name "api-specification" -e API_VERSION=1.0 -e API_HOST="localhost:8080" -e API_BASE_PATH="\/" -p 8080:8080 f8/api-specification
```

To verify, open your browser to [http://localhost:8080](http://localhost:8080)

