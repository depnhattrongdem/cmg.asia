package asia.cmg.f8.marketing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.marketing.client.MarketingUserClient;
import asia.cmg.f8.marketing.dto.MarketingUserDetail;

@Service
public class MarketingUserService {

	@Autowired
	private MarketingUserClient userClient;
	
	public MarketingUserDetail createUser(MarketingUserDetail userDetail) {
		try {
			UserGridResponse<MarketingUserDetail> newUser =  userClient.create(userDetail);
			return newUser.getEntities().get(0);
		} catch (Exception e) {
			return null;
		}
	}
}
