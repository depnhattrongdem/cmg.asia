package asia.cmg.f8.marketing.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.marketing.dto.MarketingUserDetail;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@FeignClient(value = "marketingUser", url = "${feign.url}")
public interface MarketingUserClient {

	String SECRET_QUERY = "client_id=${userGrid.userGridClientId}&client_secret=${userGrid.userGridClientSecret}";
	String LIMIT = "limit";
    String QUERY = "query";
    
    @RequestMapping(value = "/marketing_user?" + SECRET_QUERY, method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE)
    UserGridResponse<MarketingUserDetail> create(@RequestBody final MarketingUserDetail userInfo);
}
