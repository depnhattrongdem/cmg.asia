package asia.cmg.f8.marketing.api;

import java.util.Objects;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import asia.cmg.f8.common.dto.ApiRespObject;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.marketing.dto.MarketingUserDetail;
import asia.cmg.f8.marketing.service.MarketingUserService;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class UserApi {
	
	private Logger logger = LoggerFactory.getLogger(UserApi.class);
	private Gson gson = new Gson();
	
	@Autowired
	private MarketingUserService userService;

	@PostMapping(value = "/marketing/web/v1/users", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> createUser(@RequestBody @Valid final MarketingUserDetail userDetail, final Account account) {
		logger.info("Request body input: {}", gson.toJson(userDetail));
		
		MarketingUserDetail newUser = userService.createUser(userDetail);
		ApiRespObject<MarketingUserDetail> response = new ApiRespObject<MarketingUserDetail>();
		
		if(Objects.isNull(newUser)) {
			response.setStatus(ErrorCode.UG_ERROR_CREATE);
		} else {
			response.setStatus(ErrorCode.SUCCESS);
			response.setData(newUser);
		}
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
}
