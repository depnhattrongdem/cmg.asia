# Dockerized F8 Application
This contains Dockerfile for required services that are used to init and run UserGrid instance.

1. Cassandra 2.1
2. Elastic Search 1.7.4
3. UserGrid 2.1
4. UserGrid Portal
5. UserGrid Setup. This image is used to init initial data for User Grid. Initial data of F8 should be also put here.
6. Swagger UI for F8 API specification.

# How to build

Before you start, you should edit **.env** file and update value of **IP_HOST** variable with IP of your machine.

Start building docker images

```
docker-compose -f docker-compose.yml -f initial-data-setup.yml build
```

Initialize initial data for UserGrid & F8 application. It's one time execution.

```
docker-compose -f docker-compose.yml -f initial-data-setup.yml up
```

Next start up time, you just need to execute

```
docker-compose up -d
```

# Pre-setup accounts

There are some UserGrid accounts are created for development purpose

1. Admin account to login to UserGrid Portal: **admin/admin!@#**

2. Application account: **demo/Demo123!@#**

# Verify your installation

- Verify UserGrid installation by open browser to http://[your-host-ip]:8080/status
- Verify UserGrid Portal by open browser to http://[your-host-ip]:9090. Log-in with **admin/admin!@#**
- Verify API specification by open browser to http://[your-host-ip]:9999

# Environment variables

| Variable | Services/Containers | Notes |
|----------|-------------------|-------|
|CASSANDRA_PORT_9160_TCP_ADDR | Cassandra | IP address of cassandra |
|CASSANDRA_PORT_9160_TCP_PORT | Cassandra | Opened port. Default is 9160 |
| CASSANDRA_CLUSTER_NAME | Cassandra | Cluster name. Default value is usergrid |
|ELASTICSEARCH_PORT_9300_TCP_ADDR| Elasticsearch | IP address of Elasticsearch |
|ELASTICSEARCH_PORT_9300_TCP_PORT| Elasticsearch | Opended port. Default is 9300 |
| ADMIN_PASS | UserGrid | Default administrator's password for UserGrid. Specified on init|
| ORG_NAME | UserGrid | Organization name. Default vaule is cmg |
| APP_NAME | UserGrid | Application name. Defaule value is f8 |
| ADMIN_USER | UserGrid | Default username of administator. Specified on init |
| USERGRID_HOST | UserGrid, UserGrid Portal| An external IP address of UserGrid. Default value is IP of docker host machine |
| API_VERSION | API Spec | the API version |
| API_HOST | API Spec | the API host |
| API_BASE_PATH | Api Spec | The base path. Default value is / |

# Opened ports on host-machine

List opened ports on host-machine for containers.

| Opened ports | Service/Container |
| -------------| ------------------|
| **9042**: native transport (cassandra query language, cql) | Cassandra |
| **9160**: thrift interface (legacy) | cassandra |
| **9200**: HTTP | Elasticsearch |
| **9300**: native transport | Elasticsearch |
| **8080**: HTTP| UserGrid |
| **8443**: HTTP/SSL | UserGrid |
| **9090**: HTTP | User Grid Portal |
| **9999**: HTTP | API Specification |
