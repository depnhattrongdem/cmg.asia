###
###               ejabberd configuration file
###
###

### The parameters used in this configuration file are explained in more detail
### in the ejabberd Installation and Operation Guide.
### Please consult the Guide in case of doubts, it is included with
### your copy of ejabberd, and is also available online at
### http://www.process-one.net/en/ejabberd/docs/

###   =======
###   LOGGING

loglevel: {{ env['EJABBERD_LOGLEVEL'] or 4 }}
log_rotate_size: 10485760
log_rotate_count: 0
log_rate_limit: 100

## watchdog_admins:
##   - "bob@example.com"

###   ================
###   SERVED HOSTNAMES

hosts:
{%- for xmpp_domain in env['XMPP_DOMAIN'].split() %}
  - "{{ xmpp_domain }}"
{%- endfor %}

##
## route_subdomains: Delegate subdomains to other XMPP servers.
## For example, if this ejabberd serves example.org and you want
## to allow communication with an XMPP server called im.example.org.
##
## route_subdomains: s2s

###   ===============
###   LISTENING PORTS

listen:
  -
    port: 5222
    module: ejabberd_c2s
    {%- if env['EJABBERD_STARTTLS'] == "true" %}
    starttls_required: true
    {%- endif %}
    protocol_options:
      - "no_sslv3"
    {%- if env.get('EJABBERD_PROTOCOL_OPTIONS_TLSV1', "false") == "false" %}
      - "no_tlsv1"
    {%- endif %}
    {%- if env.get('EJABBERD_PROTOCOL_OPTIONS_TLSV1_1', "true") == "false" %}
      - "no_tlsv1_1"
    {%- endif %}
    max_stanza_size: 65536
    shaper: c2s_shaper
    access: c2s
    ciphers: "{{ env.get('EJABBERD_CIPHERS', 'HIGH:!aNULL:!3DES') }}"
    {%- if env.get('EJABBERD_DHPARAM', false) == "true" %}
    dhfile: "/opt/ejabberd/ssl/dh.pem"
    {%- endif %}
  -
    port: 5269
    module: ejabberd_s2s_in
  -
    port: 4560
    module: ejabberd_xmlrpc
    access_commands:
      configure:
        all: []

  -
    port: 5280
    module: ejabberd_http
    request_handlers:
      "/websocket": ejabberd_http_ws
    ##  "/pub/archive": mod_http_fileserver
    web_admin: true
    http_bind: true
    ## register: true
    captcha: true
    {%- if env['EJABBERD_HTTPS'] == "true" %}
    tls: true
    certfile: "/opt/ejabberd/ssl/host.pem"
    {% endif %}
  -
    port: 5443
    module: ejabberd_http
    request_handlers:
      "": mod_http_upload
    {%- if env['EJABBERD_HTTPS'] == "true" %}
    tls: true
    certfile: "/opt/ejabberd/ssl/host.pem"
    {% endif %}


###   SERVER TO SERVER
###   ================

{%- if env['EJABBERD_S2S_SSL'] == "true" %}
s2s_use_starttls: required
s2s_certfile: "/opt/ejabberd/ssl/host.pem"
s2s_protocol_options:
  - "no_sslv3"
  {%- if env.get('EJABBERD_PROTOCOL_OPTIONS_TLSV1', "false") == "false" %}
  - "no_tlsv1"
  {%- endif %}
  {%- if env.get('EJABBERD_PROTOCOL_OPTIONS_TLSV1_1', "true") == "false" %}
  - "no_tlsv1_1"
  {%- endif %}
s2s_ciphers: "{{ env.get('EJABBERD_CIPHERS', 'HIGH:!aNULL:!3DES') }}"
{%- if env.get('EJABBERD_DHPARAM', false) == "true" %}
s2s_dhfile: "/opt/ejabberd/ssl/dh.pem"
{%- endif %}
{% endif %}

###   ==============
###   AUTHENTICATION

allow_contrib_modules: true
auth_password_format: plain
disable_sasl_mechanisms:
  - "scram-sha-1"
  - "digest-md5"
  - "x-oauth2"
auth_method: anonymous
auth_opts:
  host: "{{ env.get('HTTP_AUTHENTICATION_ENDPOINT', 'http://localhost:8080') }}"
  connection_pool_size: 10
  path_prefix: "{{ env.get('HTTP_AUTHENTICATION_ENDPOINT_PATH_PREFIX', '/') }}"

###   ===============
###   TRAFFIC SHAPERS

shaper:
  normal: 1000
  fast: 50000
max_fsm_queue: 1000

###   ====================
###   ACCESS CONTROL LISTS

acl:
  admin:
    user:
    {%- if env['EJABBERD_ADMINS'] %}
      {%- for admin in env['EJABBERD_ADMINS'].split() %}
      - "{{ admin.split('@')[0] }}": "{{ admin.split('@')[1] }}"
      {%- endfor %}
    {%- else %}
      - "admin": "{{ env['XMPP_DOMAIN'].split()[0] }}"
    {%- endif %}
  local:
    user_regexp: ""

###   ============
###   ACCESS RULES

access:
  ## Maximum number of simultaneous sessions allowed for a single user:
  max_user_sessions:
    all: 10
  ## Maximum number of offline messages that users can have:
  max_user_offline_messages:
    admin: 5000
    all: 100
  ## This rule allows access only for local users:
  local:
    local: allow
  ## Only non-blocked users can use c2s connections:
  c2s:
    blocked: deny
    all: allow
  ## For C2S connections, all users except admins use the "normal" shaper
  c2s_shaper:
    admin: none
    all: normal
  ## All S2S connections use the "fast" shaper
  s2s_shaper:
    all: fast
  ## Only admins can send announcement messages:
  announce:
    admin: allow
  ## Only admins can use the configuration interface:
  configure:
    admin: allow
  ## Admins of this server are also admins of the MUC service:
  muc_admin:
    admin: allow
  ## Only accounts of the local ejabberd server, or only admins can create rooms, depending on environment variable:
  muc_create:
    {%- if env['EJABBERD_MUC_CREATE_ADMIN_ONLY'] == "true" %}
    admin: allow
    {% else %}
    local: allow
    {% endif %}
  ## All users are allowed to use the MUC service:
  muc:
    all: allow
  ## Only accounts on the local ejabberd server can create Pubsub nodes:
  pubsub_createnode:
    local: allow
  ## In-band registration allows registration of any possible username.
  register:
    {%- if env['EJABBERD_REGISTER_ADMIN_ONLY'] == "true" %}
    all: deny
    admin: allow
    {% else %}
    all: allow
    {% endif %}
  ## Only allow to register from localhost
  trusted_network:
    loopback: allow
  soft_upload_quota:
    all: 400 # MiB
  hard_upload_quota:
    all: 500 # MiB


language: "en"

###   =======
###   MODULES

modules:  
  mod_mam:
   db_type: sql
   default: always
  mod_offline:
    db_type: sql
    access_max_user_messages: max_user_offline_messages

###   ============
###   HOST CONFIG

host_config:
{%- for xmpp_domain in env['XMPP_DOMAIN'].split() %}
  "{{ xmpp_domain }}":
    domain_certfile: "/opt/ejabberd/ssl/{{ xmpp_domain }}.pem"
{%- endfor %}

{%- if env['EJABBERD_CONFIGURE_ODBC'] == "true" %}
###   ====================
###   ODBC DATABASE CONFIG
odbc_type: {{ env['EJABBERD_ODBC_TYPE'] }}
odbc_server: {{ env['EJABBERD_ODBC_SERVER'] }}
odbc_database: {{ env['EJABBERD_ODBC_DATABASE'] }}
odbc_username: {{ env['EJABBERD_ODBC_USERNAME'] }}
odbc_password: {{ env['EJABBERD_ODBC_PASSWORD'] }}
odbc_pool_size: {{ env['EJABBERD_ODBC_POOL_SIZE'] }}
{% endif %}

{%- if env['EJABBERD_DEFAULT_DB'] is defined %}
default_db: {{ env['EJABBERD_DEFAULT_DB'] }}
{% endif %}

###   =====================
###   SESSION MANAGEMENT DB
sm_db_type: {{ env['EJABBERD_SESSION_DB'] or "mnesia" }}

{%- if env['EJABBERD_CONFIGURE_REDIS'] == "true" %}
###   ====================
###   REDIS DATABASE CONFIG
redis_server: {{ env['EJABBERD_REDIS_SERVER'] or "localhost" }}
redis_port: {{ env['EJABBERD_REDIS_PORT'] or 6379 }}
{%- if env['EJABBERD_REDIS_PASSWORD'] is defined %}
redis_password: {{ env['EJABBERD_REDIS_PASSWORD'] }}
{% endif %}
redis_db: {{ env['EJABBERD_REDIS_DB'] or 0}}
redis_reconnect_timeout: {{ env['EJABBERD_REDIS_RECONNECT_TIMEOUT'] or 1 }}
redis_connect_timeout: {{ env['EJABBERD_REDIS_CONNECT_TIMEOUT'] or 1 }}
{% endif %}

###   ====================
###   MYSQL DATABASE CONFIG
sql_type: mysql
sql_server: "{{ env.get('MYSQL_HOST', 'localhost') }}"
sql_port: {{ env.get('MYSQL_PORT', 3306) }}
sql_database: "{{ env.get('MYSQL_DATABASE', 'ejabberd') }}"
sql_username: "{{ env.get('MYSQL_USERNAME', 'ejabberd') }}"
sql_password: "{{ env.get('MYSQL_PASSWORD', 'ejabberd') }}"
sql_pool_size: 5