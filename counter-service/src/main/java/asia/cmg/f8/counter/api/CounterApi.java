package asia.cmg.f8.counter.api;

import asia.cmg.f8.common.dto.ApiRespObject;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.security.annotation.RequiredAdminRole;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.counter.dto.CounterDto;
import asia.cmg.f8.counter.dto.LikeViewCounterDTO;
import asia.cmg.f8.counter.entity.Counter;
import asia.cmg.f8.counter.service.CounterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created on 1/19/17.
 */
@RestController
public class CounterApi {

    public static final String COUNTER_NAME = "counterName";
    public static final String COUNTERS = "counters";
    public static final String LIKE_COUNTER = "like_counter";
    public static final String VIEW_COUNTER = "view_counter";
    private final CounterService counterService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CounterApi.class);

    public CounterApi(final CounterService counterService) {
        this.counterService = counterService;
    }


    @GetMapping(value = "/counters/{counterName}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity loadCounter(@PathVariable(COUNTER_NAME) final String name) {
        int count = 0;
        final Optional<Counter> counter = counterService.findOne(name);
        if (counter.isPresent()) {
            count = counter.get().getValue();
        }
        return new ResponseEntity<>(Collections.singletonMap("count", count), HttpStatus.OK);
    }

    @GetMapping(value = "/counters", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity getCounters(@RequestParam(COUNTERS) final Set<String> counters) {
        final Optional<List<Counter>> countersResp = counterService.findByNames(counters);
        if (countersResp.isPresent()) {
            final Map<String, CounterDto> mapCounterResp =
                    countersResp.get().stream().collect(Collectors.toMap(Counter::getName, CounterDto::new));
            return new ResponseEntity<>(counters.stream().map(counterName -> {
                if (mapCounterResp.containsKey(counterName)) {
                    return mapCounterResp.get(counterName);
                }
                final CounterDto newCounter = new CounterDto();
                newCounter.setValue(0);
                newCounter.setName(counterName);
                return newCounter;
            }).collect(Collectors.toList()), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping(value = "/counters/{counterName}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity resetCounter(@PathVariable(COUNTER_NAME) final String name) {
        counterService.resetCounter(name);
        return new ResponseEntity<>(Collections.singletonMap("success", true), HttpStatus.OK);
    }

    @PutMapping(value = "/counters/{counterName}/increase", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity increaseCounter(@PathVariable(COUNTER_NAME) final String name) {
        counterService.inCreaseCounter(name);
        return new ResponseEntity<>(Collections.singletonMap("success", true), HttpStatus.OK);
    }

    @PutMapping(value = "/counters/{counterName}/decrease", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity decreaseCounter(@PathVariable(COUNTER_NAME) final String name) {
        counterService.decreaseCounter(name);
        return new ResponseEntity<>(Collections.singletonMap("success", true), HttpStatus.OK);
    }
    
    @PutMapping(value = "/counters/{counterName}/random", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity randomCounter(@PathVariable(COUNTER_NAME) final String name, @RequestParam(value = "value") final int value) {
        counterService.randomCounter(name, value);
        return new ResponseEntity<>(Collections.singletonMap("success", true), HttpStatus.OK);
    }
    
    /**
     * Update the number of views/likes of an article
     * @param postId - The post uuid
     * @return
     */
    @PostMapping(value = "/admin/update_counter/post/{post_id}", produces = APPLICATION_JSON_VALUE)
    @RequiredAdminRole
    public ResponseEntity<Object> updateViewOrLikeCounter(@PathVariable(name = "post_id") final String postId,
    													@RequestBody final LikeViewCounterDTO request,
    													final Account account) {
    	ApiRespObject<Object> apiResponse = new ApiRespObject<Object>();
    	apiResponse.setStatus(ErrorCode.SUCCESS);
    	try {
    		String counterName = "";
    		Integer counterValue = 0;
    		
    		if(request.getViewCounter() != null && request.getViewCounter() >= 0) {
    			counterName = postId.concat("_").concat(VIEW_COUNTER);
				counterValue = request.getViewCounter();
				
				counterService.updateCounter(counterName, counterValue);
				LOGGER.info("[updateViewOrLikeCounter] Already updated view counter = {} of post id {}", counterValue, postId);
    		}
    		
    		if(request.getLikeCounter() != null && request.getLikeCounter() >= 0) {
    			counterName = postId.concat("_").concat(LIKE_COUNTER);
				counterValue = request.getLikeCounter() == null ? 0 : request.getLikeCounter();
				
				counterService.updateCounter(counterName, counterValue);
				LOGGER.info("[updateViewOrLikeCounter] Already updated like counter = {} of post id {}", counterValue, postId);
    		}
    		
    		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
		} catch (Exception e) {
			apiResponse.setStatus(ErrorCode.FAILED.withDetail(e.getMessage()));
			return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
		}
    }
}
