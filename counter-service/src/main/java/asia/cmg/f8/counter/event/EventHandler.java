package asia.cmg.f8.counter.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.stereotype.Component;
import asia.cmg.counter.thread.CalculatePTIMatchRunnable;
import asia.cmg.f8.common.event.user.AdminApprovedDocumentEvent;
import asia.cmg.f8.common.event.user.UnapproveDocumentEvent;
import asia.cmg.f8.common.profile.CompleteProfileEvent;
import asia.cmg.f8.common.spec.user.UserType;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.counter.client.UserClient;
import asia.cmg.f8.counter.config.CounterProperties;
import asia.cmg.f8.counter.dto.UserDTO;
import asia.cmg.f8.counter.repository.QuestionGroupRepository;
import asia.cmg.f8.counter.repository.UserPtiMatchRepository;
import asia.cmg.f8.counter.service.AnswersCollectionService;
import asia.cmg.f8.counter.service.PTIMatchingService;

@Component
@EnableBinding(value = EventStream.class)
public class EventHandler {

	@Autowired
    @Qualifier("completeProfileEventConverter")
    private MessageConverter completeProfileEventConverter;
	
	@Autowired
	@Qualifier("adminApproveDocumentEventConverter")
	private MessageConverter adminApproveDocumentEventConverter;
	
	@Autowired
	@Qualifier("adminUnapproveDocumentEventConverter")
	private MessageConverter adminUnapproveDocumentEventConverter;
	
	@Autowired
	private UserClient userClient;
	
	@Autowired
	private UserPtiMatchRepository userPtiMatchRepo;
	
	@Autowired
	private AnswersCollectionService answerService;
	
	@Autowired
	private PTIMatchingService ptiMatchingService;
	
	@Autowired
	private QuestionGroupRepository questionGroupRepo;
	
	@Autowired
	private CounterProperties counterProperties;
	
	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
//	@StreamListener(value = EventStream.COMPLETE_PROFILE_IN_CHANNEL)
//	public void handleCompletedOnboarQuestionEvent(final Message<?> message) {
//		final CompleteProfileEvent messageEvent = (CompleteProfileEvent)completeProfileEventConverter.fromMessage(message, CompleteProfileEvent.class);
//		String userUuid = messageEvent.getUserId().toString();
//		String userType = messageEvent.getUserType().toString();
//		
//		LOGGER.info("Handling CompletedOnboarQuestionEvent for user [{}]", userUuid);
//		
//		if(UserType.EU.name().compareToIgnoreCase(userType) == 0) {
//			UserGridResponse<UserDTO> ugResponse = userClient.getUser(userUuid);
//			if(ugResponse != null && ugResponse.getEntities() != null) {
//				UserDTO userInfo = ugResponse.getEntities().get(0);
//				boolean isRecalculateForEU = true;
//				CalculatePTIMatchRunnable ptiService = new CalculatePTIMatchRunnable("handleCompletedOnboarQuestionEvent", userInfo, isRecalculateForEU, userClient, userPtiMatchRepo, answerService, ptiMatchingService, questionGroupRepo, counterProperties);
//				ptiService.run();
//			}
//		} else {
//			LOGGER.info("[handleCompletedOnboarQuestionEvent] Ignore calculate PTI Matching for PT [{}]", userUuid);
//		}
//	}
	
//	@StreamListener(value = EventStream.ADMIN_APPROVE_DOCUMENT_IN_CHANNEL)
//	public void handleAdminApproveDocumentEvent(final Message<?> message) {
//		final AdminApprovedDocumentEvent messageEvent = (AdminApprovedDocumentEvent)adminApproveDocumentEventConverter.fromMessage(message, AdminApprovedDocumentEvent.class);
//		String trainerUuid = messageEvent.getTrainerUuid().toString();
//		
//		LOGGER.info("Handling AdminApproveDocumentEvent for trainer [{}]", trainerUuid);
//		
//		UserGridResponse<UserDTO> ugResponse = userClient.getUser(trainerUuid);
//		if(ugResponse != null && ugResponse.getEntities() != null) {
//			UserDTO trainerInfo = ugResponse.getEntities().get(0);
//			boolean isRecalculateForEU = false;
//			CalculatePTIMatchRunnable ptiService = new CalculatePTIMatchRunnable(userClient, isRecalculateForEU, userPtiMatchRepo, answerService, ptiMatchingService, questionGroupRepo, counterProperties);
//			ptiService.calculatePtiMatchForPT(trainerInfo);
//		} else {
//			LOGGER.info("[handleAdminApproveDocumentEvent] Trainer not existed  in UG database with uuid: [{}]", trainerUuid);
//		}
//	}
	
	/**
	 * Handling admin unapprove documentation to clear PTI-Match caching of trainer on user_ptimatch table
	 * @param message
	 */
//	@StreamListener(value = EventStream.UNAPPROVE_DOCUMENT_IN_CHANNEL)
//	public void handleAdminUnapprovedDocumentEvent(final Message<?> message) {
//		final UnapproveDocumentEvent messageEvent = (UnapproveDocumentEvent)adminUnapproveDocumentEventConverter.fromMessage(message, UnapproveDocumentEvent.class);
//		LOGGER.info("Handling AdminUnapproveDocumentEvent: {}", messageEvent.toString());
//		
//		String trainerUuid = messageEvent.getTrainerUuid().toString();
//		long deletedCount = ptiMatchingService.clearPtiMatchDataOfTrainer(trainerUuid);
//		
//		LOGGER.info("[handleAdminUnapprovedDocumentEvent] cleared pti-match data of trainer {} effected records amount {}", trainerUuid, deletedCount);
//	}
}
