package asia.cmg.f8.counter.event;

import java.io.IOException;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import asia.cmg.f8.common.message.AvRoMessageConverterLoader;
import asia.cmg.f8.common.message.EnableMessage;

@Configuration
@EnableMessage
public class EventConfiguration {

	private final AvRoMessageConverterLoader avroMessageConverterLoader;
	
	@Inject
    public EventConfiguration(final AvRoMessageConverterLoader avroMessageConverterLoader) {
        this.avroMessageConverterLoader = avroMessageConverterLoader;
    }
	
	@Bean(name = "completeProfileEventConverter")
    public MessageConverter completeProfileEventConverter() throws IOException {
        return avroMessageConverterLoader.load("CompleteProfileEvent.avsc").get();
    }
	
	@Bean(name = "adminApproveDocumentEventConverter")
	public MessageConverter adminApproveDocumentEventConverter() throws IOException {
		return avroMessageConverterLoader.load("AdminApprovedDocumentEvent.avsc").get();
	}
	
	@Bean(name = "adminUnapproveDocumentEventConverter")
	public MessageConverter adminUnapproveDocumentEventConverter() throws IOException {
		return avroMessageConverterLoader.load("UnapproveDocumentEvent.avsc").get();
	}
}
