package asia.cmg.f8.counter.dto;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LikeViewCounterDTO {
	
	@Nullable
	@JsonProperty("view_counter")
	private Integer viewCounter;
	
	@Nullable
	@JsonProperty("like_counter")
	private Integer likeCounter;
	
	public Integer getViewCounter() {
		return viewCounter;
	}

	public void setViewCounter(Integer viewCounter) {
		this.viewCounter = viewCounter;
	}

	public Integer getLikeCounter() {
		return likeCounter;
	}

	public void setLikeCounter(Integer likeCounter) {
		this.likeCounter = likeCounter;
	}
}
