package asia.cmg.f8.counter.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuestionPTiMatch {
	
	@JsonProperty(value = "question_id")
    private String questionId;

    @JsonProperty(value = "weight")
    private int weight;

    @JsonProperty(value = "options")
    private List<String> options;

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}
}
