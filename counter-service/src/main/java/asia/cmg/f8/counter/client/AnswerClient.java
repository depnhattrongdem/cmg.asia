package asia.cmg.f8.counter.client;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.counter.collection.Answers;

/**
 * @author tung.nguyenthanh
 */
@FeignClient(value = "answers", url = "${feign.url}", fallback = AnswerClientFallbackImpl.class)
public interface AnswerClient {

    String QUERY = "query";
    String LIMIT = "limit";
    String SECRECT_QUERY = "client_id=${userGrid.userGridClientId}&client_secret=${userGrid.userGridClientSecret}";

    @RequestMapping(value = "/answers?limit={limit}&ql={query}&" + SECRECT_QUERY, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<Answers> getAnswersByQuery(@PathVariable(QUERY) String query, @RequestParam(LIMIT) int limit);
    
    @RequestMapping(value = "/answers?ql={query}&" + SECRECT_QUERY, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<Answers> getAnswersByQuery(@PathVariable(QUERY) String query);
}
