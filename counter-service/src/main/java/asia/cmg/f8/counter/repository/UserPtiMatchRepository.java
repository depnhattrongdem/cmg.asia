package asia.cmg.f8.counter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import asia.cmg.f8.counter.entity.UserPtiMatchEntity;

@Repository
@Transactional(readOnly = true)
public interface UserPtiMatchRepository extends JpaRepository<UserPtiMatchEntity, Long> {

	@Query(value = "SELECT * FROM user_ptimatch WHERE eu_uuid = ?1 AND pt_uuid = ?2", nativeQuery = true)
	Optional<UserPtiMatchEntity> getByEuUuidAndPtUuid(final String euUuid, final String ptUuid);
	
	@Query(value = "SELECT COUNT(*) FROM user_ptimatch WHERE eu_uuid = ?1", nativeQuery = true)
	int countByEuUuid(final String euUuid);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE user_ptimatch SET average = 0, interest = 0, personality = 0, training_style = 0 WHERE eu_uuid = :eu_uuid", nativeQuery = true)
	void resetMatchedValuesByEuUuid(@Param("eu_uuid") final String euUuid);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE user_ptimatch SET is_old = 1 WHERE eu_uuid = :eu_uuid", nativeQuery = true)
	void markedIsOldByEuUuid(@Param("eu_uuid") final String euUuid);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM user_ptimatch WHERE eu_uuid = :eu_uuid", nativeQuery = true)
	void clearResultByEuUuid(@Param("eu_uuid") final String euUuid);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE user_ptimatch SET average = 0, interest = 0, personality = 0, training_style = 0 WHERE pt_uuid = :pt_uuid", nativeQuery = true)
	void resetMatchedValuesByPtUuid(@Param("pt_uuid") final String pt_uuid);
	
	@Modifying
	@Transactional
	Long deleteByPtUuid(final String ptUuid);
}
