package asia.cmg.f8.counter.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import asia.cmg.f8.counter.dto.UserPTIMatchType;

@Entity
@Table(name = "user_ptimatch")
public class UserPtiMatchEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "eu_uuid", length = 50)
	private String euUuid;
		
	@ElementCollection
	private List<String> eu_location_answers = new ArrayList<String>();
	
	@ElementCollection
	private List<String> eu_district_answers = new ArrayList<String>();
	
	@ElementCollection
	private List<String> eu_age_answers = new ArrayList<String>();
	
	@ElementCollection
	private List<String> eu_gender_answers = new ArrayList<String>();
	
	@Column(name = "pt_uuid", length = 50)
	private String ptUuid;
	
	@Column(name = "average")
	private Double average = 0d;
	
	@Column(name = "personality")
	private Double personality = 0d;
	
	@Column(name = "training_style")
	private Double trainingStyle = 0d;
	
	@Column(name = "interest")
	private Double interest = 0d;
	
	@CreationTimestamp
	@Column(name = "created_date", updatable = false)
	@JsonProperty("created_date")
	private Date createdDate;
	
	@UpdateTimestamp
	@Column(name = "modified_date")
	@JsonIgnore
	private Date modifiedDate;
	
	@Column(name = "modified_count")
	private Integer modifiedCount = 0;
	
	@Column(name = "matched_type", columnDefinition = "int default 0")
	@Enumerated(EnumType.ORDINAL)
	private UserPTIMatchType matchedType;
	
	@Column(name = "is_old",  columnDefinition = "tinyint(1) default 0")
	private Boolean isOld = Boolean.FALSE;
	
	public Boolean getIsOld() {
		return isOld;
	}

	public void setIsOld(Boolean isOld) {
		this.isOld = isOld;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEu_uuid() {
		return euUuid;
	}

	public void setEu_uuid(String eu_uuid) {
		this.euUuid = eu_uuid;
	}

	public String getPt_uuid() {
		return ptUuid;
	}

	public void setPt_uuid(String pt_uuid) {
		this.ptUuid = pt_uuid;
	}

	public Double getAverage() {
		return average;
	}

	public void setAverage(Double average) {
		this.average = average;
	}

	public Double getPersonality() {
		return personality;
	}

	public void setPersonality(Double personality) {
		this.personality = personality;
	}

	public Double getTrainingStyle() {
		return trainingStyle;
	}

	public void setTrainingStyle(Double trainingStyle) {
		this.trainingStyle = trainingStyle;
	}

	public Double getInterest() {
		return interest;
	}

	public void setInterest(Double interest) {
		this.interest = interest;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getModifiedCount() {
		return modifiedCount;
	}

	public void setModifiedCount(Integer modifiedCount) {
		this.modifiedCount = modifiedCount;
	}
	
	public List<String> getEu_location_answers() {
		return eu_location_answers;
	}

	public void setEu_location_answers(List<String> eu_location_answers) {
		this.eu_location_answers = eu_location_answers;
	}

	public List<String> getEu_district_answers() {
		return eu_district_answers;
	}

	public void setEu_district_answers(List<String> eu_district_answers) {
		this.eu_district_answers = eu_district_answers;
	}

	public List<String> getEu_age_answers() {
		return eu_age_answers;
	}

	public void setEu_age_answers(List<String> eu_age_answers) {
		this.eu_age_answers = eu_age_answers;
	}

	public List<String> getEu_gender_answers() {
		return eu_gender_answers;
	}

	public void setEu_gender_answers(List<String> eu_gender_answers) {
		this.eu_gender_answers = eu_gender_answers;
	}
	
	public UserPTIMatchType getMatchedType() {
		return matchedType;
	}

	public void setMatchedType(UserPTIMatchType matchedType) {
		this.matchedType = matchedType;
	}

	public String toString() {
		return String.format("{\"id\": %s, \"eu_uuid\": %s, \"pt_uuid\": %s, \"average\": %s, \"personality\": %s, \"trainingStyle\": %s, \"interest\": %s}", 
				id, euUuid, ptUuid, average, personality, trainingStyle, interest);
	}
}
