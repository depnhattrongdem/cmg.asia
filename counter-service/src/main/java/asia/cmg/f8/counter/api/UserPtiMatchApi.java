package asia.cmg.f8.counter.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.util.PagedUserGridResponse;
import asia.cmg.f8.counter.client.UserClient;
import asia.cmg.f8.counter.dto.UserDTO;
import asia.cmg.f8.counter.entity.UserPtiMatchEntity;
import asia.cmg.f8.counter.repository.UserPtiMatchRepository;
import asia.cmg.f8.counter.service.PTIMatchingService;

@RestController
public class UserPtiMatchApi {
	
	@Autowired
	private UserPtiMatchRepository userPtiMatchRepo;
	
	@Autowired
	private UserClient userClient;
	
	@Autowired
	private PTIMatchingService ptiMatchingService;
	
	private final String getEuQuery = "select * where userType = 'eu' and activated = true";
	private final String getPtQuery = "select * where userType = 'pt' and activated = true and status.document_status = 'approved'";
	private final int PT_LIMIT = 100;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserPtiMatchApi.class);

//	@RequestMapping(value = "/mobile/v1/user_pti_match", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
//	public ResponseEntity<Object> updatePtiMatchForEU(final Account account) {
//		try {
//			if(account.isEu()) {
//				this.calculatePtiMatchForEU(account.uuid());
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		
//		return new ResponseEntity<Object>(HttpStatus.OK);
//	}
	
//	private boolean calculatePtiMatchForEU(final String euUuid) {
//		try {
//			String cursor = null;
//			do {
//				PagedUserGridResponse<UserDTO> pagedPTs = userClient.getUser(getPtQuery, PT_LIMIT, cursor);
//				if(pagedPTs != null) {
//					cursor = pagedPTs.getCursor();
//					
//					List<UserDTO> ptList = pagedPTs.getEntities();
//					ptList.forEach(pt -> {
//						Optional<UserPtiMatch> userPtiMatchOld = userPtiMatchRepo.getByEuUuidAndPtUuid(euUuid, pt.getUuid());
//						UserPtiMatch userPtiMatchNew = ptiMatchingService.calculatePtiMatchBetweenEUAndPT(euUuid, pt.getUuid());
//						
//						if(userPtiMatchOld.isPresent()) {
//							userPtiMatchNew.setId(userPtiMatchOld.get().getId());
//							userPtiMatchNew.setModifiedCount(userPtiMatchOld.get().getModifiedCount() + 1);
//						}
//						userPtiMatchRepo.saveAndFlush(userPtiMatchNew);
//						LOGGER.info("UserPtiMatch info: {}", userPtiMatchNew);
//					});
//				}
//			} while(cursor != null);
//			
//		} catch (Exception e) {
//			return false;
//		}
//		return true;
//	}
}
