package asia.cmg.f8.counter.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "question_group")
public class QuestionGroupEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "name", length = 100)
	private String name;
	
	@Column(name = "weight")
	private Double weight;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "questionGroup", targetEntity = QuestionWeightEntity.class)
	private List<QuestionWeightEntity> questions = new ArrayList<QuestionWeightEntity>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public List<QuestionWeightEntity> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionWeightEntity> questions) {
		this.questions = questions;
		questions.forEach(question -> {
			question.setQuestionGroup(this);
		});
	}
}
