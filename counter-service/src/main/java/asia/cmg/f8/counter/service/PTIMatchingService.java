package asia.cmg.f8.counter.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.counter.collection.Answers;
import asia.cmg.f8.counter.dto.MigrationPTIRequestDto;
import asia.cmg.f8.counter.entity.QuestionGroupEntity;
import asia.cmg.f8.counter.entity.QuestionWeightEntity;
import asia.cmg.f8.counter.entity.UserPtiMatchEntity;
import asia.cmg.f8.counter.repository.UserPtiMatchRepository;

@Service
public class PTIMatchingService {
	
	@Autowired
	UserPtiMatchRepository ptiMatchRepo;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PTIMatchingService.class);
	
	/**
	 * Clear pti match data of trainer
	 * @param trainerUuid
	 * @return The amount of deleted records
	 */
	public long clearPtiMatchDataOfTrainer(final String trainerUuid) {
		try {
			return ptiMatchRepo.deleteByPtUuid(trainerUuid);
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Transactional
	public long migratePtiResult(List<MigrationPTIRequestDto> users, Account account) {
		
		ptiMatchRepo.clearResultByEuUuid(account.uuid());
		
		List<UserPtiMatchEntity> entities = new ArrayList<UserPtiMatchEntity>();
		
		for (MigrationPTIRequestDto user : users) {
		
			UserPtiMatchEntity entity = new UserPtiMatchEntity();
			
			entity.setPt_uuid(user.getPtUuid());
			entity.setEu_uuid(account.uuid());
			entity.setPersonality(user.getPersonality());
			entity.setInterest(user.getInterest());
			entity.setTrainingStyle(user.getTrainingStyle());
			entity.setAverage(user.getAverage());
			entity.setIsOld(false);
			
			entities.add(entity);
			
		}
		
		return ptiMatchRepo.save(entities).size();
	}
	
	
	private UserPtiMatchEntity calculatePtiMatch(final Map<String, Answers> euAnswers, final Map<String, Answers> ptAnswers, final List<QuestionGroupEntity> questionGroups) {
		UserPtiMatchEntity ptiMatch = new UserPtiMatchEntity();
		Double interest = 0d;
		Double trainingStyle = 0d;
		Double personality = 0d;
		Double average = 0d;
		
		for (QuestionGroupEntity questionGroup : questionGroups) {
			switch (questionGroup.getName()) {
			case "I":
				interest = this.calculatePtiMatchPerQuestionGroup(euAnswers, ptAnswers, questionGroup);
				average += interest * questionGroup.getWeight();
				ptiMatch.setInterest(interest);
				break;
			case "T":
				trainingStyle = this.calculatePtiMatchPerQuestionGroup(euAnswers, ptAnswers, questionGroup);
				average += trainingStyle * questionGroup.getWeight();
				ptiMatch.setTrainingStyle(trainingStyle);
				break;
			case "P":
				personality = this.calculatePtiMatchPerQuestionGroup(euAnswers, ptAnswers, questionGroup);
				average += personality * questionGroup.getWeight();
				ptiMatch.setPersonality(personality);
				break;
			default:
				break;
			} 
		}
		
		ptiMatch.setAverage(average);
		return ptiMatch;
	}
	
	private Double calculatePtiMatchPerQuestionGroup(final Map<String, Answers> euAnswers, final Map<String, Answers> ptAnswers, final QuestionGroupEntity questionGroup) {
		double[] ptiMatchValue = {0d};
		double[] questionWeightValue = {0d};

		euAnswers.forEach((questionId, euAnswer) -> {
			boolean isInQuestionGroup = false;
			
			// Check if the EU's question is in current question group 
			for (QuestionWeightEntity questionWeight : questionGroup.getQuestions()) {
				if(questionId != null && questionId.equalsIgnoreCase(questionWeight.getQuestionKey())) {
					isInQuestionGroup = true;
					questionWeightValue[0] = questionWeight.getWeight();
					break;
				}
			}
			
			if(isInQuestionGroup) {
				Answers ptAnswer = ptAnswers.get(questionId);
				int matchedCount = 0;

				if (ptAnswer != null) { // Matched on Question between EU and PT
					int euAnswerCount = euAnswer.getOptionKeys().size();

					for (String euOptionKey : euAnswer.getOptionKeys()) {
						for (String ptOptionKey : ptAnswer.getOptionKeys()) {
							if (euOptionKey.equalsIgnoreCase(ptOptionKey)) { // Count matched on answer between EU and PT
								matchedCount++;
								break;
							}
						}
					}

					if(matchedCount > 0) {
						ptiMatchValue[0] += (questionWeightValue[0]/euAnswerCount)*matchedCount;
					}
				}
			}
		});

		return ptiMatchValue[0];
	}
	
	public UserPtiMatchEntity calculatePtiMatchBetweenEUAndPT(final String euUuid, final List<Answers> euAnswers, final String ptUuid, final List<Answers> ptAnswers,
															final List<QuestionGroupEntity> questionGroups) {
		Map<String, Answers> mapOfPTAnswers = new HashMap<String, Answers>();
		ptAnswers.forEach(ptAnswer -> {
			mapOfPTAnswers.put(ptAnswer.getQuestionId(), ptAnswer);
		});
		
		Map<String, Answers> mapOfEUAnswers = new HashMap<String, Answers>();
		euAnswers.forEach(euAnswer -> {
			mapOfEUAnswers.put(euAnswer.getQuestionId(), euAnswer);
		});
		
		UserPtiMatchEntity userPTIMatch = this.calculatePtiMatch(mapOfEUAnswers, mapOfPTAnswers, questionGroups);
		userPTIMatch.setEu_uuid(euUuid);
		userPTIMatch.setPt_uuid(ptUuid);
		
		return userPTIMatch;
	}
	
	public UserPtiMatchEntity setDefaultMaxMatching(final String euUuid,  final String ptUuid) {
		UserPtiMatchEntity userPTIMatch = new UserPtiMatchEntity();
		userPTIMatch.setAverage(1d);
		userPTIMatch.setPersonality(1d);
		userPTIMatch.setTrainingStyle(1d);
		userPTIMatch.setInterest(1d);
		userPTIMatch.setEu_uuid(euUuid);
		userPTIMatch.setPt_uuid(ptUuid);
		
		return userPTIMatch;
	}
}
