package asia.cmg.f8.counter.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import asia.cmg.counter.thread.CalculatePTIMatchRunnable;
import asia.cmg.f8.common.dto.ApiRespObject;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.security.annotation.RequiredAdminRole;
import asia.cmg.f8.common.spec.user.UserType;
import asia.cmg.f8.common.util.PagedUserGridResponse;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.counter.client.AnswerClient;
import asia.cmg.f8.counter.client.QuestionGroupClient;
import asia.cmg.f8.counter.client.UserClient;
import asia.cmg.f8.counter.collection.Answers;
import asia.cmg.f8.counter.collection.QuestionGroups;
import asia.cmg.f8.counter.config.CounterProperties;
import asia.cmg.f8.counter.dto.MigrationPTIRequestDto;
import asia.cmg.f8.counter.dto.UserDTO;
import asia.cmg.f8.counter.entity.QuestionGroupEntity;
import asia.cmg.f8.counter.entity.QuestionWeightEntity;
import asia.cmg.f8.counter.entity.UserPtiMatchEntity;
import asia.cmg.f8.counter.repository.QuestionGroupRepository;
import asia.cmg.f8.counter.repository.UserPtiMatchRepository;
import asia.cmg.f8.counter.service.AnswersCollectionService;
import asia.cmg.f8.counter.service.PTIMatchingService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;

@RestController
public class MigrationDataApi {

//	@Autowired
//	private QuestionGroupRepository questionGroupRepo;
	
	@Autowired
	private UserPtiMatchRepository userPtiMatchRepo;
	
//	@Autowired
//	private QuestionGroupClient questionGroupClient;
	
	@Autowired
	private UserClient userClient;
	
	@Autowired
	private AnswersCollectionService answerService;
	
	@Autowired
	private PTIMatchingService ptiMatchingService;
	
	@Autowired
	private QuestionGroupRepository questionGroupRepo;
	
	@Autowired
	private CounterProperties counterProperties;
	
	private final String getEuQuery = "select * where userType = 'eu' and activated = true";
	private final String getPtQuery = "select * where userType = 'pt' and activated = true and status.document_status = 'approved'";
	private final String USER_FILTERED_ANSWERS_QUERY = "select * where owner='%s' and (not deleted = true) and (questionId = 'District' or questionId = 'Gender' or questionId = 'Age')";
	private final String USER_ANSWERED_QUERY = "select * where owner='%s'";
	
	private final int PT_LIMIT = 100;
	private final int EU_LIMIT = 1000;
	
	private final String AGE_QUESTION_KEY = "Age";
    private final String GENDER_QUESTION_KEY = "Gender";
    private final String DISTRICT_QUESTION_KEY = "District";
    
    private final String AGE_DOESNOTMATTER_ANSWER_KEY = "AgeA3";
    private final String GENDER_DOESNOTMATTER_ANSWER_KEY = "GenderA3";
	
	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
//	@RequestMapping(value = "/mobile/v1/migration/question_group", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
//	public ResponseEntity<Object> migrateQuestionGroupCollection(final Account account) {
//		try {
//			String ugQuery = "select *";
//			List<QuestionGroups> questionGroups = questionGroupClient.getByQuery(ugQuery).getEntities();
//			questionGroups.forEach(ugQuestionGroup -> {
//				QuestionGroupEntity questionGroupEntity = this.convert(ugQuestionGroup);
//				questionGroupRepo.saveAndFlush(questionGroupEntity);
//			});
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		
//		return new ResponseEntity<Object>(HttpStatus.OK);
//	}
	
//	@RequestMapping(value = "/mobile/v1/migration/user_pti_match/eu/{eu_uuid}/pt/{pt_uuid}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
//	public ResponseEntity<Object> migrateUserPtiMatch(@PathVariable("eu_uuid") final String euUuid,
//													  @PathVariable("pt_uuid") final String ptUuid, final Account account) {
//		try {
////			String euUuid = "8dcd54db-d8ee-11e8-b68e-0a580a800094";	// hue
////			String ptUuid = "128f5031-d8f1-11e8-b68e-0a580a800094"; // HuePT long long long long long
//			
//			List<Answers> euAnswers = ptiMatchingService.getAnswersByUserUuid(account.ugAccessToken(), euUuid);
//			List<Answers> ptAnswers = ptiMatchingService.getAnswersByUserUuid(account.ugAccessToken(), ptUuid);
//			List<QuestionGroupEntity> questionGroups = questionGroupRepo.findAll();
//			
//			Map<String, Answers> mapOfPTAnswers = new HashMap<String, Answers>();
//			ptAnswers.forEach(ptAnswer -> {
//				mapOfPTAnswers.put(ptAnswer.getQuestionId(), ptAnswer);
//			});
//			
//			UserPtiMatch userPTIMatch = ptiMatchingService.calculatePtiMatch(euAnswers, mapOfPTAnswers, questionGroups);
//			userPTIMatch.setEu_uuid(euUuid);
//			userPTIMatch.setPt_uuid(ptUuid);
//			
//			return new ResponseEntity<Object>(userPTIMatch, HttpStatus.OK);
//			
//		} catch (Exception e) {
//			return new ResponseEntity<Object>(HttpStatus.OK);
//		}
//	}
	
//	@RequestMapping(value = "/mobile/v1/export/user_answer_count", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
//	public ResponseEntity<Object> exportEuAndAnswerCount(final Account account) {
//		String cursor = null;
//		String filename = "EUAnserCount.csv";
//		File file = new File(filename);
//		
//		try {
//			FileOutputStream fos = new FileOutputStream(file,true);
//			do {
//				PagedUserGridResponse<UserDTO> pagedEUs = userClient.getUser(getEuQuery, EU_LIMIT, cursor);
//				if(pagedEUs != null) {
//					cursor = pagedEUs.getCursor();
//					List<UserDTO> euList = pagedEUs.getEntities();
//					euList.forEach(eu -> {
//						UserGridResponse<Answers> response = answerClient.getAnswersByUser(String.format("select * where owner = '%s'", eu.getUuid()));
//						if(response != null) {
//							int count = response.getCount();
//							String line = String.format("%s,%s,%s,%s\n",  eu.getUuid(), eu.getName(), eu.getUsername(), count);
//							try {
//								fos.write(line.getBytes());
//							} catch (Exception e) {
//								LOGGER.info("Error on writing for line [{}]", line);
//							} 
//						}
//					});
//				}
//			} while(cursor != null);
//		} catch (Exception e) {
//			LOGGER.info("Error on get user [{}] with cursor [{}]", e.getMessage(), cursor);
//		} 
//		
//		return new ResponseEntity<Object>(HttpStatus.OK);
//	}
	
	/**
	 * Only for DEV checking
	 * @param euUuid
	 * @param ptUuid
	 * @return
	 */
	@RequestMapping(value = "/mobile/v1/user_ptimatch/check", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> checkPTI(@RequestParam(value = "eu_uuid") final String euUuid,
											@RequestParam(value = "pt_uuid") final String ptUuid) {
		ApiRespObject<Object> apiResponse = new ApiRespObject<Object>();
		apiResponse.setStatus(ErrorCode.SUCCESS);
		
		try {
			List<QuestionGroupEntity> questionGroups = questionGroupRepo.findAll();
			List<Answers> euAnswers = answerService.getAnswersByQuery(String.format(USER_ANSWERED_QUERY, euUuid));
			List<Answers> ptAnswers = answerService.getAnswersByQuery(String.format(USER_ANSWERED_QUERY, ptUuid));
			UserPtiMatchEntity ptimatch = ptiMatchingService.calculatePtiMatchBetweenEUAndPT(euUuid, euAnswers, ptUuid, ptAnswers, questionGroups);
			
			apiResponse.setData(ptimatch);
		} catch (Exception e) {
			apiResponse.setStatus(ErrorCode.FAILED.withDetail(e.getMessage()));
		}
		
		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mobile/v1/pt-imatch/taiger/integration", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> migatePTI(@RequestBody @Valid final List<MigrationPTIRequestDto> users, final Account account) {
		ApiRespObject<Object> apiResponse = new ApiRespObject<Object>();
		apiResponse.setStatus(ErrorCode.SUCCESS);
		
		if(!users.isEmpty())
		{
			try {
				long count = ptiMatchingService.migratePtiResult(users, account);
				apiResponse.setData(count);
				
			} catch (Exception e) {
				LOGGER.error("[migatePTI] failed: ", e);
				apiResponse.setStatus(ErrorCode.FAILED.withDetail(e.getMessage()));
			}
		}
		else {
			apiResponse.setStatus(ErrorCode.FAILED.withDetail("PTI-Match Data is empty!"));
		}
		
		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mobile/v1/migration/user_ptimatch/user/{eu_uuid}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	@RequiredAdminRole
	public ResponseEntity<Object> migratePTIForAnUser(@PathVariable(name = "eu_uuid") final String euUuid,
														@RequestParam(name = "is_recalculate") final Boolean isRecalculate,
														final Account account) {
		try {
			LOGGER.info("[migrateUserPtiMatchBetweenEUAndPT] Processing migration pti-match for EU[{}]", euUuid);
			
			UserGridResponse<UserDTO> ugResponse = userClient.getUser(euUuid);
			if(ugResponse != null && ugResponse.getEntities() != null) {
				UserDTO userInfo = ugResponse.getEntities().get(0);
				if(UserType.EU.name().compareToIgnoreCase(userInfo.getUserType()) == 0) {
					CalculatePTIMatchRunnable ptiCalculator = new CalculatePTIMatchRunnable("migratePTIForAnUser", userInfo, isRecalculate, userClient, userPtiMatchRepo, answerService, ptiMatchingService, questionGroupRepo, counterProperties);
					ptiCalculator.run();
				}
			}
		} catch (Exception e) {
			LOGGER.error("[migrateUserPtiMatchBetweenEUAndPT] error: {}", e.getMessage());
			return new ResponseEntity<Object>(Collections.singletonMap("status", "FAILED"), HttpStatus.OK);
		}
		
		return new ResponseEntity<Object>(Collections.singletonMap("status", "SUCCESS"), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mobile/v1/migration/user_ptimatch", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	@RequiredAdminRole
	public ResponseEntity<Object> migrateUserPtiMatchAll(@RequestParam(value = "cursor", required = false) String cursor,
														@RequestParam(value = "limit", required = false, defaultValue = "500") final int limit,
														@RequestParam(value = "corePoolSize", required = false, defaultValue = "50") final int corePoolSize,
														@RequestParam(value = "maximumPoolSize", required = false, defaultValue = "100") final int maximumPoolSize,
														@RequestParam(value = "queueCapacity", required = false, defaultValue = "200") final int queueCapacity,
														final Account account) {
		try {
			boolean isRecalculation = true;
			ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 60, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(queueCapacity));
			
			do {
				LOGGER.info("!!! Processing migration pti-match between EUs and PTs at cursor[{}] !!!", cursor);
				
				PagedUserGridResponse<UserDTO> pagedEUs = userClient.getUser(getEuQuery, limit, cursor);
				
				if(pagedEUs != null) {
					List<UserDTO> euList = pagedEUs.getEntities();
					for (UserDTO eu : euList) {
						String threadName = String.format("Cursor[%s]-EU[%s]", cursor, eu.getUuid());
						executor.execute(new CalculatePTIMatchRunnable(threadName, eu, isRecalculation, userClient, userPtiMatchRepo, answerService, ptiMatchingService, questionGroupRepo, counterProperties));
						Thread.sleep(5*1000);
					}
					cursor = pagedEUs.getCursor();
				} else {
					LOGGER.info("Error getting EUs on query [{}] at cursor [{}]", getEuQuery, cursor);
				}
			} while(cursor != null);
		} catch (Exception e) {
			LOGGER.error("[migrateUserPtiMatch] error: {}", e.getMessage());
			return new ResponseEntity<Object>(Collections.singletonMap("status", "FAILED"), HttpStatus.OK);
		}
		
		return new ResponseEntity<Object>(Collections.singletonMap("status", "SUCCESS"), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mobile/v1/missed_migration/user_ptimatch", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	@RequiredAdminRole
	public ResponseEntity<Object> migratePTIForMissedUsers(@RequestParam(value = "cursor", required = false) String cursor,
														@RequestParam(value = "limit", required = false, defaultValue = "500") final int limit,
														@RequestParam(value = "corePoolSize", required = false, defaultValue = "50") final int corePoolSize,
														@RequestParam(value = "maximumPoolSize", required = false, defaultValue = "100") final int maximumPoolSize,
														@RequestParam(value = "queueCapacity", required = false, defaultValue = "200") final int queueCapacity,
														final Account account) {
		try {
			boolean isRecalculation = false;
			ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 60, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(queueCapacity));
			
			do {
				LOGGER.info("!!! Reprocessing migration pti-match between EUs and PTs at cursor[{}] !!!", cursor);
				
				PagedUserGridResponse<UserDTO> pagedEUs = userClient.getUser(getEuQuery, limit, cursor);
				
				if(pagedEUs != null) {
					List<UserDTO> euList = pagedEUs.getEntities();
					for (UserDTO eu : euList) {
						int count = userPtiMatchRepo.countByEuUuid(eu.getUuid());
						if(count == 0) {
							String threadName = String.format("Cursor[%s]-EU[%s]", cursor, eu.getUuid());
							executor.execute(new CalculatePTIMatchRunnable(threadName, eu, isRecalculation, userClient, userPtiMatchRepo, answerService, ptiMatchingService, questionGroupRepo, counterProperties));
							Thread.sleep(3*1000);
						}
					}
					cursor = pagedEUs.getCursor();
				} else {
					LOGGER.info("Error getting EUs on query [{}] at cursor [{}]", getEuQuery, cursor);
				}
			} while(cursor != null);
		} catch (Exception e) {
			LOGGER.error("[migrateUserPtiMatch] error: {}", e.getMessage());
			return new ResponseEntity<Object>(Collections.singletonMap("status", "FAILED"), HttpStatus.OK);
		}
		
		return new ResponseEntity<Object>(Collections.singletonMap("status", "SUCCESS"), HttpStatus.OK);
	}
	
//	
//	private boolean calculatePtiMatchForEU(final UserDTO userInfo) {
//		
//		try {
//			List<UserPtiMatch> userPTIMatchList = new ArrayList<UserPtiMatch>();
//			
//			// Return if EU has not answered any questions
//			List<Answers> euAnswers = ptiMatchingService.getAnswersByUserUuid(userInfo.getUuid());
//			if(euAnswers.isEmpty()) {
//				LOGGER.info("EU have no any answers with uuid: {}", userInfo.getUuid());
//				return false;
//			}
//			
//			List<UserDTO> trainers = this.getMatchedTrainers(userInfo);
//			if(!trainers.isEmpty()) {
//				trainers.forEach(pt -> {
//					Optional<UserPtiMatch> userPtiMatchOld = userPtiMatchRepo.getByEuUuidAndPtUuid(userInfo.getUuid(), pt.getUuid());
//					List<Answers> ptAnswers = ptiMatchingService.getAnswersByUserUuid(pt.getUuid());
//					UserPtiMatch userPtiMatchNew = ptiMatchingService.calculatePtiMatchBetweenEUAndPT(userInfo.getUuid(), euAnswers, pt.getUuid(), ptAnswers);
//					
//					// Update if existed otherwise insert
//					if(userPtiMatchOld.isPresent()) {
//						userPtiMatchNew.setId(userPtiMatchOld.get().getId());
//						userPtiMatchNew.setModifiedCount(userPtiMatchOld.get().getModifiedCount() + 1);
//					}
//					
//					// Only insert into DB with average > 0
//					if(userPtiMatchNew.getAverage() > 0d) {
//						userPTIMatchList.add(userPtiMatchNew);
//						LOGGER.info("Generated new UserPtiMatch info: {}", userPtiMatchNew);
//					}
//				});
//				
//				userPtiMatchRepo.save(userPTIMatchList);
//				return true;
//			} else {
//				LOGGER.info("Have no any PTs matched with EU [{}]", userInfo.getUuid());
//			}
//			return false;
//		} catch (Exception e) {
//			LOGGER.error("[calculatePtiMatchForEU] error: {}", e.getMessage());
//			return false;
//		} 
//	}
//	
//	private List<UserDTO> getMatchedTrainers(UserDTO userInfo) {
//		
//    	try {
//    		List<UserDTO> matchedTrainers = new ArrayList<UserDTO>();
//    		List<Answers> userAnswers = answerService.getAnswersByQuery(String.format(this.USER_FILTERED_ANSWERS_QUERY, userInfo.getUuid()));
//    		Map<String, Answers> mapOfUSerAnswer = new HashMap<String, Answers>();
//			userAnswers.forEach(answer -> { mapOfUSerAnswer.put(answer.getQuestionId(), answer);});
//			
//			if(mapOfUSerAnswer.isEmpty()) {
//				return Collections.emptyList();
//			}
//			
//			String cursor = null;
//			do {
//				PagedUserGridResponse<UserDTO> pagedPTList = userClient.getUser(getPtQuery, PT_LIMIT, cursor);
//				if(pagedPTList != null) {
//					List<UserDTO> PTList = pagedPTList.getEntities();
//					for (UserDTO trainerDTO : PTList) {
//						boolean matchedTrainer = true;
//						List<Answers> trainerAnswers = answerService.getAnswersByQuery(String.format(USER_FILTERED_ANSWERS_QUERY, trainerDTO.getUuid()));
//						
//						for (Answers trainerAnswer : trainerAnswers) {
//							Answers userAnswer = mapOfUSerAnswer.get(trainerAnswer.getQuestionId());
//							if(userAnswer != null) {
//								boolean isMatched = this.isMatchingOneOfAnswers(userAnswer, trainerAnswer);
//								if(isMatched == false) {
//									matchedTrainer = false;
//									break;
//								}
//							}
//						}
//						
//						if(matchedTrainer == true) {
//							matchedTrainers.add(trainerDTO);
//						}
//					}
//				}
//			} while (cursor != null);
//			
//			return matchedTrainers;
//		} catch (Exception e) {
//			return null;
//		}
//	}
//	
//	private boolean isMatchingOneOfAnswers(final Answers userAnswer, final Answers trainerAnswer) {
//    	List<String> userOptionKeys = userAnswer.getOptionKeys();
//    	List<String> trainerOptionKeys = trainerAnswer.getOptionKeys();
//    	
//    	if(userAnswer.getQuestionId().compareToIgnoreCase(trainerAnswer.getQuestionId()) != 0) {
//    		return false;
//    	}
//    	
//    	for (String userOptionKey : userOptionKeys) {
//			for (String trainerOptionKey : trainerOptionKeys) {
//				if(userAnswer.getQuestionId().compareToIgnoreCase(AGE_QUESTION_KEY) == 0 &&
//						userOptionKey.compareToIgnoreCase(AGE_DOESNOTMATTER_ANSWER_KEY) == 0) {
//					return true;
//				}
//				
//				if(userAnswer.getQuestionId().compareToIgnoreCase(GENDER_QUESTION_KEY) == 0 &&
//						userOptionKey.compareToIgnoreCase(GENDER_DOESNOTMATTER_ANSWER_KEY) == 0) {
//					return true;
//				}
//				
//				if(userOptionKey.compareToIgnoreCase(trainerOptionKey) == 0) {
//					return true;
//				}
//			}
//		}
//    	
//    	return false;
//    }
//	
//	private UserPtiMatch calculatePtiMatch(final String euUuid, final String ptUuid, Account account) {
//		List<Answers> euAnswers = ptiMatchingService.getAnswersByUserUuid(account.ugAccessToken(), euUuid);
//		List<Answers> ptAnswers = ptiMatchingService.getAnswersByUserUuid(account.ugAccessToken(), ptUuid);
//		List<QuestionGroupEntity> questionGroups = questionGroupRepo.findAll();
//		
//		Map<String, Answers> mapOfPTAnswers = new HashMap<String, Answers>();
//		ptAnswers.forEach(ptAnswer -> {
//			mapOfPTAnswers.put(ptAnswer.getQuestionId(), ptAnswer);
//		});
//		
//		UserPtiMatch userPTIMatch = ptiMatchingService.calculatePtiMatch(euAnswers, mapOfPTAnswers, questionGroups);
//		userPTIMatch.setEu_uuid(euUuid);
//		userPTIMatch.setPt_uuid(ptUuid);
//		
//		return userPTIMatch;
//	}
	
//	private QuestionGroupEntity convert(QuestionGroups ugQuestionGroup) {
//		QuestionGroupEntity questionGroupEntity = new QuestionGroupEntity();
//		
//		List<QuestionWeightEntity> questions = new ArrayList<QuestionWeightEntity>();
//		ugQuestionGroup.getQuestions().forEach(ugQuestion -> {
//			QuestionWeightEntity dbQuestion = new QuestionWeightEntity();
//			dbQuestion.setQuestionKey(ugQuestion.getQuestionKey());
//			dbQuestion.setWeight(ugQuestion.getWeight());
//			questions.add(dbQuestion);
//		});
//		questionGroupEntity.setName(ugQuestionGroup.getGroupName());
//		questionGroupEntity.setWeight(ugQuestionGroup.getGroupWeight());
//		questionGroupEntity.setQuestions(questions);
//		
//		return questionGroupEntity;
//	}
}
