package asia.cmg.f8.counter.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface EventStream {

	String COMPLETE_PROFILE_IN_CHANNEL = "completeProfileIn";
	
	String ADMIN_APPROVE_DOCUMENT_IN_CHANNEL = "adminApprovedDocumentIn";
	
	String UNAPPROVE_DOCUMENT_IN_CHANNEL = "adminUnapprovedDocumentIn";
	
	@Input(COMPLETE_PROFILE_IN_CHANNEL)
    SubscribableChannel handleCompleteProfile();
	
	@Input(ADMIN_APPROVE_DOCUMENT_IN_CHANNEL)
	SubscribableChannel handleAdminAprroveDocument();
	
	@Input(UNAPPROVE_DOCUMENT_IN_CHANNEL)
	SubscribableChannel handleAdminUnaprroveDocument();
}
