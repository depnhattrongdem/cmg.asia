package asia.cmg.f8.counter.dto;

import asia.cmg.f8.counter.entity.Counter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 2/14/17.
 */
public class CounterDto {
    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private Integer value;

    public CounterDto() {
        //Empty constructor
    }

    public CounterDto(final Counter counter) {
        this.name = counter.getName();
        this.value = counter.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(final Integer value) {
        this.value = value;
    }
}
