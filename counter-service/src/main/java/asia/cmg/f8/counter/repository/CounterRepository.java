package asia.cmg.f8.counter.repository;

import asia.cmg.f8.counter.entity.Counter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created on 1/19/17.
 */
@Repository
public interface CounterRepository extends JpaRepository<Counter, String> {

    String COUNT_NAME = "countName";
    String COUNT_VALUE = "countValue";

    @Query(value = "INSERT INTO counter_counters (counter_name,counter_value) values(:countName, 1) ON DUPLICATE KEY UPDATE counter_value=counter_value+1", nativeQuery = true)
    @Modifying
    int increaseCounter(@Param(COUNT_NAME) final String name);
    
    @Query(value = "INSERT INTO counter_counters (counter_name,counter_value) values(:countName, :countValue) ON DUPLICATE KEY UPDATE counter_value = :countValue", nativeQuery = true)
    @Modifying
    int updateCounter(@Param(COUNT_NAME) final String name, @Param(COUNT_VALUE) final int value);

    @Query(value = "INSERT INTO counter_counters (counter_name,counter_value) values(:countName, 0) ON DUPLICATE KEY UPDATE counter_value=counter_value-1", nativeQuery = true)
    @Modifying
    int decreaseCounter(@Param(COUNT_NAME) final String name);

    @Query(value = "INSERT INTO counter_counters (counter_name,counter_value) values(:countName, 0) ON DUPLICATE KEY UPDATE counter_value=0", nativeQuery = true)
    @Modifying
    int resetCounter(@Param(COUNT_NAME) final String name);

    @Query(value = "INSERT INTO counter_counters (counter_name, counter_value) values(:countName, :countValue) ON DUPLICATE KEY UPDATE counter_value = :countValue", nativeQuery = true)
    @Modifying
    int randomCounter(@Param(COUNT_NAME) final String name, @Param(COUNT_VALUE) final int value);

    @Query(value = "SELECT counter_name,counter_value FROM counter_counters where counter_name IN (:countName)", nativeQuery = true)
    List<Counter> getCounters(@Param(COUNT_NAME) final Collection<String> counterNames);
}
