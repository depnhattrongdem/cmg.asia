package asia.cmg.f8.counter.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import asia.cmg.f8.common.dto.ApiRespListObject;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.counter.entity.QuestionGroupEntity;
import asia.cmg.f8.counter.repository.QuestionGroupRepository;

@RestController
public class QuestionGroupApi {

	@Autowired
	QuestionGroupRepository questionGroupRepo;
	
	@RequestMapping(value = "/mobile/v1/question_groups", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getQuestionGroup(final Account account) {
		ApiRespListObject<QuestionGroupEntity> apiResponse = new ApiRespListObject<QuestionGroupEntity>();
		apiResponse.setStatus(ErrorCode.SUCCESS);
		try {
			List<QuestionGroupEntity> questionGroups = questionGroupRepo.findAll();
			apiResponse.setData(questionGroups);
		} catch (Exception e) {
			apiResponse.setStatus(ErrorCode.FAILED.withDetail(e.getMessage()));
		}
		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
	}
}
