package asia.cmg.f8.counter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import asia.cmg.f8.counter.entity.QuestionWeightEntity;

@Repository
public interface QuestionWeightRepository extends JpaRepository<QuestionWeightEntity, Integer> {

}
