package asia.cmg.f8.counter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "question_weight")
public class QuestionWeightEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "question_key", length = 255)
	private String questionKey;
	
	@Column(name = "weight")
	private Double weight;
	
	@ManyToOne
	@JoinColumn(name = "question_group_id", nullable = false)
	@JsonIgnore
	private QuestionGroupEntity questionGroup;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQuestionKey() {
		return questionKey;
	}

	public void setQuestionKey(String questionKey) {
		this.questionKey = questionKey;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public QuestionGroupEntity getQuestionGroup() {
		return questionGroup;
	}

	public void setQuestionGroup(QuestionGroupEntity questionGroup) {
		this.questionGroup = questionGroup;
	}
}
