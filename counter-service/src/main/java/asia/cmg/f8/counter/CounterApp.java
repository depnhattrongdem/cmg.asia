package asia.cmg.f8.counter;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;


/**
 * Created on 9/28/16.
 */
@Configuration
@EnableAutoConfiguration
@EnableFeignClients
@SpringBootApplication
@EnableConfigurationProperties
@EnableBinding
@SuppressWarnings("PMD")
public class CounterApp {

    public static void main(final String[] args) {
        new SpringApplicationBuilder(CounterApp.class).web(true).run(args);
    }
}

