package asia.cmg.f8.counter.client;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.counter.collection.QuestionGroups;

@FeignClient(value = "questionGroups", url = "${feign.url}", fallback = QuestionGroupClientFallbackImpl.class)
public interface QuestionGroupClient {

	String SECRECT_QUERY = "client_id=${userGrid.userGridClientId}&client_secret=${userGrid.userGridClientSecret}";
    final String QUERY = "query";
    final String LIMIT = "limit";
    
    @RequestMapping(value = "/question_groups?ql={query}&" + SECRECT_QUERY, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<QuestionGroups> getByQuery(@PathVariable(QUERY) final String query);
}
	