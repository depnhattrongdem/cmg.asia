package asia.cmg.f8.counter.dto;

import java.util.List;

import asia.cmg.f8.counter.collection.Answers;

public class UserDTO {
	private String uuid;
	private String name;
	private String username;
	private String email;
	private String userType;
	private Boolean activated;
	private List<Answers> answers;
	private UserPTIMatchType matchedType = UserPTIMatchType.FILTERED;

	public List<Answers> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answers> answers) {
		this.answers = answers;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public UserPTIMatchType getMatchedType() {
		return matchedType;
	}

	public void setMatchedType(UserPTIMatchType matchedType) {
		this.matchedType = matchedType;
	}
}