package asia.cmg.f8.counter.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.counter.client.AnswerClient;
import asia.cmg.f8.counter.collection.Answers;

@Service
public class AnswersCollectionService {

	@Autowired
	private AnswerClient answerClient;
	
	
	public List<Answers> getAnswersByQuery(final String query) {
		try {
			UserGridResponse<Answers> answers = answerClient.getAnswersByQuery(query);
			return (answers == null || answers.isEmpty()) ? Collections.emptyList() : answers.getEntities();
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}
	
	public List<Answers> getAnswersByQuery(final String query, final int limit) {
		try {
			UserGridResponse<Answers> answers = answerClient.getAnswersByQuery(query, limit);
			return (answers == null || answers.isEmpty()) ? Collections.emptyList() : answers.getEntities();
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}
}
