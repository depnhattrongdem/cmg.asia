package asia.cmg.f8.counter.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "counter")
public class CounterProperties {

	private String[] listTrainersMaxMatching;

	public String[] getListTrainersMaxMatching() {
		return listTrainersMaxMatching;
	}

	public void setListTrainersMaxMatching(String[] listTrainersMaxMatching) {
		this.listTrainersMaxMatching = listTrainersMaxMatching;
	}
}
