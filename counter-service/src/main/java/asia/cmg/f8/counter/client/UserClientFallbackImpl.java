package asia.cmg.f8.counter.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import asia.cmg.f8.common.util.PagedUserGridResponse;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.counter.dto.UserDTO;

@Component
public class UserClientFallbackImpl implements UserClient{

    private static final Logger LOG = LoggerFactory.getLogger(UserClientFallbackImpl.class);

    @Override
    public UserGridResponse<UserDTO> getUser(final String uuid) {
        LOG.error("Could not get getUser with uuid [{}]", uuid);
        return new UserGridResponse<>();
    }

    @Override
    public UserGridResponse<UserDTO> getUser(final String query, final Integer limit) {
        LOG.error("Could not get getUserByQuery with query [{}]", query);
        return new UserGridResponse<>();
    }

	@Override
	public PagedUserGridResponse<UserDTO> getUser(String query, Integer limit, String cursor) {
		LOG.error("Could not get getUserByQuery with query [{}] and cursor [{}]", query, cursor);
		return null;
	}
}
