package asia.cmg.f8.counter.service;

import asia.cmg.f8.counter.entity.Counter;
import asia.cmg.f8.counter.repository.CounterRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 1/19/17.
 */
@Service
public class CounterService {

    private final CounterRepository counterRepository;

    public CounterService(final CounterRepository counterRepository) {
        this.counterRepository = counterRepository;
    }

    @Transactional(readOnly = true)
    public Optional<Counter> findOne(final String name) {
        return Optional.ofNullable(counterRepository.findOne(name));
    }

    public Optional<List<Counter>> findByNames(final Set<String> counterNames) {
        return Optional.ofNullable(counterRepository.getCounters(counterNames));
    }

    @Transactional
    public void resetCounter(final String name) {
        counterRepository.resetCounter(name);
    }

    @Transactional
    public void inCreaseCounter(final String name) {
        counterRepository.increaseCounter(name);
    }
    
    @Transactional
    public void randomCounter(final String name, final int value) {
        counterRepository.randomCounter(name, value);
    }

    @Transactional
    public void decreaseCounter(final String name) {
        counterRepository.decreaseCounter(name);
    }
    
    @Transactional
    public void updateCounter(final String name, final int value) {
        counterRepository.updateCounter(name, value);
    }
}
