package asia.cmg.f8.counter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created on 1/19/17.
 */
@Entity
@Table(name = "counter_counters")
public class Counter {

    @Id
    @Column(name = "counter_name", unique = true)
    private String name; // counter name

    @Column(name = "counter_value")
    private int value;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(final int value) {
        this.value = value;
    }
}
