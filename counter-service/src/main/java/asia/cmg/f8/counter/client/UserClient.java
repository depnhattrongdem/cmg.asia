package asia.cmg.f8.counter.client;

import asia.cmg.f8.common.util.PagedUserGridResponse;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.counter.dto.UserDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(value = "users", url = "${feign.url}", fallback = UserClientFallbackImpl.class)
public interface UserClient {

    String QUERY = "query";
    String LIMIT = "limit";
    String UUID = "uuid";
    String CURSOR = "cursor";
    String SECRECT_QUERY = "client_id=${userGrid.userGridClientId}&client_secret=${userGrid.userGridClientSecret}";
    
    @RequestMapping(value = "/users/{uuid}?" + SECRECT_QUERY, method = RequestMethod.GET, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<UserDTO> getUser(@PathVariable("uuid") String uuid);

    @RequestMapping(value = "/users?limit={limit}&ql={query}&" + SECRECT_QUERY, method = RequestMethod.GET, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<UserDTO> getUser(@PathVariable(QUERY) final String query, @PathVariable(LIMIT) final Integer limit);
    
    @RequestMapping(value = "/users?cursor={cursor}&limit={limit}&ql={query}&" + SECRECT_QUERY, method = RequestMethod.GET, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    PagedUserGridResponse<UserDTO> getUser(@PathVariable(QUERY) final String query, @PathVariable(LIMIT) final Integer limit, @PathVariable(CURSOR) final String cursor);
}
