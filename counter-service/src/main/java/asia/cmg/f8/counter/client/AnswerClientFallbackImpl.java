package asia.cmg.f8.counter.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.counter.collection.Answers;

@Component
public class AnswerClientFallbackImpl implements AnswerClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(AnswerClientFallbackImpl.class);
	
	@Override
	public UserGridResponse<Answers> getAnswersByQuery(final String query, int limit) {
		LOGGER.error("Error on getAnswersByQuery({}, {})", query, limit);
		return null;
	}

	@Override
	public UserGridResponse<Answers> getAnswersByQuery(final String query) {
		LOGGER.error("Error on getAnswersByQuery({})", query);
		return null;
	}
}
