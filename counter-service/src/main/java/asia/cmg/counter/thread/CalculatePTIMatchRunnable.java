package asia.cmg.counter.thread;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import asia.cmg.f8.common.util.PagedUserGridResponse;
import asia.cmg.f8.counter.client.UserClient;
import asia.cmg.f8.counter.collection.Answers;
import asia.cmg.f8.counter.config.CounterProperties;
import asia.cmg.f8.counter.dto.UserDTO;
import asia.cmg.f8.counter.dto.UserPTIMatchType;
import asia.cmg.f8.counter.entity.QuestionGroupEntity;
import asia.cmg.f8.counter.entity.UserPtiMatchEntity;
import asia.cmg.f8.counter.repository.QuestionGroupRepository;
import asia.cmg.f8.counter.repository.UserPtiMatchRepository;
import asia.cmg.f8.counter.service.AnswersCollectionService;
import asia.cmg.f8.counter.service.PTIMatchingService;

public class CalculatePTIMatchRunnable implements Runnable {

	private String name;
	private UserDTO userInfo;
	private boolean isRecalculation = false;
	private final UserClient userClient;
	private final UserPtiMatchRepository userPtiMatchRepo;
	private final AnswersCollectionService answerService;
	private final PTIMatchingService ptiMatchingService;
	private final QuestionGroupRepository questionGroupRepo;
	private final CounterProperties counterProperties;
	
	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private final String getEuQuery = "select * where userType = 'eu' and activated = true";
	private final String getPtQuery = "select * where userType = 'pt' and activated = true and status.document_status = 'approved'";
	private final String USER_ANSWERED_QUERY = "select * where owner='%s'";

	private final int PT_LIMIT = 500;
	private final int EU_LIMIT = 500;

	private final String AGE_QUESTION_KEY = "Age";
	private final String GENDER_QUESTION_KEY = "Gender";
	private final String DISTRICT_QUESTION_KEY = "District";

	private final String AGE_DOESNOTMATTER_ANSWER_KEY = "AgeA3";
	private final String GENDER_DOESNOTMATTER_ANSWER_KEY = "GenderA3";

	public CalculatePTIMatchRunnable(String threadName, UserDTO userInfo, boolean isRecalculation,
							final UserClient userClient,
							final UserPtiMatchRepository userPtiMatchRepo,
							final AnswersCollectionService answerService,
							final PTIMatchingService ptiMatchingService,
							final QuestionGroupRepository questionGroupRepo,
							final CounterProperties counterProperties) {
		this.name = threadName;
		this.userInfo = userInfo;
		this.isRecalculation = isRecalculation;
		this.userClient = userClient;
		this.userPtiMatchRepo = userPtiMatchRepo;
		this.answerService = answerService;
		this.ptiMatchingService = ptiMatchingService;
		this.questionGroupRepo = questionGroupRepo;
		this.counterProperties = counterProperties;
	}
	
	public CalculatePTIMatchRunnable(final UserClient userClient,
									boolean isRecalculation,
									final UserPtiMatchRepository userPtiMatchRepo,
									final AnswersCollectionService answerService,
									final PTIMatchingService ptiMatchingService,
									final QuestionGroupRepository questionGroupRepo,
									final CounterProperties counterProperties) {
		this.userClient = userClient;
		this.isRecalculation = isRecalculation;
		this.userPtiMatchRepo = userPtiMatchRepo;
		this.answerService = answerService;
		this.ptiMatchingService = ptiMatchingService;
		this.questionGroupRepo = questionGroupRepo;
		this.counterProperties = counterProperties;
	}

	@Override
	public void run() {
		LOGGER.info("Starting thread [{}] to calculate PTI Matching for user uuid [{}]", name, userInfo.getUuid());
		boolean result = false;

		if(isRecalculation) {
			result = this.recalculatePtiMatchForEU(userInfo);
		} else {
			result = this.calculatePtiMatchForNewEU(userInfo);
		}
		
		if(result) {
			LOGGER.info("Calculate PTI Matching successfully for user [{}]", userInfo.getUuid());
		} else {
			LOGGER.info("Calculate PTI Matching failed for user [{}]", userInfo.getUuid());
		}
	}
	
	private List<UserDTO> getMatchedUsers(UserDTO trainerInfo) {
		try {
			List<UserDTO> matchedUsers = new ArrayList<UserDTO>();
			String cursor = null;
			
			List<Answers> trainerAnswers = answerService.getAnswersByQuery(String.format(this.USER_ANSWERED_QUERY, trainerInfo.getUuid()));
			Map<String, Answers> mapOfTrainerAnswers = new HashMap<String, Answers>();
			trainerAnswers.forEach(answer -> {
				if(AGE_QUESTION_KEY.compareToIgnoreCase(answer.getQuestionId()) == 0 || 
						GENDER_QUESTION_KEY.compareToIgnoreCase(answer.getQuestionId()) == 0 ||
						DISTRICT_QUESTION_KEY.compareToIgnoreCase(answer.getQuestionId()) == 0) {
					mapOfTrainerAnswers.put(answer.getQuestionId(), answer);
				}
			});
			
			if(mapOfTrainerAnswers.isEmpty()) {
				LOGGER.info("[getMatchedUsers] Trainer [{}] has no any answers", trainerInfo.getUuid());
				return Collections.emptyList();
			}
			
			do {
				PagedUserGridResponse<UserDTO> ugResponse = userClient.getUser(getEuQuery, EU_LIMIT, cursor);
				if (ugResponse != null) {
					boolean matchedUser = false;
					List<UserDTO> users = ugResponse.getEntities();
					cursor = ugResponse.getCursor();
					for (UserDTO userDTO : users) {
						List<Answers> userAnswers = answerService.getAnswersByQuery(String.format(this.USER_ANSWERED_QUERY, userDTO.getUuid()));
						
						for (Answers userAnswer : userAnswers) {
							Answers trainerAnswer = mapOfTrainerAnswers.get(userAnswer.getQuestionId());
							if(trainerAnswer != null) {
								boolean isMatched = this.isMatchingOneOfAnswers(userAnswer, trainerAnswer);
								if (isMatched == false) {
									matchedUser = isMatched;
									break;
								}
								matchedUser = isMatched;
							}
						}
						
						if(matchedUser) {
							userDTO.setAnswers(userAnswers);
							matchedUsers.add(userDTO);
						}
					}
				}
			} while (cursor != null);
			
			return matchedUsers;
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}

	private List<UserDTO> getMatchedTrainers(String euUuid, List<Answers> userAnswers) {

		try {
			String cursor = null;
			List<UserDTO> matchedTrainers = new ArrayList<UserDTO>();
			Map<String, Answers> mapOfUSerAnswer = new HashMap<String, Answers>();
			userAnswers.forEach(answer -> {
				if(AGE_QUESTION_KEY.compareToIgnoreCase(answer.getQuestionId()) == 0 || 
					GENDER_QUESTION_KEY.compareToIgnoreCase(answer.getQuestionId()) == 0 ||
					DISTRICT_QUESTION_KEY.compareToIgnoreCase(answer.getQuestionId()) == 0) {
					mapOfUSerAnswer.put(answer.getQuestionId(), answer);
				}
			});

			if (mapOfUSerAnswer.isEmpty()) {	// EU does not answer all of 3 hard filter questions will be marked as NO_FILTERED pti-match
				LOGGER.info("EU progress matching NO_FILTERED [{}]", euUuid);
				do {
					PagedUserGridResponse<UserDTO> pagedPTList = userClient.getUser(getPtQuery, PT_LIMIT, cursor);
					if (pagedPTList != null) {
						List<UserDTO> trainers = pagedPTList.getEntities();
						cursor = pagedPTList.getCursor();
						trainers.forEach(trainer -> {
							List<Answers> trainerAnswers = answerService.getAnswersByQuery(String.format(USER_ANSWERED_QUERY, trainer.getUuid()));
							trainer.setAnswers(trainerAnswers);
							trainer.setMatchedType(UserPTIMatchType.NO_FILTERED);
						});
						matchedTrainers.addAll(trainers);
					}
				} while (cursor != null);
				
				return matchedTrainers;
			} else {
				do {
					PagedUserGridResponse<UserDTO> pagedPTList = userClient.getUser(getPtQuery, PT_LIMIT, cursor);
					if (pagedPTList != null) {
						List<UserDTO> PTList = pagedPTList.getEntities();
						cursor = pagedPTList.getCursor();
						for (UserDTO trainerDTO : PTList) {
							boolean matchedTrainer = false;
							List<Answers> trainerAnswers = answerService.getAnswersByQuery(String.format(USER_ANSWERED_QUERY, trainerDTO.getUuid()));

							for (Answers trainerAnswer : trainerAnswers) {
								Answers userAnswer = mapOfUSerAnswer.get(trainerAnswer.getQuestionId());
								if (userAnswer != null) {
									boolean isMatched = this.isMatchingOneOfAnswers(userAnswer, trainerAnswer);
									if (isMatched == false) {
										matchedTrainer = isMatched;
										break;
									}
									matchedTrainer = isMatched;
								}
							}

							if (matchedTrainer == true) {
								trainerDTO.setAnswers(trainerAnswers);
								matchedTrainers.add(trainerDTO);
							}
						}
					}
				} while (cursor != null);

				return matchedTrainers;
			}
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}

	private boolean isMatchingOneOfAnswers(final Answers userAnswer, final Answers trainerAnswer) {
		List<String> userOptionKeys = userAnswer.getOptionKeys();
		List<String> trainerOptionKeys = trainerAnswer.getOptionKeys();

		if (userAnswer.getQuestionId().compareToIgnoreCase(trainerAnswer.getQuestionId()) != 0) {
			return false;
		}

		for (String userOptionKey : userOptionKeys) {
			for (String trainerOptionKey : trainerOptionKeys) {
				if (userAnswer.getQuestionId().compareToIgnoreCase(AGE_QUESTION_KEY) == 0
						&& userOptionKey.compareToIgnoreCase(AGE_DOESNOTMATTER_ANSWER_KEY) == 0) {
					return true;
				}

				if (userAnswer.getQuestionId().compareToIgnoreCase(GENDER_QUESTION_KEY) == 0
						&& userOptionKey.compareToIgnoreCase(GENDER_DOESNOTMATTER_ANSWER_KEY) == 0) {
					return true;
				}

				if (userOptionKey.compareToIgnoreCase(trainerOptionKey) == 0) {
					return true;
				}
			}
		}

		return false;
	}

	public boolean calculatePtiMatchForPT(final UserDTO trainerInfo) {
		try {
			List<UserDTO> matchedUsers = this.getMatchedUsers(trainerInfo);
			List<QuestionGroupEntity> questionGroups = questionGroupRepo.findAll();
			
			// Return if PT has not answered any questions
			List<Answers> ptAnswers = answerService.getAnswersByQuery(String.format(this.USER_ANSWERED_QUERY, trainerInfo.getUuid()));
			if (ptAnswers.isEmpty()) {
				LOGGER.info("[calculatePtiMatchForPT] PT has no any answers with uuid: {}", trainerInfo.getUuid());
				return false;
			}
			
			// Reset all matched values(P,T,I,A) to zero of this PT with all EUs
			userPtiMatchRepo.resetMatchedValuesByPtUuid(trainerInfo.getUuid());
			
			List<UserPtiMatchEntity> listOfPtiMatch = new ArrayList<UserPtiMatchEntity>();
			final int batchUpdateMax = 10;
			int count = 0;
			int index = 0;
			final int countOfMatchedUsers = matchedUsers.size();
			for (UserDTO matchedUser : matchedUsers) {
				index++;
				Optional<UserPtiMatchEntity> userPtiMatchOld = userPtiMatchRepo.getByEuUuidAndPtUuid(matchedUser.getUuid(), trainerInfo.getUuid());
				UserPtiMatchEntity newUserPtiMatch = ptiMatchingService.calculatePtiMatchBetweenEUAndPT(matchedUser.getUuid(), matchedUser.getAnswers(), trainerInfo.getUuid(), ptAnswers, questionGroups);
					
				if(newUserPtiMatch.getAverage() > 0) {	// Only insert/update into DB with average > 0
					count++;
					
					// Update if existed otherwise insert
					if (userPtiMatchOld.isPresent()) {
						UserPtiMatchEntity updatedUserPtiMatch = userPtiMatchOld.get();
						updatedUserPtiMatch.setAverage(newUserPtiMatch.getAverage());
						updatedUserPtiMatch.setInterest(newUserPtiMatch.getInterest());
						updatedUserPtiMatch.setPersonality(newUserPtiMatch.getPersonality());
						updatedUserPtiMatch.setTrainingStyle(newUserPtiMatch.getTrainingStyle());
						updatedUserPtiMatch.setModifiedCount(updatedUserPtiMatch.getModifiedCount() + 1);
						
						listOfPtiMatch.add(updatedUserPtiMatch);
						LOGGER.info("[calculatePtiMatchForPT] Update existed UserPtiMatch info: {}", updatedUserPtiMatch);
					} else {
						listOfPtiMatch.add(newUserPtiMatch);
						LOGGER.info("[calculatePtiMatchForPT] Generate new UserPtiMatch info: {}", newUserPtiMatch);
					}
				}
				
				if(index < countOfMatchedUsers) {
					if(count > 0 && count % batchUpdateMax == 0) {
						userPtiMatchRepo.save(listOfPtiMatch);
						userPtiMatchRepo.flush();
						listOfPtiMatch.clear();
					}
				} else {
					if(count > 0) {
						userPtiMatchRepo.save(listOfPtiMatch);
						userPtiMatchRepo.flush();
						listOfPtiMatch.clear();
					}
				}
			}
			
			return true;
		} catch (Exception e) {
			LOGGER.error("[calculatePtiMatchForPT] error: {}", e.getMessage());
			return false;
		}
	}
	
	public boolean recalculatePtiMatchForEU(final UserDTO userInfo) {

		try {
			List<QuestionGroupEntity> questionGroups = questionGroupRepo.findAll(); 
			List<String> listTrainersMaxMatching = Arrays.asList(counterProperties.getListTrainersMaxMatching());
			listTrainersMaxMatching = listTrainersMaxMatching.stream().map(uuid -> uuid.trim()).collect(Collectors.toList());

			// Return if EU has not answered any questions
			List<Answers> euAnswers = answerService.getAnswersByQuery(String.format(this.USER_ANSWERED_QUERY, userInfo.getUuid()));
			if (euAnswers.isEmpty()) {
				LOGGER.info("[calculatePtiMatchForPT] EU have no any answers with uuid: {}", userInfo.getUuid());
				return false;
			}
			
			// Set all records PTI of this EU is old data 
			userPtiMatchRepo.markedIsOldByEuUuid(userInfo.getUuid());

			List<UserDTO> matchedTrainers = this.getMatchedTrainers(userInfo.getUuid(), euAnswers);
			
			if (!matchedTrainers.isEmpty()) {
				List<UserPtiMatchEntity> listOfPtiMatch = new ArrayList<UserPtiMatchEntity>();
				final int batchUpdateMax = 10;
				int count = 0;
				int index = 0;
				final int countOfMatchedTrainers = matchedTrainers.size();
				
				for (UserDTO matchedTrainer : matchedTrainers) {
					index ++;
					Optional<UserPtiMatchEntity> userPtiMatchOld = userPtiMatchRepo.getByEuUuidAndPtUuid(userInfo.getUuid(), matchedTrainer.getUuid());
					
					UserPtiMatchEntity newUserPtiMatch = new UserPtiMatchEntity();
					if(listTrainersMaxMatching.contains(matchedTrainer.getUuid())) {
						newUserPtiMatch = ptiMatchingService.setDefaultMaxMatching(userInfo.getUuid(), matchedTrainer.getUuid());
					} else {
						newUserPtiMatch = ptiMatchingService.calculatePtiMatchBetweenEUAndPT(userInfo.getUuid(), euAnswers, matchedTrainer.getUuid(), matchedTrainer.getAnswers(), questionGroups);
					}

					if(newUserPtiMatch.getAverage() > 0) {	// Only insert/update into DB with average > 0
						count++;
						
						// Update if existed otherwise insert
						if (userPtiMatchOld.isPresent()) {
							UserPtiMatchEntity updatedUserPtiMatch = userPtiMatchOld.get();
							updatedUserPtiMatch.setAverage(newUserPtiMatch.getAverage());
							updatedUserPtiMatch.setInterest(newUserPtiMatch.getInterest());
							updatedUserPtiMatch.setPersonality(newUserPtiMatch.getPersonality());
							updatedUserPtiMatch.setTrainingStyle(newUserPtiMatch.getTrainingStyle());
							updatedUserPtiMatch.setModifiedCount(updatedUserPtiMatch.getModifiedCount() + 1);
							updatedUserPtiMatch.setMatchedType(matchedTrainer.getMatchedType());
							updatedUserPtiMatch.setIsOld(Boolean.FALSE);
							
							listOfPtiMatch.add(updatedUserPtiMatch);
							LOGGER.info("[calculatePtiMatchForEU] Update existed UserPtiMatch info: {}", updatedUserPtiMatch);
						} else {	
							newUserPtiMatch.setMatchedType(matchedTrainer.getMatchedType());
							listOfPtiMatch.add(newUserPtiMatch);
							LOGGER.info("[calculatePtiMatchForEU] Generate new UserPtiMatch info: {}", newUserPtiMatch);
						}
					}
					
					if(index < countOfMatchedTrainers) {
						if(count > 0 && count % batchUpdateMax == 0) {
							userPtiMatchRepo.save(listOfPtiMatch);
							userPtiMatchRepo.flush();
							listOfPtiMatch.clear();
						}
					} else {
						if(count > 0) {
							userPtiMatchRepo.save(listOfPtiMatch);
							userPtiMatchRepo.flush();
							listOfPtiMatch.clear();
						}
					}
				}
				
				// Delete all records PTI of this EU is old
				userPtiMatchRepo.clearResultByEuUuid(userInfo.getUuid());
				
				return true;
			} else {
				LOGGER.info("[calculatePtiMatchForEU] Have no any PTs matched with EU [{}]", userInfo.getUuid());
			}
			return false;
		} catch (Exception e) {
			LOGGER.error("[calculatePtiMatchForEU] error: {}", e.getMessage());
			return false;
		}
	}
	
	public boolean calculatePtiMatchForNewEU(final UserDTO userInfo) {

		try {
			List<QuestionGroupEntity> questionGroups = questionGroupRepo.findAll(); 
			List<String> listTrainersMaxMatching = Arrays.asList(counterProperties.getListTrainersMaxMatching());
			listTrainersMaxMatching = listTrainersMaxMatching.stream().map(uuid -> uuid.trim()).collect(Collectors.toList());

			// Return if EU has not answered any questions
			List<Answers> euAnswers = answerService.getAnswersByQuery(String.format(this.USER_ANSWERED_QUERY, userInfo.getUuid()));
			if (euAnswers.isEmpty()) {
				LOGGER.info("[calculatePtiMatchForPT] EU have no any answers with uuid: {}", userInfo.getUuid());
				return false;
			}
			
			List<UserDTO> matchedTrainers = this.getMatchedTrainers(userInfo.getUuid(), euAnswers);
			
			if (!matchedTrainers.isEmpty()) {
				List<UserPtiMatchEntity> listOfPtiMatch = new ArrayList<UserPtiMatchEntity>();
				final int batchUpdateMax = 10;
				int count = 0;
				int index = 0;
				final int countOfMatchedTrainers = matchedTrainers.size();
				
				for (UserDTO matchedTrainer : matchedTrainers) {
					index ++;
					
					UserPtiMatchEntity newUserPtiMatch = new UserPtiMatchEntity();
					if(listTrainersMaxMatching.contains(matchedTrainer.getUuid())) {
						newUserPtiMatch = ptiMatchingService.setDefaultMaxMatching(userInfo.getUuid(), matchedTrainer.getUuid());
					} else {
						newUserPtiMatch = ptiMatchingService.calculatePtiMatchBetweenEUAndPT(userInfo.getUuid(), euAnswers, matchedTrainer.getUuid(), matchedTrainer.getAnswers(), questionGroups);
					}

					if(newUserPtiMatch.getAverage() > 0) {	// Only insert/update into DB with average > 0
						count++;
						
						newUserPtiMatch.setMatchedType(matchedTrainer.getMatchedType());
						listOfPtiMatch.add(newUserPtiMatch);
						LOGGER.info("[calculatePtiMatchForEU] Generate new UserPtiMatch info: {}", newUserPtiMatch);
					}
					
					if(index < countOfMatchedTrainers) {
						if(count > 0 && count % batchUpdateMax == 0) {
							userPtiMatchRepo.save(listOfPtiMatch);
							userPtiMatchRepo.flush();
							listOfPtiMatch.clear();
						}
					} else {
						if(count > 0) {
							userPtiMatchRepo.save(listOfPtiMatch);
							userPtiMatchRepo.flush();
							listOfPtiMatch.clear();
						}
					}
				}
				return true;
			} else {
				LOGGER.info("[calculatePtiMatchForEU] Have no any PTs matched with EU [{}]", userInfo.getUuid());
			}
			
			return false;
		} catch (Exception e) {
			LOGGER.error("[calculatePtiMatchForEU] error: {}", e.getMessage());
			return false;
		}
	}
}
