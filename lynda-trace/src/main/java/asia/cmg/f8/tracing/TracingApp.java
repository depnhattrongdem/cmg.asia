package asia.cmg.f8.tracing;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.sleuth.zipkin.stream.EnableZipkinStreamServer;
import org.springframework.context.annotation.Configuration;


/**
 * Created on 9/28/16.
 */
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@EnableZipkinStreamServer
@SuppressWarnings("PMD")
public class TracingApp {

    public static void main(final String[] args) {
        new SpringApplicationBuilder(TracingApp.class).web(true).run(args);
    }
}

