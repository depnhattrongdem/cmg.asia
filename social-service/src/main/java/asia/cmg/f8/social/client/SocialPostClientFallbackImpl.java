package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import rx.Observable;

/**
 * Created on 12/21/16.
 */
@Component
public class SocialPostClientFallbackImpl implements SocialPostClient {
    private static final Logger LOG = LoggerFactory.getLogger(SocialPostClientFallbackImpl.class);

    private static final String FALLBACK_MESSAGE = "Failed! fallback to empty implementation";

    @Override
    public UserGridResponse<ActivityEntity> createActivity(@PathVariable(USER_ID) final String userId, 
    														@RequestBody final ActivityEntity entity) {
        return null;
    }

    @Override
    public PagedResponse<ActivityEntity> getActivityByUserId(
            @PathVariable(USER_ID) final String userId,
            @PathVariable(QUERY) final String query,
            @RequestParam(value = CURSOR, required = false) final String cursor,
            @RequestParam(LIMIT) final int limit) {
        return null;
    }

    @Override
    public Observable<UserGridResponse<ActivityEntity>> getUserLikedPostStatusByQuery(
            @PathVariable(USER_ID) final String userId,
            @PathVariable(QUERY) final String query) {
        return Observable.empty();
    }

    @Override
    public UserGridResponse<ActivityEntity> getUserLikedPostById(
            @PathVariable(USER_ID) final String userId,
            @PathVariable(QUERY) final String query) {
        return null;
    }

    @Override
    public UserGridResponse<ActivityEntity> userLikesPost(
            @PathVariable(USER_ID) final String userId,
            @PathVariable(POST_ID) final String postId) {
        return null;
    }

    @Override
    public UserGridResponse<ActivityEntity> userUnLikesPost(
            @PathVariable(USER_ID) final String userId,
            @PathVariable(POST_ID) final String postId) {
        return null;
    }

    @Override
    public UserGridResponse<ActivityEntity> getActivityByQuery(
            @PathVariable(QUERY) final String query) {
        return null;
    }

    @Override
    public UserGridResponse<ActivityEntity> editPost(
            @PathVariable(POST_ID) final String postId,
            @RequestBody final ActivityEntity updatedActivity) {
        return null;
    }

    @Override
    public UserGridResponse<ActivityEntity> getActivityOfUserByQuery(
            @PathVariable(USER_ID) final String userId,
            @PathVariable(QUERY) final String query) {
        return null;
    }

    @Override
    public PagedResponse<ActivityEntity> getActivitiesByQuery(String query, String cursor, int limit) {
        LOG.warn(FALLBACK_MESSAGE);
        return null;
    }

}
