package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.AttributeEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.RatingSessionEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created on 12/6/16.
 */
@FeignClient(value = "ratingSessions", url = "${feign.url}",
        fallback = RatingSessionClientFallbackImpl.class)
public interface RatingSessionClient {
    String SECRET_QUERY =
            "client_id=${socials.userGridClientId}&client_secret=${socials.userGridClientSecret}";
    String GET_RATING_BY_QUERY = "/rating_sessions?" + SECRET_QUERY;
    String GET_RATING_BY_QUERY_CURSOR = "/rating_sessions?limit={limit}&cursor={cursor}&" + SECRET_QUERY;
    String CREATE_RATING_SESSION_USER = "/rating_sessions?" + SECRET_QUERY;
    String UPDATE_RATING_SESSION_USER = "/rating_sessions/{rating_id}?" + SECRET_QUERY;

    String GET_RATING_SESSION_REASON = "/rating_settings?" + SECRET_QUERY
            + "&ql={query}&limit=${socials.maxSearchResult}";

	@RequestMapping(value = GET_RATING_BY_QUERY_CURSOR, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	PagedResponse<RatingSessionEntity> searchRatingOfUserWithCursor(@RequestParam("query") final String query,
			@RequestParam("cursor") final String cursor, @RequestParam("limit") final int limit);

	@RequestMapping(value = GET_RATING_BY_QUERY, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	PagedResponse<RatingSessionEntity> searchPagingRatingSessionOfUser(@RequestParam("query") final String query,
			@RequestParam("limit") final int limit);

	@RequestMapping(value = CREATE_RATING_SESSION_USER, method = RequestMethod.POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	UserGridResponse<RatingSessionEntity> rateSessionUser(@RequestBody final RatingSessionEntity entity);

    @RequestMapping(value = UPDATE_RATING_SESSION_USER, method = RequestMethod.PUT,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<RatingSessionEntity> updateReasonRatingSession(
            @PathVariable("rating_id") final String ratingSessionUserId,
            @RequestBody final Map<String, List<String>> reasons);

    @RequestMapping(value = GET_RATING_SESSION_REASON, method = RequestMethod.GET,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<AttributeEntity> getRatingReason(@PathVariable(value = "query") String query);
    
    @RequestMapping(value = "/users/{pt_uuid}/rating_sessions/{session_uuid}?client_id=${socials.userGridClientId}&client_secret=${socials.userGridClientSecret}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    PagedResponse<RatingSessionEntity> getRatingSessions(@PathVariable("pt_uuid") final String ptUuid,@PathVariable("session_uuid") final String session_uuid);
    
    
}
