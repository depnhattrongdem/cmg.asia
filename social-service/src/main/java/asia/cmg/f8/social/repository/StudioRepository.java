package asia.cmg.f8.social.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import asia.cmg.f8.social.database.entity.StudioEntity;
import asia.cmg.f8.social.dto.StudioDto;

@Repository
public interface StudioRepository extends JpaRepository<StudioEntity, Integer> {

	/**
	 * @param id
	 * @param uuid
	 * @param name
	 * @param address
	 * @param longitude
	 * @param latitude
	 * @param image
	 * @param logo
	 * @param facebook
	 * @param website
	 * @param phone
	 * @param checkinCredit
	 * @param openAt
	 * @param closeAt
	 * @param active
	 */
	final String GET_ACTIVE_STUDIOS_QUERY 
	        = "SELECT " 
	        + "new asia.cmg.f8.social.dto.StudioDto("
	    	+ "studio.id,"
	    	+ "studio.uuid,"
	    	+ "studio.name,"
	    	+ "studio.address,"
	    	+ "studio.longitude,"
	    	+ "studio.latitude,"
	    	+ "studio.image,"
	    	+ "studio.logo,"
	    	+ "studio.facebook,"
	    	+ "studio.website,"
	    	+ "studio.phone,"
	    	+ "studio.checkinCredit,"
	    	+ "studio.openAt,"
	    	+ "studio.closeAt,"
	    	+ "studio.categories)"
			+ " FROM StudioEntity studio"
	        + " WHERE studio.active =:active";
	@Query(value = GET_ACTIVE_STUDIOS_QUERY)
	List<StudioDto> getStudios(@Param("active") Boolean isActive,Pageable pageable);
	
	final String GET_STUDIO_QUERY_BY_UUID 
    = "SELECT " 
    + "new asia.cmg.f8.social.dto.StudioDto("
	+ "studio.id,"
	+ "studio.uuid,"
	+ "studio.name,"
	+ "studio.address,"
	+ "studio.longitude,"
	+ "studio.latitude,"
	+ "studio.image,"
	+ "studio.logo,"
	+ "studio.facebook,"
	+ "studio.website,"
	+ "studio.phone,"
	+ "studio.checkinCredit,"
	+ "studio.openAt,"
	+ "studio.closeAt,"
	+ "studio.categories)"
	+ " FROM StudioEntity studio"
    + " WHERE studio.uuid =:studio_uuid";
	@Query(value = GET_STUDIO_QUERY_BY_UUID)
	StudioDto getStudioByUuid(@Param("studio_uuid") String studio_uuid);
}
