package asia.cmg.f8.social.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by on 12/29/16.
 */
@Component
@ConfigurationProperties(prefix = "socials")
public class SocialProperties {

    private String webPortal;
    private String pageTitle;
    private String hostUrl;
    private String appleAssociationContent;
    private String[] f8UserUuids;
    private long cacheInMiliSecond;
    private long cacheMaximumSize;
    private int numberOfThreadsForPosting;
    private int socialPageSize;
    
    
    public int getSocialPageSize() {
		return socialPageSize;
	}

	public void setSocialPageSize(int socialPageSize) {
		this.socialPageSize = socialPageSize;
	}

	public long getCacheMaximumSize() {
		return cacheMaximumSize;
	}

	public void setCacheMaximumSize(long cacheMaximumSize) {
		this.cacheMaximumSize = cacheMaximumSize;
	}

	public long getCacheInMiliSecond() {
		return cacheInMiliSecond;
	}

	public void setCacheInMiliSecond(long cacheInMiliSecond) {
		this.cacheInMiliSecond = cacheInMiliSecond;
	}

	public String getWebPortal() {
        return webPortal;
    }

    public void setWebPortal(final String webPortal) {
        this.webPortal = webPortal;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(final String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    public void setHostUrl(final String hostUrl) {
        this.hostUrl = hostUrl;
    }

    public String getAppleAssociationContent() {
        return appleAssociationContent;
    }

    public void setAppleAssociationContent(final String appleAssociationContent) {
        this.appleAssociationContent = appleAssociationContent;
    }

    public String[] getF8UserUuids() {
        return f8UserUuids;
    }

    public void setF8UserUuids(final String[] f8UserUuid) {
        this.f8UserUuids = f8UserUuid;
    }

	public int getNumberOfThreadsForPosting() {
		return numberOfThreadsForPosting;
	}

	public void setNumberOfThreadsForPosting(int numberOfThreadsForPosting) {
		this.numberOfThreadsForPosting = numberOfThreadsForPosting;
	}
}
