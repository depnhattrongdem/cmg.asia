package asia.cmg.f8.social.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import java.util.List;
import asia.cmg.f8.common.dto.QuestionDTO;

@FeignClient(value = "profileClient", url = "${feign.profileUrl}", fallback = ProfileClientFallbackImpl.class)
public interface ProfileClient {

	@RequestMapping(path = "/internal/v1/questions/user_type/{user_type}/language/{language}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	List<QuestionDTO> getQuestionByUserTypeAndLanguage(@PathVariable("user_type") final String userType,
																	@PathVariable("language") final String language);
}
