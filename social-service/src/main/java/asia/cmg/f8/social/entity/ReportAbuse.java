package asia.cmg.f8.social.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name = "report_abuses",
        indexes = {@Index(name = "IDX_USER_ID", columnList = "user_id")}
)
public class ReportAbuse {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id", length = 36, nullable = false)
    private String userId;

    @Column(name = "post_id", length = 36, nullable = false)
    private String postId;

    @Column(name = "reason")
    private String reason;

    protected ReportAbuse() {
    }

    public ReportAbuse(String userId, String postId, String reason) {
        this.userId = userId;
        this.postId = postId;
        this.reason = reason;
    }

    public String getUserId() {
        return userId;
    }

    public String getPostId() {
        return postId;
    }

    public Long getId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("userId", userId)
                .append("postId", postId)
                .append("reason", reason)
                .toString();
    }
}