package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on 2/20/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectionInfo {

    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }
}
