package asia.cmg.f8.social.service;

import asia.cmg.f8.common.dto.QuestionDTO;
import asia.cmg.f8.common.spec.user.UserType;
import asia.cmg.f8.social.dto.DataMatchDto;
import asia.cmg.f8.social.entity.AnswerEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PTIMatchingService {

    private SocialService socialService;
    
    @Autowired
    private ProfileClientService profileClientService;

    @Inject
    public PTIMatchingService(SocialService socialService) {
        this.socialService = socialService;
    }
    
    public Map<String, QuestionDTO> getQuestionsMap() {
    	Map<String, QuestionDTO> mapQuestions = new HashMap<String, QuestionDTO>();
    	
    	List<QuestionDTO> questions = profileClientService.getQuestionByUserTypeAndLanguage(UserType.PT.toString(), "en");
        questions.forEach(question -> {
        	mapQuestions.put(question.getKey(), question);
        });
        
        return mapQuestions;
    }

    public List<DataMatchDto> getDataMatchs(String userUuid, Map<String, QuestionDTO> questions) {
        
        List<AnswerEntity> answers = socialService.getAnswersByUserUuid(userUuid);

        if (questions.isEmpty()) {
            return Collections.emptyList();
        }

        List<DataMatchDto> dataMatchDtos = new ArrayList<>();
        
        if(questions == null || questions.isEmpty()) {
        	return Collections.emptyList();
        }
        
        answers.forEach(answer -> {
            DataMatchDto dataMatchDto = new DataMatchDto();
            
            dataMatchDto.setQuestionId(answer.getQuestionId());
            QuestionDTO question = questions.get(answer.getQuestionId());
            if(question != null) {
            	dataMatchDto.setWeight(question.getWeight());
            }
            dataMatchDto.setOptions(answer.getOptionKeys());

            dataMatchDtos.add(dataMatchDto);
        });

        return dataMatchDtos;
    }
}
