package asia.cmg.f8.social.service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.spec.club.Club;
import asia.cmg.f8.common.spec.user.UserType;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.common.util.ZoneDateTimeUtils;
import asia.cmg.f8.social.client.CheckInUserClient;
import asia.cmg.f8.social.client.ClubClient;
import asia.cmg.f8.social.client.UserClient;
import asia.cmg.f8.social.config.SocialProperties;
import asia.cmg.f8.social.entity.CheckInUserEntity;
import asia.cmg.f8.social.entity.ClubEntity;
import asia.cmg.f8.social.entity.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Created on 12/6/16.
 */
@Service
public class ClubService {
    private static final Logger LOG = LoggerFactory.getLogger(ClubService.class);

    private static final String QUERY_USER_CHECK_IN_CLUB =
            "select * where expired_time > %1$s "
                    + "and club_id = '%2$s' "
                    + "and not (user_id = '%3$s') "
                    + "order by user_type desc, full_name asc";
    private static final String QUERY_USER_INFO =
            "select * where uuid = '%s'";

    private static final int PT_CHECK_IN_TIMEOUT = 360;
    private static final int EU_CHECK_IN_TIMEOUT = 1;

    private final ClubClient clubClient;
    private final UserClient userClient;
    private final CheckInUserClient checkInUserClient;
    private ExecutorService executor;

    public ClubService(final ClubClient clubClient,
                       final CheckInUserClient checkInUserClient,
                       final UserClient userClient,
                       final SocialProperties socialProperties) {
        this.clubClient = clubClient;
        this.checkInUserClient = checkInUserClient;
        this.userClient = userClient;
        executor = Executors.newFixedThreadPool(socialProperties.getNumberOfThreadsForPosting()); 
    }

    @Async
    public void createClub(final Account account, final ClubEntity clubEntity) {
    	executor.submit(() -> {
    		UserGridResponse<ClubEntity> createClubResp = clubClient.createClub(clubEntity);
        	if(Objects.isNull(createClubResp) || createClubResp.getEntities().isEmpty()) {
        		LOG.error("user-grid could not create club: {}", clubEntity.getName());
        	}
    	});
    }
    
    public List<ClubEntity> getAllClubs() {
        return clubClient.getAllClubs().getEntities().stream().collect(Collectors.toList());
    }
    
    public Optional<ClubEntity> getOneClub(final String uuid) {
    	return clubClient.getOneByUuid(uuid).getEntities().stream().findFirst();
    }
    
    @Async
    public void updateClub(final String uuid, final ClubEntity clubEntity) {
    	executor.submit(() -> {
    		UserGridResponse<ClubEntity> updateClubResp = clubClient.updateClub(uuid, clubEntity);
        	if(Objects.isNull(updateClubResp) || updateClubResp.getEntities().isEmpty()) {
        		LOG.error("user-grid could not update club: {}", clubEntity.getName());
        	}
    	});
    }

    public List<CheckInUserEntity> getCheckedUserInClub(final Account account,
                                                        final String clubId) {

        final String userCheckUserInClubQuery = String.format(QUERY_USER_CHECK_IN_CLUB,
                ZoneDateTimeUtils.convertToSecondUTC(LocalDateTime.now()),
                clubId,
                account.uuid());
        final UserGridResponse<CheckInUserEntity> listCheckInUserResp = checkInUserClient
                .getAllUserCheckInClub(userCheckUserInClubQuery);

        if (Objects.isNull(listCheckInUserResp)) {
            LOG.error("Usergrid went wrong while executing query: " + userCheckUserInClubQuery);
            return Collections.emptyList();
        }

        return listCheckInUserResp.getEntities();
    }

    public Optional<CheckInUserEntity> clubCheckedUser(final Account account, final String clubId) {
        final Optional<UserEntity> userInfo =
                userClient
                        .getUserByQuery(String.format(QUERY_USER_INFO, account.uuid()))
                        .getEntities()
                        .stream()
                        .findFirst();
        if (userInfo.isPresent()) {
            final CheckInUserEntity checkInUserEntity = new CheckInUserEntity();
            final UserEntity userEntity = userInfo.get();
            checkInUserEntity.setUserId(userEntity.getUuid());
            checkInUserEntity.setFullName(userEntity.getName());
            checkInUserEntity.setUsername(userEntity.getUsername());
            checkInUserEntity.setEmail(userEntity.getEmail());
            checkInUserEntity.setClubId(clubId);
            if (userEntity.getUserType() == UserType.PT)
            {
                checkInUserEntity.setExpiredTime(
                        ZoneDateTimeUtils.convertToSecondUTC(LocalDateTime.now().plusHours(PT_CHECK_IN_TIMEOUT)));
            } else {
                checkInUserEntity.setExpiredTime(
                        ZoneDateTimeUtils.convertToSecondUTC(LocalDateTime.now().plusHours(EU_CHECK_IN_TIMEOUT)));
            }
            checkInUserEntity.setUserType(userEntity.getUserType().name().toLowerCase(Locale.US));
            return checkInUserClient.checkUserInClub(checkInUserEntity)
                    .getEntities().stream().findFirst();
        }

        return Optional.empty();
    }
}
