package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created on 12/21/16.
 */
public class LikePostRequest {
    @JsonProperty("post_id")
    @NotBlank
    private String postId;

    @JsonProperty("like")
    @NotNull
    private Boolean like;

    public String getPostId() {
        return postId;
    }

    public void setPostId(final String postId) {
        this.postId = postId;
    }

    public Boolean getLike() {
        return like;
    }

    public void setLike(final Boolean like) {
        this.like = like;
    }
}
