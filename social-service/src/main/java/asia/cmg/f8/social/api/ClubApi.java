package asia.cmg.f8.social.api;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.security.annotation.RequiredAdminRole;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.entity.CheckInUserEntity;
import asia.cmg.f8.social.entity.CheckInUserImpl;
import asia.cmg.f8.social.entity.ClubEntity;
import asia.cmg.f8.social.entity.ClubImpl;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.service.ClubService;
import asia.cmg.f8.social.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import rx.Observable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created on 12/6/16.
 */
@RestController
public class ClubApi {
    private static final Logger LOG = LoggerFactory.getLogger(ClubApi.class);

    private static final String CLUB_ID = "club_id";
    private static final String SUCCESS = "success";
    private static final String CLASS_USER_ROLE = "class";

    private final ClubService clubService;
    private final UserService userService;
    private Gson gson = new Gson();

    public ClubApi(final ClubService clubService, final UserService userService) {
        this.clubService = clubService;
        this.userService = userService;
    }

    /**
     * Modified: Thach Vo
     * Get all clubs that show on Locate screen 
     * @return
     */
    @RequestMapping(value = "/pubClubs", method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public List<ClubImpl> getListClubs() {
        return clubService.getAllClubs().stream().map(entity -> ClubImpl
										                        .builder()
										                        .id(entity.getId())
										                        .address(entity.getAddress())
										                        .code(entity.getCode())
										                        .name(entity.getName())
										                        .latitude(entity.getLatitude())
										                        .longitude(entity.getLongitude())
										                        .clubtype(entity.getClubtype())
										                        .city(entity.getCity())
										                        .locationIcon(entity.getLocationIcon())
										                        .logo(entity.getLogo())
										                        .build()).collect(Collectors.toList());
    }
    
    /**
     * Get club detail by uuid
     * @author thach vo
     * @return
     */
    @GetMapping(value = "/clubs/{uuid}", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getClubDetail(@PathVariable(name = "uuid") final String uuid, Account account) {
    	try {
    		Optional<ClubEntity> clubEntityOptional = clubService.getOneClub(uuid);
        	
        	if(!clubEntityOptional.isPresent()) {
        		return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
        	}
        	
        	return new ResponseEntity<>(clubEntityOptional.get(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
		}
    }
    
    @PutMapping(value = "/clubs/{uuid}", produces = APPLICATION_JSON_UTF8_VALUE)
    @RequiredAdminRole
    public Map<String, Object> updateClub(@PathVariable(name = "uuid") final String clubUuid, @RequestBody final ClubEntity clubEntity) {
    	LOG.info("Logging clubEntity info input: {}", gson.toJson(clubEntity));
    	try {
    		try {
    			Optional<ClubEntity> clubEntityOptional = clubService.getOneClub(clubUuid);
    			if(!clubEntityOptional.isPresent()) {
    				LOG.error("Update club failed. Club's uuid is not existed: {}", clubUuid);
            		return Collections.singletonMap(SUCCESS, Boolean.FALSE);
    			}
			} catch (Exception e) {
				LOG.error("Update club failed. Club's uuid is not existed: {}", clubUuid);
        		return Collections.singletonMap(SUCCESS, Boolean.FALSE);
			}
    		clubService.updateClub(clubUuid, clubEntity);
		} catch (Exception e) {
			return Collections.singletonMap(SUCCESS, Boolean.FALSE);
		}
    	
    	return Collections.singletonMap(SUCCESS, Boolean.TRUE);
    }
    
    /**
     * Documented: Thach Vo
     * Notes: Currently not used
     * @return
     */
    @RequestMapping(value = "/clubs", method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public List<ClubImpl> getListClub() {
        return clubService.getAllClubs().stream().map(entity -> ClubImpl
											                        .builder()
											                        .id(entity.getId())
											                        .address(entity.getAddress())
											                        .code(entity.getCode())
											                        .name(entity.getName())
											                        .latitude(entity.getLatitude())
											                        .longitude(entity.getLongitude())
											                        .clubtype(entity.getClubtype())
											                        .city(entity.getCity())
											                        .locationIcon(entity.getLocationIcon())
											                        .logo(entity.getLogo())
											                        .build()).collect(Collectors.toList());
    }

    /**
     * Documented: Thach Vo
     * Get all information(Classes, Trainers, Friends when click on a club location icon)
     * @param clubId
     * @param account
     * @return
     */
    @RequestMapping(value = "/clubs/{club_id}/checked/users", method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<List<CheckInUserImpl>> getClubCheckedUser(@PathVariable("club_id") final String clubId, final Account account) {
        final List<CheckInUserEntity> checkInUserEntityList = clubService.getCheckedUserInClub(account, clubId);
        final Observable<List<CheckInUserImpl>> checkInUserObs;

        if (Objects.isNull(checkInUserEntityList) || checkInUserEntityList.isEmpty()) {
            checkInUserObs = Observable.just(Collections.emptyList());
        } else {
            final Set<String> listUserId = checkInUserEntityList
                    .stream()
                    .map(CheckInUserEntity::getUserId)
                    .collect(Collectors.toSet());

            //Checked-in users
            checkInUserObs = Observable.zip(
                    userService.getFollowingConnection(account, listUserId),
                    userService.getBasicUserInfoByUUIDs(listUserId),
                    (listFollowingUserId, mapUsers) -> checkInUserEntityList
                            .stream()
                            .filter(Objects::nonNull)
                            .map(entity -> {
                                final UserEntity userEntity = mapUsers.get(entity.getUserId());
                                if(userEntity == null) {
                                	return null;
                                }

                                return CheckInUserImpl
                                        .builder()
                                        .id(entity.getUserId())
                                        .name(userEntity.getName())
                                        .username(entity.getUsername())
                                        .picture(userEntity.getPicture())
                                        .isFollowed(listFollowingUserId.contains(entity.getUserId()))
                                        .userType(entity.getUserType())
                                        .email(entity.getEmail())
                                        .build();
                            })
                            .filter(Objects::nonNull)
                            .filter(distinctByKey(user -> (null == user) ? null : user.getId()))
                            .collect(Collectors.toList()))
                    .doOnError(error -> LOG.error("Populating Checked in User data occurs Error {}", error))
                    .firstOrDefault(Collections.emptyList());
        }

        //Class users
        final Observable<List<CheckInUserImpl>> classUserObs = userService.getClassUsers(account)
                .map(list -> list
                        .stream()
                        .map(classUserEntity ->  CheckInUserImpl
                            .builder()
                            .id(classUserEntity.getId())
                            .name(classUserEntity.getName())
                            .username(classUserEntity.getUsername())
                            .picture(classUserEntity.getPicture())
                            .isFollowed(classUserEntity.isFollowed())
                            .userRole(CLASS_USER_ROLE)
                            .email(classUserEntity.getEmail())
                            .build())
                        .distinct()
                        .collect(Collectors.toList()))
                .doOnError(error -> LOG.error("Populating class user data occurs Error {}", error))
                .firstOrDefault(Collections.emptyList());


        return Observable.zip(checkInUserObs, classUserObs,
                    (checkInUserList, classUserList) -> Stream
                                                            .concat(checkInUserList.stream(), classUserList.stream())
                                                            .collect(Collectors.toList()))
                .doOnError(error -> LOG.error("Populating class user data occurs Error {}", error))
                .firstOrDefault(Collections.emptyList());
    }

    private static <T> Predicate<T> distinctByKey(final Function<? super T, Object> keyExtractor)
    {
        final Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return entity -> map.putIfAbsent(keyExtractor.apply(entity), Boolean.TRUE) == null;
    }

    @RequestMapping(value = "/clubs/checks/users", method = RequestMethod.POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public Map<String, Object> clubCheckedUser(@RequestBody final Map<String, String> request, final Account account) {
        return Collections.singletonMap(SUCCESS,clubService.clubCheckedUser(account, request.get(CLUB_ID)).isPresent());
    }
}
