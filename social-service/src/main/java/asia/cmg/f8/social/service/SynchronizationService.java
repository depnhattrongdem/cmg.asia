package asia.cmg.f8.social.service;

import asia.cmg.f8.common.profile.ChangeUserInfoEvent;
import asia.cmg.f8.common.spec.session.SessionStatus;
import asia.cmg.f8.common.util.CommonConstant;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.session.ChangeSessionStatusEvent;
import asia.cmg.f8.social.RatingSessionEvent;
import asia.cmg.f8.social.client.CompletedSessionClient;
import asia.cmg.f8.social.client.UserClient;
import asia.cmg.f8.social.entity.CompletedSessionEntity;
import asia.cmg.f8.social.entity.Profile;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.event.EventHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 11/30/16.
 */
@Service
public class SynchronizationService {
    private static final Logger LOG = LoggerFactory.getLogger(SynchronizationService.class);

    private static final String QUERY_USER = "select * where uuid='%s'";

    private static final String QUERY_COMPLETED_SESSION = "select * where session_id='%s'";
    private final UserClient userClient;
    private final CompletedSessionClient completedSessionClient;
    private final EventHandler eventHandler;

    @Inject
    public SynchronizationService(final UserClient userClient,
                                  final EventHandler eventHandler,
                                  final CompletedSessionClient completedSessionClient) {
        this.userClient = userClient;
        this.eventHandler = eventHandler;
        this.completedSessionClient = completedSessionClient;
    }

    public void completeRatingSessionUserInfoEvent(
            final RatingSessionEvent ratingSessionEventEvent) {
        updateUser(ratingSessionEventEvent.getUserId().toString(),
                ratingSessionEventEvent.getStars());
        LOG.info("Updated rating information for user: {}", ratingSessionEventEvent.getUserId());
    }

    public void completeRatingSessionStatusRatedEvent(
            final RatingSessionEvent ratingSessionEventEvent) {

        final String sessionId = ratingSessionEventEvent.getSessionId().toString();
        final String userType = ratingSessionEventEvent.getUserType().toString();

        if (StringUtils.isEmpty(sessionId) || StringUtils.isEmpty(userType)) {
            LOG.info("Request data is invalid with sessionId {} userType {}",
                    sessionId,
                    userType);
            return;
        }

        final Optional<CompletedSessionEntity> completedSessionEntityOptional =
                completedSessionClient
                        .findOneCompletedSessions(String.format(QUERY_COMPLETED_SESSION, sessionId))
                        .getEntities()
                        .stream()
                        .findFirst();
        if (!completedSessionEntityOptional.isPresent()) {
            LOG.info("Could not found completed session: {}", sessionId);
            return;
        }

        final CompletedSessionEntity entity = completedSessionEntityOptional.get();
        final String userId = ratingSessionEventEvent.getUserId().toString();
        if (CommonConstant.EU_USER_TYPE.equalsIgnoreCase(userType)
                && !Objects.isNull(completedSessionClient
                .updateCompletedSession(entity.getId(), CompletedSessionEntity
                        .copyOf(entity)
                        .withIsEndUserRated(Boolean.TRUE)))) {
            LOG.info("Marked session is rated by EU: {}", userId);
        } else if (CommonConstant.PT_USER_TYPE.equalsIgnoreCase(userType)
                && !Objects.isNull(completedSessionClient
                .updateCompletedSession(entity.getId(), CompletedSessionEntity
                        .copyOf(entity)
                        .withIsTrainerRated(Boolean.TRUE)))) {
            LOG.info("Marked session is rated by PT: {}", userId);
        }
    }

    //Update user when User info is changed.
    public UserEntity updateUser(final String userId, final double stars) {
        LOG.info("Processing update user info when user rating sessions");
        final Optional<UserEntity> user = userClient
                .getUserByQuery(String.format(QUERY_USER, userId))
                .getEntities()
                .stream()
                .findFirst();

        if (user.isPresent()) {

            final UserEntity currentUser = user.get();

            Profile currentProfile = currentUser.getProfile();
            double updatedTotalRate = stars;
            int updatedNumberOfRate = 1;
            double updatedRated = stars;

            if (!Objects.isNull(currentProfile)) {
                updatedTotalRate = Double.sum(
                        Objects.isNull(currentProfile.getTotalRate())
                                ? 0
                                : currentProfile.getTotalRate(),
                        stars);
                updatedNumberOfRate =
                        1 + (Objects.isNull(currentProfile.getNumberOfRate())
                                ? 0
                                : currentProfile.getNumberOfRate());
                updatedRated = updatedTotalRate / updatedNumberOfRate;

            } else {
                currentProfile = Profile.builder().build();
            }

            final Profile updatedProfile = Profile.builder()
                    .from(currentProfile)
                    .withTotalRate(updatedTotalRate)
                    .withNumberOfRate(updatedNumberOfRate)
                    .withRated(updatedRated)
                    .build();

            final UserEntity updatedUserEntity = UserEntity.builder()
                    .from(currentUser)
                    .withProfile(updatedProfile).build();

            final Optional<UserEntity> updatedUserResp =
                    userClient.updateRatingSessionUser(userId, updatedUserEntity)
                            .getEntities()
                            .stream()
                            .findFirst();
            if (updatedUserResp.isPresent()) {
                //fire ChangeUserInfoEvent to update ES in profile data
                final ChangeUserInfoEvent changeUserInfoEvent = ChangeUserInfoEvent.newBuilder()
                        .setEventId(java.util.UUID.randomUUID().toString())
                        .setUserId(userId)
                        .setUserType(updatedUserResp.get().getUserType().toString())
                        .setSubmittedAt(System.currentTimeMillis())
                        .build();
                eventHandler.publish(changeUserInfoEvent);
                LOG.info("Fired changeUserInfoEvent to update user info");

                return updatedUserResp.get();
            }
        }
        LOG.error("Could not found user {} when handling RatingSessionEvent", userId);

        return null;
    }

    public void handleChangeSessionStatusEvent(
            final ChangeSessionStatusEvent changeSessionStatusEvent) {
    	
        if (Objects.isNull(changeSessionStatusEvent.getSessionId())) {
            LOG.info("ChangeSessionStatusEvent data is invalid");
            return;
        }

        final String newStatus = changeSessionStatusEvent.getNewStatus().toString();
        final String sessionId = changeSessionStatusEvent.getSessionId().toString();

        if (newStatus.equals(SessionStatus.CONFIRMED.toString())) {
            LOG.info("Creating CompletedSession: {}", sessionId);
            final CompletedSessionEntity entity = CompletedSessionEntity
                    .builder()
                    .endUserId(changeSessionStatusEvent.getEndUserId().toString())
                    .trainerId(changeSessionStatusEvent.getTrainerId().toString())
                    .sessionDate(changeSessionStatusEvent.getSessionDate())
                    .sessionId(sessionId)
                    .name(sessionId)
                    .sessionStatus(newStatus)
                    .isTrainerRated(Boolean.FALSE)
                    .isEndUserRated(Boolean.FALSE)
                    .build();

            final UserGridResponse<CompletedSessionEntity> completedSessionResp =
                    completedSessionClient.createCompletedSession(entity);
            if (Objects.isNull(completedSessionResp)) {
                LOG.info("CompletedSession is duplicated");
                return;
            }

            final Optional<CompletedSessionEntity> createdEntity = completedSessionResp
                    .getEntities()
                    .stream()
                    .findFirst();

            if (createdEntity.isPresent()) {
                LOG.info("Created CompletedSession Id: {}", sessionId);
            }
            return;
        }

        //if (newStatus.equals(SessionStatus.CONFIRMED.toString())) {

            final Optional<CompletedSessionEntity> completedSessionEntityOptional =
                    completedSessionClient
                            .findOneCompletedSessions(String.format(QUERY_COMPLETED_SESSION, sessionId))
                            .getEntities()
                            .stream()
                            .findFirst();
            if (!completedSessionEntityOptional.isPresent()) {
                LOG.info("Could not found completed session: {}", sessionId);
                return;
            }

            final CompletedSessionEntity entity = completedSessionEntityOptional.get();
            final String userId = changeSessionStatusEvent.getEndUserId().toString();

            if (!Objects.isNull(completedSessionClient.updateCompletedSession(entity.getId(),
                    entity.withSessionStatus(newStatus)))) {
                LOG.info("Marked session is completed: {}", userId);
            }
            LOG.info("Ignored updated Session status: {}", newStatus);
        //}

        //LOG.info("Ignored updated Session status: {}", newStatus);

    }
}
