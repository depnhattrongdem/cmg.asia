package asia.cmg.f8.social.entity;

/**
 * Created on 12/29/16.
 */
public enum PostStatusType {
    published,
    deleted,
    pending_review
}
