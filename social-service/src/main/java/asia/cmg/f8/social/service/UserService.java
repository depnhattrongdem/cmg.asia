package asia.cmg.f8.social.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.client.UserClient;
import asia.cmg.f8.social.entity.ClassUserEntity;
import asia.cmg.f8.social.entity.UserEntity;
import rx.Observable;

/**
 * Created on 12/9/16.
 */
@Service
public class UserService {
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
    private static final String QUERY_PICTURE_USER = "select uuid, picture where %s";
    private static final String QUERY_BASIC_USER_INFO_ID = "select uuid, username, name, picture where uuid='%s'";
    private static final String QUERY_BASIC_USER_INFO = "select uuid, username, name, picture, level, approved where (%s)";
    private static final String QUERY_GET_CLASS_USERS = "select uuid, username, picture, email, name, profile where userRole='class' and activated=true";

    private final UserClient userClient;
    private final FollowService followService;

    public UserService(final UserClient userClient, FollowService followService) {
        this.userClient = userClient;
        this.followService = followService;
    }

    public Observable<Map<String, String>> getAvatars(final Collection<String> listUuid) {
        final String query = String.format(QUERY_PICTURE_USER, joiningListUuidWithOr(listUuid));
        return userClient.getUserInfoByQuery(query)
                .filter(Objects::nonNull)
                .map(UserGridResponse::getEntities)
                .filter(Objects::nonNull)
                .map(entries ->
                        entries.stream()
                                .collect(Collectors.toMap(
                                        UserEntity::getUuid, entity -> Optional.ofNullable(entity.getPicture()).filter(StringUtils::isNotEmpty).orElse(StringUtils.EMPTY))))
                .doOnError(error -> LOG.error("Get Avatars occurs error {} with query {}", error, query))
                .firstOrDefault(Collections.emptyMap());
    }

    public Observable<Map<String, UserEntity>> getBasicUserInfoByUUIDs(final Collection<String> listUuid) {
        final String query = String.format(QUERY_BASIC_USER_INFO, joiningListUuidWithOr(listUuid));
        if(listUuid.isEmpty()) {
        	return Observable.empty();
        }
        
        return userClient.getUserInfoByQuery(query)
                .filter(Objects::nonNull)
                .map(UserGridResponse::getEntities)
                .filter(Objects::nonNull)
                .map(userList -> userList
                        .stream()
                        .collect(Collectors.toMap(
                                UserEntity::getUuid,
                                user -> UserEntity.builder()
                                        .withUuid(user.getUuid())
                                        .withUsername(user.getUsername())
                                        .withName(user.getName())
                                        .withApproved(user.getApproved())
                                        .withPicture(Optional.ofNullable(user.getPicture()).filter(StringUtils::isNotEmpty).orElse(StringUtils.EMPTY))
                                        .withLevel(Optional.ofNullable(user.getLevel()).filter(StringUtils::isNotEmpty).orElse(StringUtils.EMPTY))
                                        .build())))
                .doOnError(error -> LOG.error("Get basic user info by uuids occurs error {} with query {}", error, query))
                .firstOrDefault(Collections.emptyMap());
    }

    public Optional<UserEntity> getBasicUserInfoByUuid(final String userId) {
        return Optional.ofNullable(userClient.getUserByQuery(String.format(QUERY_BASIC_USER_INFO_ID, userId)))
                .map(UserGridResponse::getEntities)
                .filter(Objects::nonNull)
                .map(entries -> entries
                        .stream()
                        .map(user -> UserEntity.builder()
                                .withUuid(user.getUuid())
                                .withUsername(user.getUsername())
                                .withName(user.getName())
                                .withPicture(Optional.ofNullable(user.getPicture()).filter(StringUtils::isNotEmpty).orElse(StringUtils.EMPTY))
                                .build())
                        .findFirst())
                .orElse(Optional.empty());
    }

    public Observable<Set<String>> getFollowingConnection(final Account account, final Collection<String> listUuid) {
        List<String> followerUserIds = followService.getFollowingConnection(account.uuid(), 0, Integer.MAX_VALUE).getContent();
        Set<String> results = followerUserIds.stream()
                .filter(uid->listUuid.contains(uid))
                .collect(Collectors.toSet());
        return Observable.just(results);

    }

    private String joiningListUuidWithOr(final Collection<String> listUuid) {
        return listUuid.stream()
                .distinct()
                .map(user -> "uuid ='" + user + "'")
                .collect(Collectors.joining(" or "));
    }

    public Observable<List<ClassUserEntity>> getClassUsers(final Account account) {
        final UserGridResponse<UserEntity> userListResponse = userClient.getUserByQuery(QUERY_GET_CLASS_USERS);

        if (Objects.isNull(userListResponse)) {
            LOG.error("Usergrid went wrong while executing query: " + QUERY_GET_CLASS_USERS);
            return Observable.just(Collections.emptyList());
        }

        final List<UserEntity> userList = userListResponse.getEntities();

        final Set<String> userIdSet = userList
                .stream()
                .map(UserEntity::getUuid)
                .collect(Collectors.toSet());

        return this.getFollowingConnection(account, userIdSet).map(followingSet ->
                userList.stream()
                        .map(user -> ClassUserEntity.builder()
                                .id(null == user? null : user.getUuid())
                                .name(null == user? null : user.getName())
                                .username(null == user? null : user.getUsername())
                                .picture(null == user? null : user.getPicture())
                                .isFollowed(null == user? null : followingSet.contains(user.getUuid()))
                                .phone(null == user? null : user.getProfile().getPhone())
                                .email(null == user? null : user.getEmail())
                                .build())
                        .distinct()
                        .collect(Collectors.toList()))
                .doOnError(error -> LOG.error("Populating Class User data occurs Error {}", error))
                .firstOrDefault(Collections.emptyList());
    }
}
