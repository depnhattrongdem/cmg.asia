package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.CommentEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import rx.Observable;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created on 12/22/16.
 */
@FeignClient(value = "socialsComments", url = "${feign.url}",
        fallback = SocialCommentClientFallbackImpl.class)
public interface SocialCommentClient {

	String SECRET_QUERY = "?client_id=${socials.userGridClientId}&client_secret=${socials.userGridClientSecret}";
    String UUID = "uuid";
    String LIMIT = "limit";
    String CURSOR = "cursor";
    String QUERY = "query";
    String RELATIONSHIP = "likes";
    String COMMENT_ID = "comment_id";
    String COMMENT_QUERY_PREFIX = "/comments" + SECRET_QUERY;
    String USER_LIKE_COMMENT_QUERY = "/users/{uuid}/" + RELATIONSHIP
            + "/{comment_id}" + SECRET_QUERY;
    String USER_UNLIKE_COMMENT_QUERY = "/users/{uuid}/" + RELATIONSHIP
            + "/comments/{comment_id}" + SECRET_QUERY;
    String GET_COMMENT_POST_QUERY = "/comments" + SECRET_QUERY + "&ql={query}&limit={limit}&cursor={cursor}";
    String USER_LIKE_COMMENT_STATUS_QUERY = "/users/{uuid}/likes/comments"
            + SECRET_QUERY + "&ql={query}";
    String GET_COMMENT_BY_ID = "/comments/{comment_id}" + SECRET_QUERY;

    @RequestMapping(value = COMMENT_QUERY_PREFIX,
            method = RequestMethod.POST,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CommentEntity> createComment(
            @RequestBody final CommentEntity entity);

    @RequestMapping(value = USER_LIKE_COMMENT_QUERY, method = RequestMethod.POST,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CommentEntity> likeComment(
            @RequestParam(UUID) final String userId,
            @RequestParam(COMMENT_ID) final String commentId);

    @RequestMapping(value = USER_UNLIKE_COMMENT_QUERY,
            method = RequestMethod.DELETE,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CommentEntity> unLikeComment(
            @RequestParam(UUID) final String userId,
            @RequestParam(COMMENT_ID) final String commentId);

    @RequestMapping(value = GET_COMMENT_POST_QUERY,
            method = RequestMethod.GET,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    PagedResponse<CommentEntity> getCommentByPostId(
            @PathVariable(QUERY) final String query,
            @RequestParam(value = CURSOR, required = false) final String cursor,
            @RequestParam(LIMIT) final int limit);

    @RequestMapping(value = USER_LIKE_COMMENT_STATUS_QUERY, method = RequestMethod.GET,
            produces = APPLICATION_JSON_VALUE)
    Observable<UserGridResponse<CommentEntity>> getUserLikedCommentByQuery(
            @PathVariable(UUID) final String userId,
            @PathVariable(QUERY) final String query);

    @RequestMapping(value = USER_LIKE_COMMENT_STATUS_QUERY, method = RequestMethod.GET,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CommentEntity> getLikeCommentByCommentId(
            @PathVariable(UUID) final String userId,
            @PathVariable(QUERY) final String query);

    @RequestMapping(value = GET_COMMENT_BY_ID,
            method = RequestMethod.DELETE,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CommentEntity> deleteComment(
            @RequestParam(COMMENT_ID) final String commentId);

    @RequestMapping(value = GET_COMMENT_BY_ID,
            method = RequestMethod.GET,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CommentEntity> checkCommentAvailable(
            @RequestParam(COMMENT_ID) final String commentId);

    @RequestMapping(value = GET_COMMENT_BY_ID,
            method = RequestMethod.GET,
            produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CommentEntity> getCommentById(
            @RequestParam(COMMENT_ID) final String commentId);

}
