package asia.cmg.f8.social.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

/**
 * Created on 12/9/16.
 */
public class RatingRequest {
    @JsonProperty("session_id")
    @NotNull
    private String sessionId;

    @JsonProperty("session_date")
    @NotNull
    private long sessionDate;

    @JsonProperty("user_id")
    @NotNull
    private String userId;

    @JsonProperty("reaction")
    @NotNull
    private String reaction;

    @JsonProperty("stars")
    @NotNull
    private Double stars;
    
    @JsonProperty("comment")
    @Nullable
    private String comment;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    public long getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(final long sessionDate) {
        this.sessionDate = sessionDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(final String reaction) {
        this.reaction = reaction;
    }

    public Double getStars() {
        return stars;
    }

    public void setStars(final Double stars) {
        this.stars = stars;
    }

	public String getComment() {
		return comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}
    
    
    
}
