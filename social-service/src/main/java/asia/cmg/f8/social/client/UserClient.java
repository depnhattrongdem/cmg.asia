package asia.cmg.f8.social.client;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created on 12/8/16.
 */

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.UserEntity;
import rx.Observable;

@FeignClient(value = "clubs", url = "${feign.url}", fallback = UserClientFallbackImpl.class)
public interface UserClient {

    String USER_ID = "user_id";
    String QUERY = "query";

//    String GET_FOLLOWING_USER = "/users/me/following?ql={query}";
    String SECRET_QUERY = "client_id=${socials.userGridClientId}&client_secret=${socials.userGridClientSecret}";
    String GET_USER = "/users?ql={query}&limit=10000&" + SECRET_QUERY;
    String UPDATE_RATING_USER = "/users/{user_id}?" + SECRET_QUERY;

//    @RequestMapping(value = GET_FOLLOWING_USER, method = GET, produces = APPLICATION_JSON_VALUE)
//    Observable<UserGridResponse<UserEntity>> getFollowingConnection(@PathVariable(QUERY) final String query);

    @RequestMapping(value = GET_USER, method = GET, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<UserEntity> getUserByQuery(@PathVariable(QUERY) final String query);

    @RequestMapping(value = "/users?ql={query}&limit={limit}&" + SECRET_QUERY, method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    UserGridResponse<UserEntity> getUserByQuery(@PathVariable(QUERY) final String query, @PathVariable("limit") final int limit);

    @RequestMapping(value = UPDATE_RATING_USER, method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<UserEntity> updateRatingSessionUser(@PathVariable(USER_ID) final String userId, @RequestBody final UserEntity request);

    @RequestMapping(value = GET_USER, method = GET, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    Observable<UserGridResponse<UserEntity>> getUserInfoByQuery(@PathVariable(QUERY) final String query);
}
