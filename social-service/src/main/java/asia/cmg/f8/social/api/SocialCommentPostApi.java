package asia.cmg.f8.social.api;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.dto.Comment;
import asia.cmg.f8.social.dto.CreateCommentRequest;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.CommentEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.service.CounterService;
import asia.cmg.f8.social.service.SocialCommentService;
import asia.cmg.f8.social.service.SocialPostService;
import asia.cmg.f8.social.service.UserService;
import asia.cmg.f8.social.util.SocialConstant;
import asia.cmg.f8.social.util.SocialConverterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rx.Observable;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created on 12/22/16.
 */
@RestController
public class SocialCommentPostApi {

    private static final Logger LOG = LoggerFactory.getLogger(SocialCommentPostApi.class);
    private static final String SUCCESS = "success";
    private final SocialCommentService socialCommentService;
    private final UserService userService;
    private final CounterService counterService;
    private final SocialPostService socialPostService;

    public SocialCommentPostApi(final SocialCommentService socialCommentService,
                                final UserService userService,
                                final CounterService counterService,
                                final SocialPostService socialPostService) {
        this.socialCommentService = socialCommentService;
        this.userService = userService;
        this.counterService = counterService;
        this.socialPostService = socialPostService;
    }

    @RequestMapping(
            value = "/social/users/posts/comments", method = RequestMethod.POST,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity postComments(final Account account,
                                       @RequestBody final CreateCommentRequest request) {
        final String currentUserId = account.uuid();

        final Optional<ActivityEntity> post =
                socialPostService.getValidActivityForUserInteraction(
                        request.getPostId(), currentUserId);

        if (!post.isPresent()) {

            LOG.info("The post not exist or published at post_id: {} ", request.getPostId());
            return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
        }

        final Optional<UserEntity> userInfo = userService
                .getBasicUserInfoByUuid(currentUserId);

        final Optional<CommentEntity> commentResponse = socialCommentService
                .createComment(account, request, userInfo.get(), post.get());

        if (!commentResponse.isPresent()) {
            LOG.error("user-grid could not create comment for post: {}", request.getPostId());
            return new ResponseEntity<>(ErrorCode.INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(SocialConverterUtil.convertCommentEntityToComment(
                commentResponse.get(), 0, userInfo.get(), Boolean.FALSE), HttpStatus.OK);
    }

    @RequestMapping(value = "/social/users/posts/{post_id}/comments", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<PagedResponse<Comment>> loadComments(
            final Account account,
            @PathVariable(value = "post_id") final String postId,
            @RequestParam(value = "cursor", required = false) final String cursor,
            @RequestParam(value = "limit", defaultValue = "10") final int limit) {

        //TODO Refactor code
        final PagedResponse<CommentEntity> pagedCommentResp =
                socialCommentService.getCommentsByPostId(postId, cursor, limit, account);
        if (Objects.isNull(pagedCommentResp) ||  pagedCommentResp.isEmpty()) {
            return Observable.just(new PagedResponse<>());
        }

        final String userId = account.uuid();
        return populateDataComment(account, userId, pagedCommentResp);

    }

    @RequestMapping(value = "/social/users/comments/{commentId}", method = RequestMethod.DELETE,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity deleteComment(
            @PathVariable(value = "commentId") final String commentId,
            final Account account) {
        final Optional<CommentEntity> commentOptional =
                socialCommentService.getCommentById(commentId, account);
        if (!commentOptional.isPresent()) {
            LOG.error("could not found comment : {}", commentId);
            return new ResponseEntity<>(ErrorCode.INVALID_COMMENT, HttpStatus.BAD_REQUEST);
        }

        final CommentEntity comment = commentOptional.get();
        final String currentUserId = account.uuid();
        final String postId = comment.getPostId();
        final Optional<ActivityEntity> post =
                socialPostService.getValidActivityForUserInteraction(postId, currentUserId);
        if (!post.isPresent()) {
            LOG.error("post id {} is invalid for deleting comment {}", postId, commentId);
            return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
        }

        if (currentUserId.equals(comment.getActor().getId())
                || currentUserId.equals(post.get().getActor().getId())) {
            return new ResponseEntity<>(Collections
                    .singletonMap(SUCCESS,
                            socialCommentService.deleteCommentByCommentId(commentId, account)),
                    HttpStatus.OK);
        }
        LOG.error("Do not have permission for user {} delete comment : {}",
                account.uuid(), commentId);
        return new ResponseEntity<>(ErrorCode.INVALID_COMMENT, HttpStatus.BAD_REQUEST);
    }

    /**
     * Populate data for List Comment including:
     * isLiked, numberOfLikes.
     *
     * @param userId               user uuid
     * @param pagedCommentResponse paged comment response
     * @return List of Comments
     */
    private Observable<PagedResponse<Comment>> populateDataComment(
            final Account account,
            final String userId,
            final PagedResponse<CommentEntity> pagedCommentResponse) {

        final List<CommentEntity> entities = pagedCommentResponse.getEntities();
        final Set<String> listCommentIds = new HashSet<>(entities.size());
        final Set<String> listActorIds = new HashSet<>(entities.size());

        for (final CommentEntity entity : entities) {
            listCommentIds.add(entity.getId());
            listActorIds.add(entity.getActor().getId());
        }

        return Observable.zip(
                socialCommentService.filterLikedCommentByCurrentUser(userId, listCommentIds, account),
                counterService.getTotalLikesOfComments(listCommentIds),
                userService.getBasicUserInfoByUUIDs(listActorIds),
                (listLikedComment, likeAndCommentCounter, mapUsers) -> {
                    final PagedResponse<Comment> result = new PagedResponse<>();
                    result.setCursor(pagedCommentResponse.getCursor());
                    result.setEntities(pagedCommentResponse.getEntities()
                            .stream()
                            .map(entity -> {
                                final String commentId = entity.getId();
                                return SocialConverterUtil.convertCommentEntityToComment(
                                        entity,
                                        likeAndCommentCounter.get(SocialConstant.getLikeCommentCounterName(commentId)),
                                        listLikedComment.contains(commentId),
                                        mapUsers.get(entity.getActor().getId()),
                                        entity.getTaggedUuids()
                                );
                            })
                            .sorted(Comparator.comparingLong(Comment::getPublished))
                            .collect(Collectors.toList()));
                    return result;
                })
                .doOnError(error -> LOG.error("Populating comment data occurs Error {}", error))
                .firstOrDefault(new PagedResponse<>());
    }
}