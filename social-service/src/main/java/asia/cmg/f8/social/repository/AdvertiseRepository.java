package asia.cmg.f8.social.repository;

import asia.cmg.f8.social.entity.AdvertiseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdvertiseRepository extends JpaRepository<AdvertiseEntity, Long> {
    @Query("select ad from AdvertiseEntity ad where ad.visibility = 1")
    List<AdvertiseEntity> findAll();

    @Query("select ad from AdvertiseEntity ad where ad.uuid = ?1 and ad.visibility = 1")
    Optional<AdvertiseEntity> findAdvertiseByUuid(String advertiseUuid);
}
