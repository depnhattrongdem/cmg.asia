package asia.cmg.f8.social.client;

import asia.cmg.f8.common.exception.UserGridException;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.CheckInUserEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created on 12/8/16.
 */
@Component
public class CheckInUserClientFallbackImpl implements CheckInUserClient {

    @Override
    public UserGridResponse<CheckInUserEntity> getAllUserCheckInClub(
            @PathVariable("query") final String query) {
        throw new UserGridException("ERROR_ON_GET_CHECK_IN_USER", "Usergrid went wrong while executing query: " + query);
    }

    @Override
    public UserGridResponse<CheckInUserEntity> checkUserInClub(
            @RequestBody final CheckInUserEntity entity) {
        throw new UserGridException("ERROR_ON_CHECK_IN_USER", "Usergrid went wrong while creating CheckInUserEntity");
    }
}
