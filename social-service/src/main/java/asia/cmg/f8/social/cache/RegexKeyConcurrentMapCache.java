package asia.cmg.f8.social.cache;

import org.springframework.cache.concurrent.ConcurrentMapCache;

import java.util.concurrent.ConcurrentMap;

public class RegexKeyConcurrentMapCache extends ConcurrentMapCache {
    private static final String REGEX_PREFIX = "regex:";

    public RegexKeyConcurrentMapCache(String name, ConcurrentMap<Object, Object> store, boolean allowNullValues) {
        super(name, store, allowNullValues);
    }

    @Override
    public void evict(Object key) {
        super.evict(key);
        if (key.toString().startsWith(REGEX_PREFIX)) {
            String regexKey = key.toString().replaceFirst(REGEX_PREFIX, "");
            for (Object cacheKey : this.getNativeCache().keySet()) {
                if (cacheKey.toString().matches(regexKey)) {
                    super.evict(cacheKey);
                }
            }
        }
    }
}
