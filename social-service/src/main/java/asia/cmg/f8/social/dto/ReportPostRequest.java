package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created on 12/26/16.
 */
public class ReportPostRequest {

    @JsonProperty("post_id")
    @NotBlank
    private String postId;

    @JsonProperty("reason")
    @NotBlank
    private String reason;

    public String getPostId() {
        return postId;
    }

    public void setPostId(final String postId) {
        this.postId = postId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(final String reason) {
        this.reason = reason;
    }

}
