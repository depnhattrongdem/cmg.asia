package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.CheckInUserEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created on 12/6/16.
 */
@FeignClient(value = "clubs", url = "${feign.url}", fallback = CheckInUserClientFallbackImpl.class)
public interface CheckInUserClient {
    String SECRET_QUERY =
            "client_id=${socials.userGridClientId}&client_secret=${socials.userGridClientSecret}";
    String GET_CHECK_IN_USER = "/check_in_users?" + SECRET_QUERY + "&limit=${socials.maxCheckedInUser}&ql={query}";
    String CLUB_CHECKS_USER_IN = "/check_in_users?" + SECRET_QUERY;

    @RequestMapping(value = GET_CHECK_IN_USER, method = GET, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CheckInUserEntity> getAllUserCheckInClub(@PathVariable("query") final String query);

    @RequestMapping(value = CLUB_CHECKS_USER_IN, method = POST, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<CheckInUserEntity> checkUserInClub(@RequestBody final CheckInUserEntity entity);
}
