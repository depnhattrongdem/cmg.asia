package asia.cmg.f8.social.entity;


import javax.persistence.*;

@Entity
@Table(name = "advertises")
public class AdvertiseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image", nullable = false)
    private String image;

    @Column(name = "image_visibility", nullable = false)
    private Boolean imageVisibility;

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "text_visibility", nullable = false)
    private Boolean textVisibility;

    @Column(name = "video", nullable = false)
    private String video;

    @Column(name = "video_visibility", nullable = false)
    private Boolean videoVisibility;

    @Column(name = "end_time", nullable = false)
    private Long endTime;

    @Column(name = "end_time_visibility", nullable = false)
    private Boolean endTimeVisibility;

    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "visibility", nullable = false)
    private Boolean visibility;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getImageVisibility() {
        return imageVisibility;
    }

    public void setImageVisibility(Boolean imageVisibility) {
        this.imageVisibility = imageVisibility;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getTextVisibility() {
        return textVisibility;
    }

    public void setTextVisibility(Boolean textVisibility) {
        this.textVisibility = textVisibility;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Boolean getVideoVisibility() {
        return videoVisibility;
    }

    public void setVideoVisibility(Boolean videoVisibility) {
        this.videoVisibility = videoVisibility;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Boolean getEndTimeVisibility() {
        return endTimeVisibility;
    }

    public void setEndTimeVisibility(Boolean endTimeVisibility) {
        this.endTimeVisibility = endTimeVisibility;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }
}
