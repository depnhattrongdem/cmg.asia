package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import asia.cmg.f8.social.database.entity.UserPtiMatchEntity;

public class UserPtiMatchDTO {

	private Long id = 0L;
	
	private String eu_uuid = "";
	
	private String pt_uuid = "";
	
	private Double average = 0d;
	
	private Double personality = 0d;
	
	@JsonProperty("training_style")
	private Double trainingStyle = 0d;
	
	private Double interest = 0d;

	public UserPtiMatchDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public UserPtiMatchDTO(UserPtiMatchEntity entity) {
		if(entity != null) {
			this.id = entity.getId();
			this.eu_uuid = entity.getEuUuid();
			this.pt_uuid = entity.getPtUuid();
			this.average = entity.getAverage();
			this.personality = entity.getPersonality();
			this.interest = entity.getInterest();
			this.trainingStyle = entity.getTrainingStyle();
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEu_uuid() {
		return eu_uuid;
	}

	public void setEu_uuid(String eu_uuid) {
		this.eu_uuid = eu_uuid;
	}

	public String getPt_uuid() {
		return pt_uuid;
	}

	public void setPt_uuid(String pt_uuid) {
		this.pt_uuid = pt_uuid;
	}

	public Double getAverage() {
		return average;
	}

	public void setAverage(Double average) {
		this.average = average;
	}

	public Double getPersonality() {
		return personality;
	}

	public void setPersonality(Double personality) {
		this.personality = personality;
	}

	public Double getTrainingStyle() {
		return trainingStyle;
	}

	public void setTrainingStyle(Double trainingStyle) {
		this.trainingStyle = trainingStyle;
	}

	public Double getInterest() {
		return interest;
	}

	public void setInterest(Double interest) {
		this.interest = interest;
	}
}
