package asia.cmg.f8.social.entity;

import asia.cmg.f8.common.spec.user.UserType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

/**
 * Created on 12/21/16.
 */
@Value.Immutable
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@JsonSerialize(as = ActorEntity.class)
@JsonDeserialize(builder = ActorEntity.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface AbstractActorEntity {
    @JsonProperty("username")
    @Nullable
    String getUsername();

    @JsonProperty("uuid")
    @Nullable
    String getId();

    @JsonProperty("user_type")
    @Nullable
    UserType getUserType();
}
