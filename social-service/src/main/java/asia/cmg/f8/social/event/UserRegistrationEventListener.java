package asia.cmg.f8.social.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.stereotype.Component;

import asia.cmg.f8.common.profile.SignUpUserEvent;
import asia.cmg.f8.social.config.MessageConverterConfiguration;
import asia.cmg.f8.social.config.SocialProperties;
import asia.cmg.f8.social.service.FollowService;

/**
 * Created on 2/17/17.
 */
@Component
public class UserRegistrationEventListener implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRegistrationEventListener.class);

    private final SocialProperties socialProperties;
    private final MessageConverter messageConverter;
    private final FollowService followService;

    public UserRegistrationEventListener(final SocialProperties socialProperties,
                                         @Qualifier(MessageConverterConfiguration.SIGN_UP_USER_EVENT_CONVERTER_ID) final MessageConverter messageConverter,
                                         final FollowService followService) {
        this.socialProperties = socialProperties;
        this.messageConverter = messageConverter;
        this.followService = followService;
    }

    @StreamListener(UserRegistrationEventStream.REGISTRATION_EVENT_INPUT_CHANNEL)
    public void onEvent(final Message message) {

        final SignUpUserEvent event = (SignUpUserEvent) messageConverter.fromMessage(message, SignUpUserEvent.class);
        if (event != null) {
            final String userUuid = String.valueOf(event.getUserId());
            final String[] f8PtUuids = socialProperties.getF8UserUuids();
            for(int i =0 ; i < f8PtUuids.length ; i++){
                followService.createFollowConnection(userUuid, f8PtUuids[i]);
                LOGGER.info("New registration user {} is following F8 user.", f8PtUuids[i]);           	
            }
        }
    }

    @Override
    @SuppressWarnings("PMD")
    public void afterPropertiesSet() throws Exception {
        final String[] f8UserUuids = socialProperties.getF8UserUuids();
        if (f8UserUuids == null) {
            throw new BeanInitializationException("f8UserUuid property is missing from configuration");
        }
        // if it's valid uuid string.
        LOGGER.info("Found a valid F8 uuid \"{}\" is configured", f8UserUuids);
    }
}
