package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.CommentEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import rx.Observable;

/**
 * Created on 12/22/16.
 */
@Component
public class SocialCommentClientFallbackImpl implements SocialCommentClient {
    private static final Logger LOG = LoggerFactory.getLogger(SocialCommentClientFallbackImpl.class);

    @Override
    public UserGridResponse<CommentEntity> createComment(
            @RequestBody final CommentEntity entityn) {
        LOG.error("user-grid went wrong while creating comment");
        return null;
    }

    @Override
    public UserGridResponse<CommentEntity> likeComment(
            @RequestParam(UUID) final String userId,
            @RequestParam(COMMENT_ID) final String commentId) {
        LOG.error("user-grid went wrong while creating like comment");
        return null;
    }

    @Override
    public UserGridResponse<CommentEntity> unLikeComment(
            @RequestParam(UUID) final String userId,
            @RequestParam(COMMENT_ID) final String commentId) {
        LOG.error("user-grid went wrong while creating unlike comment");
        return null;
    }

    @Override
    public PagedResponse<CommentEntity> getCommentByPostId(
            @PathVariable(QUERY) final String query,
            @RequestParam(value = CURSOR, required = false) final String cursor,
            @RequestParam(LIMIT) final int limit) {
        LOG.error("user-grid went wrong while retrieving comment");
        return null;
    }

    @Override
    public Observable<UserGridResponse<CommentEntity>> getUserLikedCommentByQuery(
            @PathVariable(UUID) final String userId,
            @PathVariable(QUERY) final String query) {
        LOG.error("user-grid went wrong while check status like comment");
        return null;
    }

    @Override
    public UserGridResponse<CommentEntity> deleteComment(
            @RequestParam(COMMENT_ID) final String commentId) {
        LOG.error("user-grid went wrong while deleting comment");
        return null;
    }

    @Override
    public UserGridResponse<CommentEntity> getLikeCommentByCommentId(
            @PathVariable(UUID) final String userId,
            @PathVariable(QUERY) final String query) {
        LOG.error("user-grid went wrong while check to count like comment");
        return null;
    }

    @Override
    public UserGridResponse<CommentEntity> checkCommentAvailable(
            @RequestParam(COMMENT_ID) final String commentId) {
        LOG.error("Comment does not exist at comment_id: {}", commentId);
        return null;
    }

    @Override
    public UserGridResponse<CommentEntity> getCommentById(
            @RequestParam(COMMENT_ID) final String commentId) {
        LOG.error("user-grid went wrong while get comment by id {}", commentId);
        return null;
    }
}
