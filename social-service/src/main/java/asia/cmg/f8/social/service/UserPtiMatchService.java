package asia.cmg.f8.social.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asia.cmg.f8.social.database.entity.UserPtiMatchEntity;
import asia.cmg.f8.social.dto.UserPtiMatchDTO;
import asia.cmg.f8.social.repository.UserPTiMatchRepository;

@Service
public class UserPtiMatchService {

	@Autowired
	UserPTiMatchRepository userPtiMatchRepo;
	
	public UserPtiMatchDTO getByEuUuidAndPtUuid(final String euUuid, final String ptUuid) {
		Optional<UserPtiMatchEntity> response = userPtiMatchRepo.getByEuUuidAndPtUuid(euUuid, ptUuid);
		if(response != null && response.isPresent()) {
			return new UserPtiMatchDTO(response.get());
		}
		return null;
	}
}
