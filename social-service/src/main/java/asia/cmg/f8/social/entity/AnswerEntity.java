package asia.cmg.f8.social.entity;

import asia.cmg.f8.common.spec.user.UserType;

import java.util.List;

/**
 * Created on 08/05/19.
 */
@SuppressWarnings("squid:S2384")
public class AnswerEntity {
    private String uuid;
    private String questionId;
    private List<String> optionKeys;
    private String owner;
    private UserType userType;
    private String eventId;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public void setOptionKeys(List<String> optionKeys) {
        this.optionKeys = optionKeys;
    }

    public List<String> getOptionKeys() {
        return optionKeys;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "AnswerEntity [uuid=" + uuid + ", questionId=" + questionId
                + ", optionKeys=" + optionKeys + ", owner=" + owner
                + ", userType=" + userType + ", eventId=" + eventId + "]";
    }

}
