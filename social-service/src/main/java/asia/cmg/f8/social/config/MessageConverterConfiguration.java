package asia.cmg.f8.social.config;

import asia.cmg.f8.common.message.AvRoMessageConverterLoader;
import asia.cmg.f8.common.message.EnableMessage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;

import javax.inject.Inject;
import java.io.IOException;

@Configuration
@EnableMessage
public class MessageConverterConfiguration {

    public static final String SIGN_UP_USER_EVENT_CONVERTER_ID = "signUpUserEventConverter";

    private final AvRoMessageConverterLoader avRoMessageConverterLoader;

    @Inject
    public MessageConverterConfiguration(final AvRoMessageConverterLoader avRoMessageConverterLoader) {
        this.avRoMessageConverterLoader = avRoMessageConverterLoader;
    }

    @Bean(name = "ratingSessionEventConverter")
    public MessageConverter ratingSessionEventConverter() throws IOException {
        return avRoMessageConverterLoader.load("RatingSessionEvent.avsc").get();
    }

    @Bean(name = "changeUserInfoEventConverter")
    public MessageConverter changeUserInfoEventConverter() throws IOException {
        return avRoMessageConverterLoader.load("ChangeUserInfoEvent.avsc").get();
    }

    @Bean(name = "sessionStatusEventConverter")
    public MessageConverter sessionStatusEventConverter() throws IOException {
        return avRoMessageConverterLoader.load("ChangeSessionStatusEvent.avsc").get();
    }

    @Bean(name = "likePostEventConverter")
    public MessageConverter likePostEventConverter() throws IOException {
        return avRoMessageConverterLoader.load("LikePostEvent.avsc").get();
    }

    @Bean(name = "likeCommentEventConverter")
    public MessageConverter likeCommentConverter() throws IOException {
        return avRoMessageConverterLoader.load("LikeCommentEvent.avsc").get();
    }

    @Bean(name = "commentPostEventConverter")
    public MessageConverter commentPostEventConverter() throws IOException {
        return avRoMessageConverterLoader.load("CommentPostEvent.avsc").get();
    }

    @Bean("userCreatedPostEventConverter")
    public MessageConverter userCreatedPostEventConverter() throws IOException {
        return avRoMessageConverterLoader.load("SocialPostCreatedEvent.avsc").get();
    }

    @Bean(name = SIGN_UP_USER_EVENT_CONVERTER_ID)
    public MessageConverter signupUserMessageConverter() throws IOException {
        return avRoMessageConverterLoader.load("SignUpUserEvent.avsc").get();
    }
    
    @Bean("viewPostEventConverter")
    public MessageConverter viewPostEventConverter() {
    	return avRoMessageConverterLoader.load("ViewPostEvent.avsc").get();
    }
}
