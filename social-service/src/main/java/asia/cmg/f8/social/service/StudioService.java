package asia.cmg.f8.social.service;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import asia.cmg.f8.social.dto.StudioDto;
import asia.cmg.f8.social.repository.StudioRepository;

@Service
public class StudioService {
	@Autowired
	private StudioRepository repository;
	
	private static final Logger LOG = LoggerFactory.getLogger(StudioService.class);
	
	public List<StudioDto> getStudios(Boolean isActive, Pageable page) {
		
		try {
			List<StudioDto> studios = repository.getStudios(isActive, page);
			
			if(studios != null && !studios.isEmpty()) 
				return studios;

			return Collections.emptyList();
			
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return Collections.emptyList();
		}
	}
	public StudioDto getStudioByUuid(String studio_uuid) {
			
			try {
				return repository.getStudioByUuid(studio_uuid);
				
			} catch (Exception e) {
				LOG.error(e.getMessage());
				return null;
			}
	}
}
