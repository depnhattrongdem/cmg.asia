package asia.cmg.f8.social.client;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import asia.cmg.f8.common.exception.UserGridException;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.UserEntity;
import rx.Observable;

/**
 * Created on 12/8/16.
 */
@Component
public class UserClientFallbackImpl implements UserClient {

//    @Override
//    public Observable<UserGridResponse<UserEntity>> getFollowingConnection(@RequestParam(ACCESS_TOKEN) final String token, @PathVariable(QUERY) final String query) {
//        return Observable.empty();
//    }

    @Override
    public UserGridResponse<UserEntity> getUserByQuery(@PathVariable(QUERY) final String query) {
        //TODO return instead throw exception
        throw new UserGridException("ERROR_ON_QUERY_USER", "Usergrid went wrong while executing query: " + query);
    }

    @Override
    public UserGridResponse<UserEntity> getUserByQuery(@PathVariable(QUERY) final String query, @PathVariable("limit") final int limit) {
        throw new UserGridException("ERROR_ON_QUERY_USER", "Usergrid went wrong while executing query: " + query);
    }

    @Override
    public UserGridResponse<UserEntity> updateRatingSessionUser(@PathVariable(USER_ID) final String userId, @RequestBody final UserEntity request) {
        //TODO return instead throw exception
        throw new UserGridException("ERROR_ON_UPDATE_RATING", "Usergrid went wrong while update rating session for user: " + userId);
    }

    @Override
    public Observable<UserGridResponse<UserEntity>> getUserInfoByQuery(@PathVariable(QUERY) final String query) {
        return Observable.empty();
    }
}
