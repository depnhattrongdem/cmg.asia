package asia.cmg.f8.social.event;

import asia.cmg.f8.session.ChangeSessionStatusEvent;
import asia.cmg.f8.social.CommentPostEvent;
import asia.cmg.f8.social.LikeCommentEvent;
import asia.cmg.f8.social.LikePostEvent;
import asia.cmg.f8.social.RatingSessionEvent;
import asia.cmg.f8.social.ViewPostEvent;
import asia.cmg.f8.social.api.SocialPostApi;
import asia.cmg.f8.social.service.SocialInteractionService;
import asia.cmg.f8.social.service.SynchronizationService;
import asia.cmg.f8.social.util.SocialConstant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.stereotype.Component;

/**
 * Created by 12/12/16.
 */
@Component
public class EventListener {
    @Autowired
    @Qualifier("ratingSessionEventConverter")
    private MessageConverter ratingSessionEventConverter;

    @Autowired
    @Qualifier("sessionStatusEventConverter")
    private MessageConverter sessionStatusEventConverter;

    @Autowired
    @Qualifier("likePostEventConverter")
    private MessageConverter likePostEventConverter;

    @Autowired
    @Qualifier("commentPostEventConverter")
    private MessageConverter commentPostEventConverter;

    @Autowired
    @Qualifier("likeCommentEventConverter")
    private MessageConverter likeCommentEventConverter;
    
    @Autowired
    @Qualifier("viewPostEventConverter")
    private MessageConverter viewPostEventConverter;

    private final SynchronizationService synchronizationService;
    private final SocialInteractionService socialInteractionService;
    
    private static final Logger LOG = LoggerFactory.getLogger(EventListener.class);

    public EventListener(final SynchronizationService synchronizationService,
                         final SocialInteractionService socialInteractionService) {
        this.synchronizationService = synchronizationService;
        this.socialInteractionService = socialInteractionService;
    }

    @StreamListener(EventStream.RATING_SS_USER_INFO_INPUT_CHANNEL)
    public void handleRatingSessionUserInfoEvent(final Message message) {
        final RatingSessionEvent ratingSessionEventEvent =
                (RatingSessionEvent) ratingSessionEventConverter
                        .fromMessage(message, RatingSessionEvent.class);
        synchronizationService.completeRatingSessionUserInfoEvent(ratingSessionEventEvent);
    }

    @StreamListener(EventStream.RATING_SS_STATUS_RATED_INPUT_CHANNEL)
    public void handleRatingSessionStatusRatedEvent(final Message message) {
        final RatingSessionEvent ratingSessionEventEvent =
                (RatingSessionEvent) ratingSessionEventConverter
                        .fromMessage(message, RatingSessionEvent.class);
        synchronizationService.completeRatingSessionStatusRatedEvent(ratingSessionEventEvent);
    }

    @StreamListener(EventStream.CHANGE_SESSION_STATUS_INPUT_CHANNEL)
    public void handleChangeSessionStatusEvent(final Message message) {
        final ChangeSessionStatusEvent changeSessionStatusEvent =
                (ChangeSessionStatusEvent) sessionStatusEventConverter
                        .fromMessage(message, ChangeSessionStatusEvent.class);
        synchronizationService.handleChangeSessionStatusEvent(changeSessionStatusEvent);
    }

    @StreamListener(EventStream.LIKE_POST_IN_CHANNEL)
    public void handleLikePostEvent(final Message message) {
        final LikePostEvent likePostEvent =
                (LikePostEvent) likePostEventConverter
                        .fromMessage(message, LikePostEvent.class);
        // Handle like post event
        if (likePostEvent.getCountValue().equals(SocialConstant.INCREASE)) {
            socialInteractionService.likesPost(likePostEvent.getPostId().toString(),
                    likePostEvent.getSubmittedAt());
        } else if (likePostEvent.getCountValue().equals(SocialConstant.DECREASE)) {
            socialInteractionService.unlikesPost(likePostEvent.getPostId().toString(),
                    likePostEvent.getSubmittedAt());
        }
    }

    @StreamListener(PostEventStream.COMMENT_POST_IN_CHANNEL)
    public void handleCommentPostEvent(final Message message) {
        final CommentPostEvent commentPostEvent =
                (CommentPostEvent) commentPostEventConverter
                        .fromMessage(message, CommentPostEvent.class);
        if (commentPostEvent.getCountValue().equals(SocialConstant.INCREASE)) {
            socialInteractionService.commentPost(commentPostEvent.getPostId().toString(),
                    commentPostEvent.getSubmittedAt());
        } else if (commentPostEvent.getCountValue().equals(SocialConstant.DECREASE)) {
            socialInteractionService.deleteCommentOfPost(commentPostEvent.getPostId().toString(),
                    commentPostEvent.getSubmittedAt());
        }
    }

    @StreamListener(EventStream.LIKE_COMMENT_IN_CHANNEL)
    public void handleLikeCommentEvent(final Message message) {
        final LikeCommentEvent likeCommentEvent =
                (LikeCommentEvent) likeCommentEventConverter
                        .fromMessage(message, LikeCommentEvent.class);
        //handle counter like comment
        socialInteractionService.handleLikeCommentEvent(likeCommentEvent);
    }

    @StreamListener(EventStream.VIEW_POST_IN_CHANNEL)
    public void handleViewPostEvent(final Message message) {
    	final ViewPostEvent viewPostEvent = (ViewPostEvent)viewPostEventConverter.fromMessage(message, ViewPostEvent.class);
    	final String postUuid = viewPostEvent.getPostId().toString();
    	
    	LOG.info("[handleViewPostEvent] post uuid {}");
    	
    	socialInteractionService.viewPost(postUuid);
    }
}
