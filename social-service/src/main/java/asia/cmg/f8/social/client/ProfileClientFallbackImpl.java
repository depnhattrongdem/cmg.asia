package asia.cmg.f8.social.client;

import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import asia.cmg.f8.common.dto.QuestionDTO;
import asia.cmg.f8.social.service.ProfileClientService;

@Component
public class ProfileClientFallbackImpl implements ProfileClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileClientService.class);

	@Override
	public List<QuestionDTO> getQuestionByUserTypeAndLanguage(String userType, String language) {
		LOGGER.error("[ProfileClientFallbackImpl][Failed upon Get Questions By UserType[{}] And Language[{}]]", userType, language);
		return Collections.emptyList();
	}
}
