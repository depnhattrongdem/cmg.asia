package asia.cmg.f8.social;

import asia.cmg.f8.common.util.F8Application;
import asia.cmg.f8.common.web.EnableWebSupport;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;


/**
 * Created on 9/28/16.
 */
@Configuration
@SpringBootApplication
@EnableFeignClients
@EnableWebSupport
@EnableAsync
@EnableCaching
@SuppressWarnings("PMD")
public class SocialApp {

    public static void main(final String[] args) {
        new F8Application(SocialApp.class.getSimpleName()).with(new SpringApplicationBuilder(SocialApp.class).web(true)).run(args);
    }
}

