package asia.cmg.f8.social.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created on 12/30/16.
 */
public interface PostEventStream {
    String COMMENT_POST_OUT_CHANNEL = "commentPostOutput";
    String COMMENT_POST_IN_CHANNEL = "commentPostInput";


    @Output(COMMENT_POST_OUT_CHANNEL)
    MessageChannel commentPost();

    @Input(COMMENT_POST_IN_CHANNEL)
    SubscribableChannel handleCommentPost();
}
