package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.AnswerEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(value = "answers", url = "${feign.url}")
public interface AnswerClient {

    String QUERY = "query";
    String LIMIT = "limit";
    String SECRET_QUERY = "client_id=${socials.userGridClientId}&client_secret=${socials.userGridClientSecret}";

    @RequestMapping(value = "/answers?limit={limit}&ql={query}&" + SECRET_QUERY, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<AnswerEntity> getAnswersByUser(@PathVariable(QUERY) String query, @RequestParam(LIMIT) int limit);
}
