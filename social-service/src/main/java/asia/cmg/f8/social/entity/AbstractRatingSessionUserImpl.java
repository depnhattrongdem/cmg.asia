package asia.cmg.f8.social.entity;

import asia.cmg.f8.common.spec.view.RatingSessionUser;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.annotation.Nullable;

import org.immutables.value.Value;

/**
 * Created on 12/9/16.
 */
@Value.Immutable
@JsonIgnoreProperties(ignoreUnknown = true)
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@SuppressWarnings({"CheckReturnValue", "PMD"})
public abstract class AbstractRatingSessionUserImpl implements RatingSessionUser {

	
	@Nullable
	abstract Boolean getIsSelected();
}
