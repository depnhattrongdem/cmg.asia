package asia.cmg.f8.social.api;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import asia.cmg.f8.common.dto.ApiRespObject;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.client.SocialPostClient;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.ResourceLink;

@RestController
public class DataMigrationApi {

	@Autowired
	private SocialPostClient socialPostClient;
	
	private static final Logger LOG = LoggerFactory.getLogger(DataMigrationApi.class);
	
//	@RequestMapping(value = "/admin/v1/migration/posts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Object> migratePosts(final Account account) {
//		ApiRespObject<Object> apiResponse = new ApiRespObject<Object>();
//		apiResponse.setStatus(ErrorCode.SUCCESS);
//		String cursor = null;
//		
//		try {
//			if(!account.isAdmin()) {
//				apiResponse.setStatus(ErrorCode.FORBIDDEN.withDetail("Need admin role for calling"));
//				return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
//			}
//			
//			do {
//				PagedResponse<ActivityEntity> pagedResponse = socialPostClient.getActivitiesByQuery("select * where content_type = 'image' or content_type = 'video'", cursor, 1000);
//				if(pagedResponse != null) {
//					List<ActivityEntity> activities = pagedResponse.getEntities();
//					activities.forEach(activity -> {
//						if(activity.getLink() != null && activity.getThumbnailLink() != null && (activity.getLinks() == null || activity.getLinks().isEmpty())) {
//							ResourceLink link = ResourceLink.builder().link(activity.getLink()).thumbnailLink(activity.getThumbnailLink()).build();
//							ActivityEntity updatedActivity = ActivityEntity.builder().from(activity).addLinks(link).build();
//							
//							socialPostClient.editPost(activity.getUuid(), updatedActivity);
//							
//							LOG.info("Updated social post id {}", activity.getUuid());
//						}
//					});
//				}
//			} while (cursor != null);
//			
//		} catch (Exception e) {
//			LOG.info("Migration social posts failed: {}", e.getMessage());
//		}
//		
//		return new ResponseEntity<Object>(HttpStatus.OK);
//	}
}
