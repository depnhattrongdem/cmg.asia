package asia.cmg.f8.social.service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.RatingSessionEvent;
import asia.cmg.f8.social.client.RatingSessionClient;
import asia.cmg.f8.social.client.UserClient;
import asia.cmg.f8.social.entity.AttributeEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.RatingRequest;
import asia.cmg.f8.social.entity.RatingSessionEntity;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.event.EventHandler;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Created on 12/7/16.
 */
@Service
public class RatingSessionService {

    private static final String SEARCH_RATING_SESSION_QUERY = "select * where user_id='%s'";
    private static final String QUERY_USERNAME_USER = "select name where uuid='%s'";
    private static final String QUERY_RATING_REASON_BY_CATEGORY_LANG =
            "select key, value where category='%1$s' and language='%2$s'";

    private static final String QUERY_RATING_REASON_BY_LANG =
            "select key, value where language='%s'";
    private static final String REASON_CATEGORY = "%s_rating_reason";

    private static final String UTF8 = "UTF-8";

    private final RatingSessionClient ratingSessionClient;

    private final UserClient userClient;

    private final EventHandler eventHandler;

    public RatingSessionService(final RatingSessionClient ratingSessionClient,
                                final UserClient userClient,
                                final EventHandler eventHandler) {
        this.ratingSessionClient = ratingSessionClient;
        this.userClient = userClient;
        this.eventHandler = eventHandler;
    }

	public Observable<PagedResponse<RatingSessionEntity>> getRatingUser(

			final String userId, final String cursor, final int limit) throws UnsupportedEncodingException {

		return Observable.zip(loadUserRating(userId, cursor, limit), countRatingByUser(userId), (rating, count) -> {
			rating.setCount(count);
			return rating;
		});
	}

	private Observable<PagedResponse<RatingSessionEntity>> loadUserRating(final String userId, final String cursor,
			final int limit) {

		return Observable.fromCallable(() -> {

			final PagedResponse<RatingSessionEntity> response;
			if (StringUtils.isNotBlank(cursor)) {
				response = ratingSessionClient.searchRatingOfUserWithCursor(
						String.format(SEARCH_RATING_SESSION_QUERY, userId), URLDecoder.decode(cursor, UTF8), limit);
			} else {
				response = ratingSessionClient
						.searchPagingRatingSessionOfUser(String.format(SEARCH_RATING_SESSION_QUERY, userId), limit);
			}
			return response;
		});
	}

    public Optional<RatingSessionEntity> createRatingSessionUser(
            final Account account,
            final RatingRequest ratingRequest) {
        final String reviewerId = account.uuid();
        final String sessionId = ratingRequest.getSessionId();
        final String reaction = ratingRequest.getReaction();

        //get user name to populate Rating Entity
        final UserEntity userResp = userClient
                .getUserByQuery(String.format(QUERY_USERNAME_USER, reviewerId))
                .getEntities().get(0);

        final RatingSessionEntity entity = new RatingSessionEntity();
        entity.setName(reviewerId + sessionId); // Prevent user re-rates session

        entity.setUserId(ratingRequest.getUserId());
        entity.setReviewerId(reviewerId);
        entity.setReviewerName(userResp.getName());
        entity.setReaction(StringUtils.isEmpty(reaction) ? " " : reaction);
        entity.setReasons(Collections.emptyList());
        entity.setStars(ratingRequest.getStars());

        entity.setSessionId(sessionId);
        entity.setSessionDate(ratingRequest.getSessionDate());
        entity.setComment(ratingRequest.getComment());
        

        final UserGridResponse<RatingSessionEntity> ratingResp =
                ratingSessionClient.rateSessionUser(entity);

        if (ratingResp == null || ratingResp.getEntities().isEmpty()) {
            return Optional.empty();
        }

        //fire RatingSessionEvent to update totalRate and numberOfRate of user
        final RatingSessionEvent ratingSessionEvent = new RatingSessionEvent();
        ratingSessionEvent.setEventId(UUID.randomUUID().toString());
        ratingSessionEvent.setStars(ratingRequest.getStars());
        ratingSessionEvent.setUserId(ratingRequest.getUserId());
        ratingSessionEvent.setSubmittedAt(System.currentTimeMillis());
        ratingSessionEvent.setSessionId(sessionId);
        ratingSessionEvent.setUserType(account.type());
        eventHandler.publish(ratingSessionEvent);

        return ratingResp.getEntities().stream().findFirst();
    }

    public Optional<RatingSessionEntity> updateRatingSessionUser(final String ratingSessionUserId,
                                                                 final Map<String, List<String>> reasons) {
        final UserGridResponse<RatingSessionEntity> updatedEntity =
                ratingSessionClient.updateReasonRatingSession(ratingSessionUserId, reasons);

        return updatedEntity != null
                ? updatedEntity.getEntities().stream().findFirst()
                : Optional.empty();
    }

    public List<AttributeEntity> getRatingReasonByUserType(final String userType, final String lang) {
        final String category = String.format(REASON_CATEGORY, userType);
        return ratingSessionClient.getRatingReason(
                String.format(QUERY_RATING_REASON_BY_CATEGORY_LANG, category, lang)).getEntities();
    }

    public List<AttributeEntity> getRatingReasonByAdmin(final String language) {
        return ratingSessionClient.getRatingReason(
                String.format(QUERY_RATING_REASON_BY_LANG, language)).getEntities();
    }

    /**
     * A convenience method to count number of rating of given PT. This only works with small data-set such as rating data.
     * For larger data-set, it's preferred to use counter instead.
     *
     * @param userUuid the uuid of pt
     * @return the number of rating
     */
    private Observable<Integer> countRatingByUser(final String userUuid) {
        return Observable.fromCallable(() -> {
          final PagedResponse<RatingSessionEntity> response = ratingSessionClient.searchPagingRatingSessionOfUser("select uuid where user_id='" + userUuid + "'", Integer.MAX_VALUE);
           if (response == null || response.isEmpty()) {
                return 0;
            }
            return response.getCount();
        });
    }
    
    
    public boolean isRatingSelected(final String pu_uuid, final String rating_uuid) {

		try {
			final List<RatingSessionEntity> ratingsessins = ratingSessionClient.getRatingSessions(pu_uuid, rating_uuid).getEntities();

			if (!ratingsessins.isEmpty()) {

				return true;
			}
		} catch (Exception e) {

			return false;
		}

		return false;
	}
}
