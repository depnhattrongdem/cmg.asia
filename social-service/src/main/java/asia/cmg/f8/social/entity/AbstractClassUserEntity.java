package asia.cmg.f8.social.entity;

import asia.cmg.f8.common.spec.view.CheckInUser;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@JsonInclude(JsonInclude.Include.ALWAYS)
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@SuppressWarnings({"CheckReturnValue", "PMD"})
public abstract class AbstractClassUserEntity implements CheckInUser {
    @Nullable
    abstract String getPhone();

    @Nullable
    abstract String getEmail();

}