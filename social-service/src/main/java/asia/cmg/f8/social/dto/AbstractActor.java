package asia.cmg.f8.social.dto;

import javax.annotation.Nullable;

import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import asia.cmg.f8.common.spec.user.UserType;

/**
 * Created on 12/21/16.
 */
@Value.Immutable
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@SuppressWarnings("CheckReturnValue")
public interface AbstractActor {
    @JsonProperty("uuid")
    @Nullable
    String getId();

    @JsonProperty("picture")
    @Nullable
    String getPicture();

    @JsonProperty("username")
    @Nullable
    String getUsername();

    @JsonProperty("name")
    @Nullable
    String getName();
    
    @JsonProperty("level")
    @Nullable
    String getLevel();

    @JsonProperty("user_type")
    @Nullable
    UserType getUserType();
    
    @JsonProperty("approved")
    @Nullable
    Boolean getApproved();
}
