package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.ClubEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created on 12/6/16.
 */
@FeignClient(value = "clubs", url = "${feign.url}", fallback = ClubClientFallbackImpl.class)
public interface ClubClient {
    String SECRET_QUERY = "client_id=${socials.userGridClientId}&client_secret=${socials.userGridClientSecret}";
    String GET_CLUBS = "/clubs?" + SECRET_QUERY + "&limit=${socials.maxClubs}";
    String GET_ONE_BY_UUID = "clubs/{uuid}?" + SECRET_QUERY; 
    String POST_CLUBS = "/clubs?" + SECRET_QUERY;
    String PUT_CLUBS = "/clubs/{uuid}?" + SECRET_QUERY;

    @RequestMapping(value = GET_CLUBS, method = GET, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<ClubEntity> getAllClubs();
    
    @RequestMapping(value = POST_CLUBS, method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    UserGridResponse<ClubEntity> createClub(@RequestBody final ClubEntity clubEntity);
    
    @RequestMapping(value = PUT_CLUBS, method = RequestMethod.PUT, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    UserGridResponse<ClubEntity> updateClub(@PathVariable(name = "uuid") final String uuid, 
    										@RequestBody final ClubEntity clubEntity);
    
    @RequestMapping(value = GET_ONE_BY_UUID, method = GET, produces = APPLICATION_JSON_VALUE)
    UserGridResponse<ClubEntity> getOneByUuid(@PathVariable(name = "uuid") final String uuid);
}
