package asia.cmg.f8.social.service;

import asia.cmg.f8.social.client.CounterClient;
import asia.cmg.f8.social.dto.Counter;
import asia.cmg.f8.social.util.SocialConstant;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created on 12/21/16.
 */
@Service
public class CounterService {
    private final CounterClient counterClient;

    public CounterService(final CounterClient counterClient) {
        this.counterClient = counterClient;
    }

    /**
     * Get number of post's comments, number of post's likes
     *
     * @param listPostUuid         list of post's uuids
     * @param listPostRequestUuid  list of post's request_uuids
     * @return Map with counter name as key and Integer value as numbers of count
     */
    public Observable<Map<String, Integer>> getNumberOfLikesCommentOfPost(
            final Collection<String> listPostUuid,
            final Collection<String> listPostRequestUuid) {

//        final int capacityOfListCounter = listPostUuid.size() + listPostRequestUuid.size();
        final Set<String> listCounter = new HashSet<>();

        listPostUuid
                .stream()
                .filter(Objects::nonNull)
                .forEach(postUuid -> {
                    final String likePostCounterName = SocialConstant.getLikeOfPostCounterName(postUuid);
                    final String viewPostCounterName = SocialConstant.getViewPostCouterName(postUuid);
                    listCounter.add(likePostCounterName);
                    listCounter.add(viewPostCounterName);
                });

        listPostRequestUuid
                .stream()
                .filter(Objects::nonNull)
                .forEach(postRequestUuid -> {
                    final String commentPostCounterName = SocialConstant.getCommentOfPostCounterName(postRequestUuid);
                    listCounter.add(commentPostCounterName);
                });
        
        return counterClient
                .getCounters(listCounter)
                .map(counters -> counters
                        .stream()
                        .collect(Collectors.toMap(Counter::getName,
                                       counter -> Integer.max(0, counter.getValue()))))
                .firstOrDefault(Collections.emptyMap());
    }

    /**
     * @param listCommentId
     * @return Map with counter name as key and Integer value as numbers of count
     */
    public Observable<Map<String, Integer>> getTotalLikesOfComments(final Collection<String> listCommentId) {

        return counterClient
                .getCounters(listCommentId.stream()
                        .map(SocialConstant::getLikeCommentCounterName)
                        .collect(Collectors.toSet()))
                .map(counters -> counters
                        .stream()
                        .collect(Collectors.toMap(Counter::getName,
                                        counter -> Integer.max(0, counter.getValue()))))
                .firstOrDefault(Collections.emptyMap());
    }
}
