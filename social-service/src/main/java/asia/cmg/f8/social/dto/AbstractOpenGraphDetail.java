package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.immutables.value.Value;

import javax.annotation.Nullable;

/**
 * Created on 12/21/16.
 */
@Value.Immutable
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@SuppressWarnings("CheckReturnValue")
public interface AbstractOpenGraphDetail {
    @JsonProperty("image")
    @Nullable
    String getImage();

    @JsonProperty("title")
    @Nullable
    String getTitle();

    @JsonProperty("description")
    @Nullable
    String getDescription();

    @JsonProperty("domain")
    @Nullable
    String getDomain();

    @JsonProperty("link")
    @Nullable
    String getLink();
}
