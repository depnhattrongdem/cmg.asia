package asia.cmg.f8.social.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;

import javax.annotation.Nullable;

/**
 * Created on 12/21/16.
 */
@Value.Immutable
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {
                JsonIgnoreProperties.class,
                JsonInclude.class,
                JsonSerialize.class,
                JsonDeserialize.class})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(as = ActivityEntity.class)
@JsonDeserialize(builder = ActivityEntity.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface AbstractActivityEntity {

    @JsonProperty("uuid")
    @Nullable
    String getUuid();

    @JsonProperty("verb")
    @Nullable
    ActivityVerbType getVerb();

    @JsonProperty("published")
    @Nullable
    Long getPublished();

    @JsonProperty("actor")
    @Nullable
    ActorEntity getActor();

    @JsonProperty("text")
    @Nullable
    String getText();

    @JsonProperty("content_type")
    @Nullable
    PostContentType getContentType();

//    @JsonProperty("link")
//    @Nullable
//    String getLink();
    
    @JsonProperty("links")
    @Nullable
    List<ResourceLink> getLinks();

    @JsonProperty("show_open_graph")
    @Nullable
    Boolean isShowOpenGraph();

    @JsonProperty("thumbnail_image_link")
    @Nullable
    String getThumbnailImageLink();

//    @JsonProperty("thumbnail_link")
//    @Nullable
//    String getThumbnailLink();
    
    @JsonProperty("status")
    @Nullable
    PostStatusType getStatus();
    
    //OpenGraph properties
    @JsonProperty("open_graph_image")
    @Nullable
    String getOpenGraphImage();

    @JsonProperty("open_graph_description")
    @Nullable
    String getOpenGraphDescription();

    @JsonProperty("open_graph_title")
    @Nullable
    String getOpenGraphTitle();

    @JsonProperty("open_graph_domain")
    @Nullable
    String getOpenGraphDomain();

    @JsonProperty("open_graph_link")
    @Nullable
    String getOpenGraphLink();

    @JsonProperty("owner_id")
    @Nullable
    String getOwnerId();
    
    @JsonProperty("request_uuid")
    @Nullable
    String getRequestUuid();

    @JsonProperty("tagged_uuids")
    @Nullable
	List<String> getTaggedUuids();
    
    @JsonProperty("video_duration")
    @Nullable
    Integer getVideoDuration();
}
