package asia.cmg.f8.social.api;

import asia.cmg.f8.common.context.LanguageContext;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.security.annotation.RequiredAdminRole;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.entity.AttributeEntity;
import asia.cmg.f8.social.entity.PagedRatingSessionResponse;
import asia.cmg.f8.social.entity.RatingRequest;
import asia.cmg.f8.social.entity.RatingSessionEntity;
import asia.cmg.f8.social.entity.RatingSessionUserImpl;
import asia.cmg.f8.social.entity.Reason;
import asia.cmg.f8.social.service.RatingSessionService;
import asia.cmg.f8.social.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rx.Observable;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created on 12/7/16.
 */
@RestController
public class RatingSessionApi {
    private static final Logger LOG = LoggerFactory.getLogger(RatingSessionApi.class);
    private static final String RATING_ID = "rating_id";
    private static final String REASONS_PROPERTIES = "reasons";
    private final RatingSessionService ratingSessionService;

    private final UserService userService;

    public RatingSessionApi(final RatingSessionService ratingSessionService,
                            final UserService userService) {
        this.ratingSessionService = ratingSessionService;
        this.userService = userService;
    }

    @RequiredAdminRole
    @RequestMapping(
            value = "/ratings/session/users/{user_id}", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<PagedRatingSessionResponse<RatingSessionUserImpl>> searchSessionRatingByUserId(
            @PathVariable("user_id") final String userId,
            @RequestParam(required = false) final String cursor,
            @RequestParam(value = "limit", defaultValue = "10") final int limit)
            throws UnsupportedEncodingException {

        return ratingSessionService.getRatingUser(userId, cursor, limit).map(response -> {

            final PagedRatingSessionResponse<RatingSessionUserImpl> result = new PagedRatingSessionResponse<>();
            result.setCursor(response.getCursor());
            result.setTotal(response.getCount());

            final List<RatingSessionEntity> ratings = response.getEntities();
            if (ratings == null || ratings.isEmpty()) {
                result.setContent(Collections.emptyList());
                return result;
            }

            // populate basic user information
            final Set<String> reviewerIds = ratings.stream().map(RatingSessionEntity::getReviewerId).collect(Collectors.toSet());
            final Map<String, String> userAvatars = userService.getAvatars(reviewerIds).toBlocking().first();
            result.setContent(response.getEntities()
                    .stream()
                    .map(rating -> RatingSessionUserImpl
                            .builder()
                            .picture(getUserAvatar(userAvatars, rating.getReviewerId()))
                            .id(rating.getReviewerId())
                            .fullName(rating.getReviewerName())
                            .reaction(rating.getReaction())
                            .stars(rating.getStars())
                            .sessionDate(rating.getSessionDate())
                            .reasons(rating.getReasons())
                            .comment(rating.getComment())
                            .build())
                    .collect(Collectors.toList()));
            return result;
        });
    }

    private String getUserAvatar(final Map<String, String> userAvatars, final String userUuid) {
        final String avatar = userAvatars.get(userUuid);
        if (!StringUtils.isEmpty(avatar)) {
            return avatar;
        }
        return StringUtils.EMPTY;
    }

    @RequestMapping(
            value = "/ratings/session", method = RequestMethod.POST,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity rateSessionUser(
            final Account account,
            @RequestBody @Valid final RatingRequest ratingRequest) {
       
    	final Optional<RatingSessionEntity> ratingResp =
                ratingSessionService.createRatingSessionUser(account, ratingRequest);
        if (ratingResp.isPresent()) {
            LOG.info("User {} votes User {} successfully for session {}",
                    account.uuid(), ratingRequest.getUserId(), ratingRequest.getSessionId());
            return new ResponseEntity<>(
                    Collections.singletonMap(RATING_ID, ratingResp.get().getUuid()),
                    HttpStatus.OK);
        }
        LOG.error("User {} can not re-votes User {} for session {}",
                account.uuid(), ratingRequest.getUserId(), ratingRequest.getSessionId());
        return new ResponseEntity<>(ErrorCode.RATING_DUPLICATE, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(
            value = "/ratings/session/{rating_id}", method = RequestMethod.PUT,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity updateReasonSessionRating(
            @PathVariable(RATING_ID) final String ratingId,
            @RequestBody final Map<String, List<String>> updateReasonRequest) {

        if (StringUtils.isBlank(ratingId)
                || !updateReasonRequest.containsKey(REASONS_PROPERTIES)) {
            return new ResponseEntity<>(ErrorCode.REQUEST_INVALID, HttpStatus.BAD_REQUEST);
        }
        final Optional<RatingSessionEntity> ratingResp =
                ratingSessionService.updateRatingSessionUser(ratingId, updateReasonRequest);
        if (ratingResp.isPresent()) {
            return new ResponseEntity<>(
                    Collections.singletonMap(RATING_ID, ratingResp.get().getUuid()),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(ErrorCode.REQUEST_INVALID, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(
            value = "/ratings/session/reasons", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public List<Reason> getListReasonRating(
            final Account account,
            final LanguageContext language) {
        return ratingSessionService.getRatingReasonByUserType(account.type(), language.language())
                .stream()
                .map(this::parseReasonSetting)
                .distinct()
                .collect(Collectors.toList());
    }

    @RequiredAdminRole
    @RequestMapping(
            value = "/admin/ratings/session/reasons", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public List<Reason> getListReasonRatingByAdmin(
            final LanguageContext language) {
        return ratingSessionService.getRatingReasonByAdmin(language.language())
                .stream()
                .map(this::parseReasonSetting)
                .distinct()
                .collect(Collectors.toList());
    }

    private Reason parseReasonSetting(final AttributeEntity attributeEntity) {
        return Reason.builder()
                .key(attributeEntity.getKey())
                .value(attributeEntity.getValue()).build();
    }
   
    
    @RequestMapping(
            value = "/ratings/session/users/{user_id}/all", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<PagedRatingSessionResponse<RatingSessionUserImpl>> searchAllSessionRatingByUserId(
            @PathVariable("user_id") final String userId,
            @RequestParam(required = false) final String cursor,
            @RequestParam(value = "limit", defaultValue = "10") final int limit)
            throws UnsupportedEncodingException {
    	
    	LOG.info("***********------------ <><><><><><><><><> ------------- *************");
    	
    	  return ratingSessionService.getRatingUser(userId, cursor, limit).map(response -> {

              final PagedRatingSessionResponse<RatingSessionUserImpl> result = new PagedRatingSessionResponse<>();
              result.setCursor(response.getCursor());
              result.setTotal(response.getCount());

              final List<RatingSessionEntity> ratings = response.getEntities();
              if (ratings == null || ratings.isEmpty()) {
                  result.setContent(Collections.emptyList());
                  return result;
              }

              // populate basic user information
              final Set<String> reviewerIds = ratings.stream().map(RatingSessionEntity::getReviewerId).collect(Collectors.toSet());
              final Map<String, String> userAvatars = userService.getAvatars(reviewerIds).toBlocking().first();
              result.setContent(response.getEntities()
                      .stream()
                      .map(rating -> RatingSessionUserImpl
                              .builder()
                              .picture(getUserAvatar(userAvatars, rating.getReviewerId()))
                              .id(rating.getUuid())
                              .fullName(rating.getReviewerName())
                              .reaction(rating.getReaction())
                              .stars(rating.getStars())
                              .sessionDate(rating.getSessionDate())
                              .reasons(rating.getReasons())
                              .comment(rating.getComment())
                              .isSelected(ratingSessionService.isRatingSelected(userId, rating.getUuid()))
                              .build())
                      .collect(Collectors.toList()));
              return result;
          });
    	
    } 
    
}
