package asia.cmg.f8.social.service;

/**
 * Created by vinh.nguyen.quang on 08/07/19.
 */
public class QuestionQuery {
    private final String type;
    private final String language;

    public QuestionQuery(final String type, final String language) {
        this.type = type;
        this.language = language;
    }

    public String getType() {
        return type;
    }

    public String getLanguage() {
        return language;
    }
}
