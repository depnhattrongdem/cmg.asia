package asia.cmg.f8.social.api;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.dto.LikeCommentRequest;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.CommentEntity;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.service.SocialCommentService;
import asia.cmg.f8.social.service.SocialPostService;
import asia.cmg.f8.social.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created by on 12/30/16.
 */
@RestController
public class SocialInteractionApi {
    private static final Logger LOG = LoggerFactory.getLogger(SocialInteractionApi.class);

    private static final String SUCCESS = "success";

    private final SocialCommentService socialCommentService;
    private final SocialPostService socialPostService;
    private final UserService userService;

    public SocialInteractionApi(final SocialCommentService socialCommentService,
                                final SocialPostService socialPostService,
                                final UserService userService) {
        this.socialCommentService = socialCommentService;
        this.socialPostService = socialPostService;
        this.userService = userService;
    }

    @RequestMapping(
            value = "/likes/comments", method = RequestMethod.POST,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity likesComments(final Account account,
                                        @RequestBody final LikeCommentRequest request) {
        //Get current comment
        final Optional<CommentEntity> commentOptional =
                socialCommentService.getCommentById(request.getCommentId(), account);
        if (!commentOptional.isPresent()) {
            LOG.error("Comment does not exist");
            return new ResponseEntity<>(ErrorCode.INVALID_COMMENT, HttpStatus.BAD_REQUEST);
        }

        final Optional<ActivityEntity> postOptional = socialPostService
                .getValidActivityForUserInteraction(commentOptional.get().getPostId(), account.uuid());

        if (!postOptional.isPresent()) {
            LOG.error("post does not exist");
            return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
        }

        if (request.getLike()) {
            //Get user info who liked this comment.
            final Optional<UserEntity> userInfo = userService
                    .getBasicUserInfoByUuid(account.uuid());
            return new ResponseEntity<>(Collections
                    .singletonMap(SUCCESS, socialCommentService
                            .userLikeComment(account, request, userInfo.get(), commentOptional.get(), postOptional.get())),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(Collections
                .singletonMap(SUCCESS, socialCommentService.userUnLikeComment(account, request)),
                HttpStatus.OK);

    }

}
