package asia.cmg.f8.social.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.LikePostEvent;
import asia.cmg.f8.social.SocialPostCreatedEvent;
import asia.cmg.f8.social.client.SocialPostClient;
import asia.cmg.f8.social.config.SocialProperties;
import asia.cmg.f8.social.dto.EditPostRequest;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.ActivityVerbType;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.PostStatusType;
import asia.cmg.f8.social.entity.ReportAbuse;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.event.EventHandler;
import asia.cmg.f8.social.repository.ReportAbuseRepository;
import asia.cmg.f8.social.util.SocialConstant;
import rx.Observable;

/**
 * Created on 12/21/16.
 */
@Service
public class SocialPostService {

    private static final Logger LOG = LoggerFactory.getLogger(SocialPostService.class);
    private static final String GET_ACTIVITY_ERR =
            "user-grid went wrong while getting activities of user {}";

    private static final String QUERY_USER_LIKED_POST_BY_POST_ID = "select uuid where %s";

    private static final String QUERY_USER_LIKED_POST_CONNECTION = "select uuid where uuid='%s'";
    private static final String QUERY_PUBLISHED_POST_BY_ID =
            "select * where (uuid = '%1$s' or request_uuid = '%1$s') "
                    + "and verb = '" + ActivityVerbType.post + "' "
                    + "and status = '" + PostStatusType.published + "' "
                    + "order by published desc";

    //Valid post is: post of current user and status is not deleted
    private static final String QUERY_VALID_POST_OF_USER_BY_ID =
            "select * where (uuid = '%1$s' or request_uuid = '%1$s') "
                    + "and verb = '" + ActivityVerbType.post + "' "
                    + "and not (status = '" + PostStatusType.deleted + "') "
                    + "order by published desc";

    private static final String QUERY_VALID_POST_4_USER_INTERACTION =
            "select * where (uuid = '%1$s' or request_uuid = '%1$s') "
                    + "and verb = '" + ActivityVerbType.post + "' "
                    + "and ((owner_id = '%2$s' and not (status = '" + PostStatusType.deleted + "')) "
                    + "       or (not (owner_id = '%2$s') and status = '" + PostStatusType.published + "'))";

    private static final String REQUES_UUID_CHECK = "select * where request_uuid = '%s'";

    private final SocialPostClient postClient;

    private final EventHandler eventHandler;
    
    private final SocialProperties socialProperties;
    
    private final SocialCacheService socialCacheService;
    private final ReportAbuseRepository reportAbuseRepository;
    
	private ExecutorService executor;
	
    public SocialPostService(final SocialPostClient postClient,
                             final EventHandler eventHandler, final SocialProperties socialProperties, final SocialCacheService socialCacheService,
                             ReportAbuseRepository reportAbuseRepository) {
        this.postClient = postClient;
        this.eventHandler = eventHandler;
        this.socialProperties = socialProperties;
        this.executor = Executors.newFixedThreadPool(socialProperties.getNumberOfThreadsForPosting());
        this.socialCacheService = socialCacheService;
        this.reportAbuseRepository = reportAbuseRepository;
    }

	@Async
	public void createPost(final Account account, final ActivityEntity activityEntity, final UserEntity userInfo) {
		executor.submit(() -> {
			final UserGridResponse<ActivityEntity> createPostResp = postClient.createActivity(account.uuid(), activityEntity);

			if (Objects.isNull(createPostResp) || createPostResp.getEntities().isEmpty()) {
				LOG.error("user-grid could not create post for user: {}", userInfo.getUuid());
				return Optional.empty();
			}
			final ActivityEntity post = createPostResp.getEntities().get(0);
			LOG.info("--- >>> --- Fired event for created post {} ", post.getUuid());
			final List<String> taggedUuids = activityEntity.getTaggedUuids() != null ? activityEntity.getTaggedUuids()
					: new ArrayList<String>();

			String shortContent = this.buildNotificationContentOfCreatePostEvent(post);
						
			final SocialPostCreatedEvent createdPostEvent = SocialPostCreatedEvent.newBuilder()
					.setEventId(UUID.randomUUID().toString())
					.setPostId(post.getUuid())
					.setPostType(post.getContentType().toString())
					.setUsername(userInfo.getUsername())
					.setName(userInfo.getName())
					.setUserUuid(userInfo.getUuid())
					.setTaggedUuids(taggedUuids.stream().collect(Collectors.toList()))
					.setSubmittedAt(System.currentTimeMillis())
					.setShortContent(shortContent)
					.build();
					
			eventHandler.publish(createdPostEvent);
			return Optional.of(post);
		});
		invalidateSocialCache(account.uuid());
	}
	
	private String buildNotificationContentOfCreatePostEvent(final ActivityEntity activity) {
		String shortContent = "";
		String content = activity.getText();
		String title = activity.getOpenGraphTitle();
		
		if(!StringUtils.isEmpty(content)) {
			if(activity.isShowOpenGraph()) {
				if(content.startsWith("http") || content.startsWith("https") || content.startsWith("www")) {
					if(!StringUtils.isEmpty(title)) {
						if(title.length() > 70) {
							shortContent = "\"" + title.substring(0, 70) + "..." + "\"";
						} else {
							shortContent = "\"" + title + "\"";
						}
						return shortContent;
					} else {
						return shortContent;
					}
				} 
			} 
			
			if(content.length() > 70) {
				shortContent = "\"" + content.substring(0, 70) + "..." + "\"";
			} else {
				shortContent = "\"" + content + "\"";
			}
		}
		
		return shortContent;
	}
    
    public Optional<ActivityEntity> editPost(final String postId,
                                             final EditPostRequest request, final Account account) {
        final ActivityEntity updatedActivity;
        if (Objects.isNull(request.getOpenGraphDetail())) {
            updatedActivity = ActivityEntity.builder()
                    .isShowOpenGraph(request.isShowOpenGraph())
                    .text(request.getContent())
                    .taggedUuids(request.getTaggedUuids())
                    .build();
        } else {
            updatedActivity = ActivityEntity.builder()
                    .isShowOpenGraph(request.isShowOpenGraph())
                    .text(request.getContent())
                    .taggedUuids(request.getTaggedUuids())
                    
                    .openGraphImage(request.getOpenGraphDetail().getImage())
                    .openGraphDescription(request.getOpenGraphDetail().getDescription())
                    .openGraphTitle(request.getOpenGraphDetail().getTitle())
                    .openGraphDomain(request.getOpenGraphDetail().getDomain())
                    .openGraphLink(request.getOpenGraphDetail().getLink())
                    
                    .build();
        }

        final UserGridResponse<ActivityEntity> editPostResp =
                postClient.editPost(postId, updatedActivity);
        if (Objects.isNull(editPostResp) || editPostResp.isEmpty()) {
            return Optional.empty();
        }
        invalidateSocialCache(account.uuid());
        return editPostResp.getEntities().stream().findFirst();
    }

    /**
     * Current user who want to retrieve post of another user.
     *
     * @param userId  user uuid
     * @param cursor  cursor
     * @param limit   limit
     * @param account current user who want to retrieve post of @param userId
     * @return posts of userId
     */
    public PagedResponse<ActivityEntity> getActivityByUserId(final String userId,
                                                             final String cursor,
                                                             final int limit,
                                                             final Account account) {
        final PagedResponse<ActivityEntity> activityResp = socialCacheService.getActivityByUserId(userId, cursor, limit, account);
        if (Objects.isNull(activityResp) || activityResp.isEmpty()) {
            LOG.error(GET_ACTIVITY_ERR, userId);
            return new PagedResponse<>();
        }
        return activityResp;

    }

    public Observable<Set<String>> getUserLikedPostStatusByPostId(
            final String userId,
            final Collection<String> listPostId) {

        final String query = listPostId.stream().map(postId -> "uuid ='" + postId + "'").collect(Collectors.joining(" or "));

        final Observable<UserGridResponse<ActivityEntity>> userGridResponseObservable =
                postClient.getUserLikedPostStatusByQuery(userId,
                        String.format(QUERY_USER_LIKED_POST_BY_POST_ID, query));

        return userGridResponseObservable
                .map(activityEntityUserGridResponse -> activityEntityUserGridResponse
                        .getEntities()
                        .stream()
                        .map(ActivityEntity::getUuid)
                        .collect(Collectors.toSet()))
                .firstOrDefault(Collections.emptySet());
    }

    public PagedResponse<ActivityEntity> getFeedByUserId(final String userId,
                                                         final String cursor,
                                                         final int limit,
                                                         final Account account) {
        final PagedResponse<ActivityEntity> activityResp = socialCacheService.getFeedByUserId(userId, cursor, limit, account);
        if (Objects.isNull(activityResp) || activityResp.isEmpty()) {
            LOG.error("user-grid went wrong while getting feed of user {}", userId);
            return new PagedResponse<>();
        }

        return activityResp;
    }
    
    public PagedResponse<ActivityEntity> getLatestFeed(final String cursor, final int limit, final Account account) {
    	
    	final PagedResponse<ActivityEntity> activityResp = socialCacheService.getLatestFeed(cursor, limit, account);
    	
    	if(Objects.isNull(activityResp) || activityResp.isEmpty()) {
    		LOG.error("user-grid went wrong while getting latest feeds");
            return new PagedResponse<>();
    	}
    	
    	return activityResp;
    }

    public Optional<ActivityEntity> userLikesDisLikesPostId(final String userId,
            											final String postId, final Boolean isLike) {
    	UserGridResponse<ActivityEntity> activityResp = null;
    	synchronized (this) {//Put the synchronized key-word for same user unlike and like same many times
	    	if(isLike) {
	    		// User already like the post
	    		try {
					boolean isLiked = checkUserLikedPost(postId, userId);
					if (isLiked) {
						LOG.error("User already liked the post user {} post {}", userId, postId);
						return Optional.empty();
					}
					activityResp = postClient.userLikesPost(userId, postId);
				} catch (Exception e) {
					return Optional.empty();
				}
	    	}
	    	else {
	    		// User does like the post
	    		try {
					boolean isLiked = checkUserLikedPost(postId, userId);
					if (!isLiked) {
						LOG.error("User does not like the post before user {} post {}", userId, postId);
						return Optional.empty();
					}
					activityResp = postClient.userUnLikesPost(userId, postId);
				} catch (Exception e) {
					return Optional.empty();
				}
	    	}
	    	
	    	if (Objects.isNull(activityResp) || activityResp.getEntities().isEmpty()) {
	            return Optional.empty();
	        }
    	}
    	
    	final ActivityEntity postLiked = activityResp.getEntities().get(0);
    	final LikePostEvent likePostEvent = new LikePostEvent();
    	likePostEvent.setSubmittedAt(System.currentTimeMillis());
        likePostEvent.setEventId(UUID.randomUUID().toString());
        likePostEvent.setPostId(postId);
        likePostEvent.setCountValue((isLike)?SocialConstant.INCREASE : SocialConstant.DECREASE);
        if(isLike) {
        	likePostEvent.setUserUuid(userId);
            likePostEvent.setUsername(postLiked.getActor().getUsername());
            likePostEvent.setPostOwnerUuid(postLiked.getOwnerId());
            likePostEvent.setPostType(postLiked.getContentType().toString());
        }
        eventHandler.publish(likePostEvent);
        LOG.debug("Fired event update counter like post {}", postId);
    	return Optional.of(postLiked);
    }   

    public Optional<ActivityEntity> getPublishedActivityById(final String postId) {
        final UserGridResponse<ActivityEntity> activityResp =
                postClient.getActivityByQuery(
                        String.format(QUERY_PUBLISHED_POST_BY_ID,
                                postId));
        if (Objects.isNull(activityResp)) {
            LOG.error(GET_ACTIVITY_ERR, postId);
            return Optional.empty();
        }

        return activityResp.getEntities().stream().findFirst();
    }

    /**
     * Valid Activity for user interaction is:
     * post id = post id
     * AND (    user is owner of post and status is not deleted
     * OR user is not owner of post and status must be published ).
     *
     * @param postId post uuid
     * @param userId user uuid
     * @return Valid Activity for interaction
     */
    public Optional<ActivityEntity> getValidActivityForUserInteraction(final String postId,
                                                                       final String userId) {
        final UserGridResponse<ActivityEntity> activityResp =
                postClient.getActivityByQuery(
                        String.format(QUERY_VALID_POST_4_USER_INTERACTION, postId, userId));

        if (Objects.isNull(activityResp)) {
            LOG.error(GET_ACTIVITY_ERR, postId);
            return Optional.empty();
        }

        return activityResp.getEntities().stream().findFirst();
    }

    public Optional<ActivityEntity> getActivityOfUserById(final String postId,
                                                          final String userId) {
        final UserGridResponse<ActivityEntity> activityResp =
                postClient.getActivityOfUserByQuery(
                        userId,
                        String.format(QUERY_VALID_POST_OF_USER_BY_ID, postId));
        if (Objects.isNull(activityResp)) {
            LOG.error(GET_ACTIVITY_ERR, userId);
            return Optional.empty();
        }

        return activityResp.getEntities().stream().findFirst();
    }

    public boolean deletePost(final String postId, Account account) {

        //Mark status post as deleted
        final ActivityEntity updatedActivity = ActivityEntity.builder()
                .status(PostStatusType.deleted)
                .build();
        final UserGridResponse<ActivityEntity> updatedPostResp =
                postClient.editPost(postId, updatedActivity);
        if (Objects.isNull(updatedPostResp) || updatedPostResp.getEntities().isEmpty()) {
            LOG.error("user-grid could not update status delete post id {}", postId);
            return false;
        }
        invalidateSocialCache(account.uuid());
        return true;
    }

    public boolean reportAbusePost(Account account, String postId, String reason) {
        String uuid = account.uuid();
        ReportAbuse reportAbuse = reportAbuseRepository.findByUserIdAndPostId(uuid, postId);
        if (reportAbuse != null) {
            return true;
        }
        invalidateSocialCache(account.uuid());
        return reportAbuseRepository.save(new ReportAbuse(uuid, postId, reason)) != null;
    }
    
    private boolean checkUserLikedPost (final String postId, final String userId) throws Exception {
    	try {
    		//check the post has been like by user or not
            final UserGridResponse<ActivityEntity> likedPostResp = postClient.getUserLikedPostById(userId,
                            					String.format(QUERY_USER_LIKED_POST_CONNECTION, postId));
            
            return !Objects.isNull(likedPostResp) && !likedPostResp.getEntities().isEmpty();
		} catch (Exception e) {
			LOG.error("[checkUserLikedPost] error: {}", e.getMessage());
			throw e;
		}
    }
    
	public boolean checkRequestUuidAlreadyExists(final String requestUuid) {
		final UserGridResponse<ActivityEntity> requestid = postClient
				.getActivityByQuery(String.format(REQUES_UUID_CHECK, requestUuid));

		return requestid!=null && !requestid.getEntities().isEmpty();
    }
	
    public void invalidateSocialCache(String userId) {
        socialCacheService.invalidateCacheForUser(userId);
    }

}
