package asia.cmg.f8.social.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import asia.cmg.f8.social.dto.FollowUserDto;
import asia.cmg.f8.social.entity.FollowUserEntity;
import asia.cmg.f8.social.repository.FollowUserRepository;

@Service
public class FollowUserService {

	@Inject
	private FollowUserRepository followUserRepository;

	public List<FollowUserDto> searchFollowingUser(final String keyword, final String userId, final Integer limit) {
		final List<FollowUserEntity> followUsers = followUserRepository.searchFollowingUser(keyword, userId, limit);
		return followUsers.stream().map(u -> convertToDto(u)).collect(Collectors.toList());
	}

	private FollowUserDto convertToDto(FollowUserEntity entity) {
		final FollowUserDto dto = new FollowUserDto();
		dto.setFullName(entity.getFullName());
		dto.setActivated(entity.getActivated());
		dto.setAvatar(entity.getAvatar());
		dto.setEmail(entity.getEmail());
		dto.setId(entity.getId());
		dto.setUserName(entity.getUserName());
		dto.setUserType(entity.getUserType());
		dto.setUuid(entity.getUuid());
		return dto;
	}
}
