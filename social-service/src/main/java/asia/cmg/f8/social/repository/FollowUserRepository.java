package asia.cmg.f8.social.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import asia.cmg.f8.social.entity.FollowUserEntity;

@Repository
public interface FollowUserRepository extends JpaRepository<FollowUserEntity, Long> {

	@Query(value = "SELECT u.* FROM session_users u " 
			+ "JOIN following f ON u.uuid = f.followed_user_id "
			+ "WHERE (u.username LIKE CONCAT('%',:keyword,'%') OR u.full_name LIKE CONCAT('%',:keyword,'%')) AND f.user_id = :userId "
			+ "LIMIT :limit ", nativeQuery = true)
	List<FollowUserEntity> searchFollowingUser(@Param("keyword") final String keyword,
			@Param("userId") final String userId, @Param("limit") final Integer limit);
}
