package asia.cmg.f8.social.service;

import asia.cmg.f8.social.entity.FollowUserEntity;
import asia.cmg.f8.social.entity.Follower;
import asia.cmg.f8.social.repository.FollowerRepository;
import asia.cmg.f8.social.repository.FollowerSpecification;
import com.google.common.base.Preconditions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Service
public class FollowService {

    private final EntityManager entityManager;

    private final FollowerRepository followerRepository;

    public FollowService(FollowerRepository followerRepository,
                         EntityManager entityManager) {
        this.followerRepository = followerRepository;
        this.entityManager = entityManager;
    }

    @Transactional
    public Follower createFollowConnection(String followUserId, String followedUserId) {
        Preconditions.checkNotNull(followUserId);
        Preconditions.checkNotNull(followedUserId);

        Follower follower = followerRepository.findByUserIdAndFollowedUserId(followUserId, followedUserId);
        if (follower == null) {
            Follower creation = followerRepository.save(new Follower(followUserId, followedUserId));
            entityManager.refresh(creation);
            return creation;
        }
        return follower;
    }

    public Boolean checkFollowingConnection(String followUserId, String followedUserId) {
        Follower follower = followerRepository.findByUserIdAndFollowedUserId(followUserId, followedUserId);
        return follower != null;
    }

    public Page<String> getFollowerConnection(String userId, int page, int pageSize) {
        return followerRepository.findByFollowedUserId(userId, new PageRequest(page, pageSize))
                .map(Follower::getUserId);
    }

    public Page<FollowUserEntity> getFollowerConnectionEntity(String userId, int page, int pageSize, String userType, String keyword) {
        return followerRepository.findAll(new FollowerSpecification(null, userId, userType, keyword), new PageRequest(page, pageSize))
                .map(Follower::getFollowingUserEntity);
    }

    public Page<String> getFollowingConnection(String userId, int page, int pageSize) {
        return followerRepository.findByUserId(userId, new PageRequest(page, pageSize))
                .map(Follower::getFollowedUserId);
    }

    public Page<FollowUserEntity> getFollowingConnectionEntity(String userId, int page, int pageSize, String userType, String keyword) {
        return followerRepository.findAll(new FollowerSpecification(userId, null, userType, keyword), new PageRequest(page, pageSize))
                .map(Follower::getFollowerUserEntity);
    }

    public Long countFollowerConnection(String userId) {
        return followerRepository.countByFollowedUserId(userId);
    }

    public Long countFollowingConnection(String userId) {
        return followerRepository.countByUserId(userId);
    }

    @Transactional
    public Boolean deleteFollowingConnection(String userId, String followedUserId) {
        return followerRepository.removeByUserIdAndFollowedUserId(userId, followedUserId) > 0;
    }
}
