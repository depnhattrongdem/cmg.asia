package asia.cmg.f8.social.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "userprofile")
public class UserProfileProperties {
    private Questionaire questionaire = new Questionaire();

    public Questionaire getQuestionaire() {
        return questionaire;
    }

    public void setQuestionaire(final Questionaire questionaire) {
        this.questionaire = questionaire;
    }

    public class Questionaire {
        private int questionLimit;
        private int answerLimit;

        public int getQuestionLimit() {
            return questionLimit;
        }

        public void setQuestionLimit(final int questionLimit) {
            this.questionLimit = questionLimit;
        }

        public int getAnswerLimit() {
            return answerLimit;
        }

        public void setAnswerLimit(final int answerLimit) {
            this.answerLimit = answerLimit;
        }

    }
}
