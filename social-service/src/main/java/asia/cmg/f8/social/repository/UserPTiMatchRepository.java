package asia.cmg.f8.social.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import asia.cmg.f8.social.database.entity.UserPtiMatchEntity;

@Repository
public interface UserPTiMatchRepository extends JpaRepository<UserPtiMatchEntity, Long> {

	@Query(value = "SELECT * FROM user_ptimatch WHERE eu_uuid = :eu_uuid ORDER BY average DESC \n#pageable\n", 
			countQuery = "SELECT COUNT(*) FROM user_ptimatch WHERE eu_uuid = :eu_uuid", nativeQuery = true)
	Page<UserPtiMatchEntity> getByEuUuid(@Param(value = "eu_uuid") final String euUuid, final Pageable pageable);
	
	@Query(value = "SELECT * FROM user_ptimatch WHERE eu_uuid = :eu_uuid AND pt_uuid = :pt_uuid LIMIT 1", nativeQuery = true)
	Optional<UserPtiMatchEntity> getByEuUuidAndPtUuid(@Param(value = "eu_uuid") final String euUuid, @Param(value = "pt_uuid") final String ptUuid);
}
