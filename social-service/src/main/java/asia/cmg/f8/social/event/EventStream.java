package asia.cmg.f8.social.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created by 12/12/16.
 */
public interface EventStream {
    String RATING_SS_USER_INFO_INPUT_CHANNEL = "ratingSessionCompletedUserInfoInput";
    String RATING_SS_STATUS_RATED_INPUT_CHANNEL = "ratingSessionCompletedStatusRatedInput";
    String RATING_SESSION_OUTPUT_EVENT_CHANNEL = "ratingSessionCompletedOutput";

    String CHANGE_SESSION_STATUS_INPUT_CHANNEL = "changeSessionStatusInput";
    
    String LIKE_POST_OUT_CHANNEL = "likePostOutput";
    String LIKE_POST_IN_CHANNEL = "likePostInput";
    
    String VIEW_POST_OUT_CHANNEL = "viewPostOutput";
    String VIEW_POST_IN_CHANNEL = "viewPostInput";

    String USER_INFO_OUT_CHANNEL = "changeUserOut";

    String LIKE_COMMENT_OUT_CHANNEL = "likeCommentOutput";
    String LIKE_COMMENT_IN_CHANNEL = "likeCommentInput";

    String USER_CREATED_POST_OUT = "userCreatedPostOutput";

    @Input(RATING_SS_USER_INFO_INPUT_CHANNEL)
    SubscribableChannel handleRatingSessionCompletedUserInfo();

    @Input(RATING_SS_STATUS_RATED_INPUT_CHANNEL)
    SubscribableChannel handleRatingSessionCompletedStatusRated();

    @Output(RATING_SESSION_OUTPUT_EVENT_CHANNEL)
    MessageChannel ratingSessionCompleted();

    @Output(USER_INFO_OUT_CHANNEL)
    MessageChannel changeUserInfo();

    @Input(CHANGE_SESSION_STATUS_INPUT_CHANNEL)
    SubscribableChannel handleChangeSessionStatus();

    @Output(LIKE_POST_OUT_CHANNEL)
    MessageChannel likePost();
    
    @Input(LIKE_POST_IN_CHANNEL)
    SubscribableChannel handleLikePost();

    @Output(LIKE_COMMENT_OUT_CHANNEL)
    MessageChannel likeComment();

    @Input(LIKE_COMMENT_IN_CHANNEL)
    SubscribableChannel handleLikeComment();

    @Output(USER_CREATED_POST_OUT)
    MessageChannel createdPost();
    
    @Input(VIEW_POST_IN_CHANNEL)
    SubscribableChannel handleViewPost();
    
    @Output(VIEW_POST_OUT_CHANNEL)
    MessageChannel viewPost();
}
