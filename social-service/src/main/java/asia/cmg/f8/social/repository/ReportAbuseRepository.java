package asia.cmg.f8.social.repository;

import asia.cmg.f8.social.entity.ReportAbuse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReportAbuseRepository extends JpaSpecificationExecutor<ReportAbuse>, JpaRepository<ReportAbuse, String> {
    ReportAbuse findByUserIdAndPostId(String userId, String postId);

    @Query(value = "SELECT post_id from report_abuses where user_id=?1", nativeQuery = true)
    List<String> getUserReports(String userId);
}
