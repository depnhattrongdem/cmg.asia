package asia.cmg.f8.social.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import asia.cmg.f8.social.util.StringLowercaseSerializer;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created on 11/28/16.
 */
@Entity
@Table(name = "session_users")  
public class FollowUserEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Column(name = "uuid", length = 36, nullable = false)
    private String uuid;

    @Column(name = "email", nullable = false, length = 255)
    private String email;

    @JsonProperty("name")
    @Column(name = "full_name", length = 255)
    private String fullName;

    @JsonProperty("picture")
    @Column(name = "avatar", length = 1024)
    private String avatar;

    @JsonProperty("username")
    @Column(name = "username", length = 255)
    private String userName;

    @Column(name = "level")
    private String level;

    @Column(name = "activated")
    private Boolean activated;
    
    @JsonSerialize(using = StringLowercaseSerializer.class)
    @Column(name = "user_type", length = 5)
    private String userType;


    public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("uuid", uuid)
                .append("email", email)
                .append("fullName", fullName)
                .append("avatar", avatar)
                .append("userName", userName)
                .append("level", level)
                .append("activated", activated)
                .toString();
    }
}
