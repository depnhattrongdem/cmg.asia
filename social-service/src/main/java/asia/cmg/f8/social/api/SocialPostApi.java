package asia.cmg.f8.social.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;
import asia.cmg.f8.social.service.PTIMatchingService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import asia.cmg.f8.common.dto.QuestionDTO;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.spec.user.UserType;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.ViewPostEvent;
import asia.cmg.f8.social.config.SocialProperties;
import asia.cmg.f8.social.dto.CreatePostRequest;
import asia.cmg.f8.social.dto.DataMatchDto;
import asia.cmg.f8.social.dto.EditPostRequest;
import asia.cmg.f8.social.dto.LikePostRequest;
import asia.cmg.f8.social.dto.Post;
import asia.cmg.f8.social.dto.ReportPostRequest;
import asia.cmg.f8.social.dto.UserPtiMatchDTO;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.PostContentType;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.event.EventHandler;
import asia.cmg.f8.social.service.CounterService;
import asia.cmg.f8.social.service.SocialPostService;
import asia.cmg.f8.social.service.UserPtiMatchService;
import asia.cmg.f8.social.service.UserService;
import asia.cmg.f8.social.util.SocialConstant;
import asia.cmg.f8.social.util.SocialConverterUtil;
import rx.Observable;

/**
 * Created on 12/21/16.
 */
@RestController
public class SocialPostApi {
    private static final Logger LOG = LoggerFactory.getLogger(SocialPostApi.class);
    private static final Gson gson = new Gson();
    private static final String SUCCESS = "success";
    private static final String NOT_FOUND_POST_ERR = "Could not found post Id: {}";
    private List<DataMatchDto> NO_NEED_MATCHING = Collections.emptyList();
    private UserPtiMatchDTO NO_USER_PTI_MATCH_DATA = null;

    private final SocialPostService postService;
    private final UserService userService;
    private final CounterService counterService;
    private final SocialProperties socialProperties;
    private final PTIMatchingService ptiMatchingService;
    private final UserPtiMatchService userPtiMatchService;
    
    @Autowired
    private EventHandler eventHandler;

    public SocialPostApi(final SocialPostService postService,
                         final UserService userService,
                         final CounterService counterService,
                         final SocialProperties socialProperties,
                         final PTIMatchingService ptiMatchingService,
                         final UserPtiMatchService userPtiMatchService) {
        this.postService = postService;
        this.userService = userService;
        this.counterService = counterService;
        this.socialProperties = socialProperties;
        this.ptiMatchingService = ptiMatchingService;
        this.userPtiMatchService = userPtiMatchService;
    }

    @RequestMapping(value = "/apple-app-site-association", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> getAppleAssociationContent() {
        return new ResponseEntity<>(socialProperties.getAppleAssociationContent(), HttpStatus.OK);
    }

    @RequestMapping(value = "/social/posts/{postid}", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> getPost(@PathVariable("postid") final String postId) {

        final Optional<ActivityEntity> activityResp = postService.getPublishedActivityById(postId);
        if (activityResp.isPresent()) {
            return new ResponseEntity<>(activityResp.get(), HttpStatus.OK);
        }

        LOG.error("Could not found post Id: {} ", postId);
        return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/social/users/posts/{post_id}", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> getPostById(final Account account,
                                      @PathVariable("post_id") final String postId) {
    	LOG.info("Request getPostById {}", postId);
    	
        final Optional<ActivityEntity> activityResp =
                postService.getValidActivityForUserInteraction(postId, account.uuid());
        if (activityResp.isPresent()) {
            final ActivityEntity activityEntity = activityResp.get();
            final String actorId = activityEntity.getActor().getId();
            final String uuid = activityEntity.getUuid();
            final String requestUuid = activityEntity.getRequestUuid();
            Map<String, QuestionDTO> questionsMap = ptiMatchingService.getQuestionsMap();

            final Post result = Observable.zip(
                    userService.getBasicUserInfoByUUIDs(Collections.singletonList(actorId)),
                    postService.getUserLikedPostStatusByPostId(account.uuid(), Collections.singletonList(uuid)),
                    counterService.getNumberOfLikesCommentOfPost(Collections.singletonList(uuid), Collections.singletonList(requestUuid)),
                    (mapUsers, listLikedPost, listLikeCommentCounter) -> SocialConverterUtil.convertActivityToPost(
                            activityEntity,
                            listLikeCommentCounter.getOrDefault(SocialConstant.getLikeOfPostCounterName(uuid), 0),
                            listLikeCommentCounter.getOrDefault(SocialConstant.getCommentOfPostCounterName(requestUuid), 0),
                            listLikeCommentCounter.getOrDefault(SocialConstant.getViewPostCouterName(uuid), 0),
                            mapUsers.get(actorId).withUserType(activityEntity.getActor().getUserType()),
                            listLikedPost.contains(uuid),
                            getSharedLinkOfPostId(uuid),
                            activityEntity.getTaggedUuids(),
                            ptiMatchingService.getDataMatchs(activityEntity.getActor().getId(), questionsMap),
                            activityEntity.getActor().getUserType() == UserType.PT ? userPtiMatchService.getByEuUuidAndPtUuid(account.uuid(), actorId) : null))
                    .doOnError(error -> LOG.error("Populating Activity data occurs Error {}", error))
                    .toBlocking()
                    .firstOrDefault(Post.builder().build());
            
            // Fire event to increase number of view by 1 of this post
            LOG.info("Fire event ViewPostEvent with post uuid {}", uuid);
            ViewPostEvent viewPostEvent = new ViewPostEvent();
            viewPostEvent.setEventId(UUID.randomUUID().toString());
            viewPostEvent.setPostId(uuid);
            viewPostEvent.setSubmittedAt(System.currentTimeMillis());
            eventHandler.publish(viewPostEvent);
            
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        
        return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/social/users/posts", method = RequestMethod.POST,
            produces = APPLICATION_JSON_UTF8_VALUE,
            consumes = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> createPost(final Account account,
                                        @RequestBody @Valid final CreatePostRequest request) {

    	LOG.info("[createPost] Logged request data: {}", gson.toJson(request));
    	
        if (isInvalidPostRequest(request.getContent(), request.getContentType())) {
            LOG.debug("Required content for the text type");
            return new ResponseEntity<>(
                    ErrorCode.REQUEST_INVALID.withDetail("content, content_type"),
                    HttpStatus.BAD_REQUEST);
        }

        if (postService.checkRequestUuidAlreadyExists(request.getRequestUuid())) {
            return new ResponseEntity<>(ErrorCode.ENTRY_EXIST
                    .withDetail("Request Uuid is  Duplicated"),
                    HttpStatus.BAD_REQUEST);
        }

        final String currentUserId = account.uuid();

        //Get user info who created this post.
        final Optional<UserEntity> userInfo = userService
                .getBasicUserInfoByUuid(currentUserId);

        if (userInfo.isPresent()) {
            UserEntity userEntity = userInfo.get();
            if (userEntity.getUserType() == null) {
                userEntity = userEntity.withUserType(UserType.valueOf(account.type().toUpperCase()));
            }
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

            ActivityEntity activityEntity = SocialConverterUtil.converterPostRequestToActivityEntity(request, userEntity)
                    .withPublished(timestamp.getTime());

            postService.createPost(account, activityEntity, userEntity);

            int numberOfLikes = 0;
            int numberOfComments = 0;
            int numberOfViews = 0;
            Boolean isUserLiked = false;
            String sharedLink = getSharedLinkOfPostId(request.getRequestUuid());
            return new ResponseEntity<>(
                    SocialConverterUtil.convertActivityToPost(
                            activityEntity,
                            numberOfLikes,
                            numberOfComments,
                            numberOfViews,
                            userEntity,
                            isUserLiked,
                            sharedLink,
                            request.getTaggedUuids(),
                            NO_NEED_MATCHING,
                            NO_USER_PTI_MATCH_DATA),
                    HttpStatus.OK);
        }

        LOG.error("user-grid could not found user: {}", currentUserId);
        return new ResponseEntity<>(ErrorCode.INTERNAL_SERVER_ERROR,
                HttpStatus.INTERNAL_SERVER_ERROR);

    }

    
    @RequestMapping(value = "/social/users/posts/{post_id}", method = RequestMethod.PUT,
            produces = APPLICATION_JSON_UTF8_VALUE,
            consumes = APPLICATION_JSON_UTF8_VALUE)
    public Observable<ResponseEntity> editPost(final Account account,
                                               @PathVariable("post_id") final String postId,
                                               @RequestBody @Valid final EditPostRequest request) {
    	LOG.info("[editPost] Logged post_id[{}] and request body[{}]", postId, gson.toJson(request));
    	
        final String currentUserId = account.uuid();

        //Check existing post id of user with status is not 'deleted' and verb is 'post'
        final Optional<ActivityEntity> existingActivityOptional =
                postService.getActivityOfUserById(postId, currentUserId);
        if (!existingActivityOptional.isPresent()) {
            LOG.debug(NOT_FOUND_POST_ERR, postId);
            return Observable
                    .just(new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST));
        }

        final String uuid = existingActivityOptional.get().getUuid();
        final String requestUuid = existingActivityOptional.get().getRequestUuid();

        if (isInvalidPostRequest(request.getContent(),
                existingActivityOptional.get().getContentType())) {
            LOG.debug("Required content for the text type");
            return Observable
                    .just(new ResponseEntity<>(
                            ErrorCode.REQUEST_INVALID.withDetail("content, content_type"),
                            HttpStatus.BAD_REQUEST));
        }

        final Optional<ActivityEntity> editedActivity = postService.editPost(uuid, request, account);

        if (!editedActivity.isPresent()) {
            LOG.debug("User {} could not edit post Id {}", currentUserId, uuid);
            return Observable
                    .just(new ResponseEntity<>(ErrorCode.REQUEST_INVALID, HttpStatus.BAD_REQUEST));
        }

        return Observable.zip(userService.getBasicUserInfoByUUIDs(Collections.singletonList(currentUserId)),
                postService.getUserLikedPostStatusByPostId(currentUserId, Collections.singletonList(uuid)),
                counterService.getNumberOfLikesCommentOfPost(Collections.singletonList(uuid), Collections.singletonList(requestUuid)),
                (mapUsers, listLikedPost, likeAndCommentCounter) -> new ResponseEntity(
                        SocialConverterUtil.convertActivityToPost(
                                editedActivity.get(),
                                likeAndCommentCounter.getOrDefault(SocialConstant.getLikeOfPostCounterName(uuid), 0),
                                likeAndCommentCounter.getOrDefault(SocialConstant.getCommentOfPostCounterName(requestUuid), 0),
                                likeAndCommentCounter.getOrDefault(SocialConstant.getViewPostCouterName(uuid), 0),
                                mapUsers.get(currentUserId),
                                listLikedPost.contains(uuid),
                                getSharedLinkOfPostId(uuid),
                                request.getTaggedUuids(),
                                NO_NEED_MATCHING,
                                NO_USER_PTI_MATCH_DATA),
                        HttpStatus.OK))
                .doOnError(error -> LOG.error("Populating Activity data occurs Error {}", error))
                .firstOrDefault(new ResponseEntity<>(ErrorCode.REQUEST_INVALID, HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/social/users/posts/{post_id}", method = RequestMethod.DELETE,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> deletePost(final Account account,
                                     @PathVariable("post_id") final String postId) {

        //Check existing post id of user with status is not 'deleted' and verb is 'post'
        final Optional<ActivityEntity> existingActivityOptional =
                postService.getActivityOfUserById(postId, account.uuid());
        if (!existingActivityOptional.isPresent()) {
            LOG.debug(NOT_FOUND_POST_ERR, postId);
            return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(
                Collections.singletonMap(SUCCESS,
                        postService.deletePost(existingActivityOptional.get().getUuid(), account)), HttpStatus.OK);

    }

    @RequestMapping(value = "/likes/posts", method = RequestMethod.POST,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> likesPost(final Account account,
                                    @RequestBody @Valid final LikePostRequest request) {
        final String postId = request.getPostId();
        final Optional<ActivityEntity> existingActivityOptional =
                postService.getValidActivityForUserInteraction(postId, account.uuid());
        if (!existingActivityOptional.isPresent()) {
            LOG.debug(NOT_FOUND_POST_ERR, postId);
            return new ResponseEntity<>(ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(
                Collections.singletonMap(SUCCESS,
                        postService.userLikesDisLikesPostId(account.uuid(),
                                existingActivityOptional.get().getUuid(), request.getLike()).isPresent()),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/reports/posts", method = RequestMethod.POST,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> reportAbusePost(Account account,
            @RequestBody @Valid final ReportPostRequest request) {
        //Check existing post id of user with status is 'published' and verb is 'post'
        final String postId = request.getPostId();

        final Optional<ActivityEntity> existingActivityOptional =
                postService.getPublishedActivityById(postId);
        if (!existingActivityOptional.isPresent()) {
            LOG.debug(NOT_FOUND_POST_ERR, postId);
            return new ResponseEntity<>(
                    ErrorCode.INVALID_POST, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(
                Collections.singletonMap(SUCCESS,
                        postService.reportAbusePost(account, postId, request.getReason())),
                HttpStatus.OK);
    }

    private boolean isInvalidPostRequest(final String content, final PostContentType contentType) {
        return PostContentType.text.equals(contentType)
                && StringUtils.isBlank(content);
    }

    /**
     * Get shared link of post.
     *
     * @param postId post uuid
     * @return shared link
     * @see SocialProperties, SocialConstant
     */
    private String getSharedLinkOfPostId(final String postId) {
        return socialProperties.getWebPortal() + SocialConstant.POST_DETAIL_PAGE + postId;
    }

}
