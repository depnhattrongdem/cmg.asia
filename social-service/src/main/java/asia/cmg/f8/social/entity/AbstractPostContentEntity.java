package asia.cmg.f8.social.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

/**
 * Created on 12/21/16.
 */
@Value.Immutable
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@JsonSerialize(as = PostContentEntity.class)
@JsonDeserialize(builder = PostContentEntity.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface AbstractPostContentEntity {
    @JsonProperty("text")
    @Nullable
    String getText();

    @JsonProperty("content_type")
    @Nullable
    String getContentType();

    @JsonProperty("link")
    @Nullable
    String getLink();

    @JsonProperty("show_open_graph")
    @Nullable
    Boolean isShowOpenGraph();

    @JsonProperty("thumbnail_image_link")
    @Nullable
    String getThumbnailImageLink();

    @JsonProperty("thumbnail_link")
    @Nullable
    String getThumbnailLink();

    @JsonProperty("open_graph_image")
    @Nullable
    String getOpenGraphImage();

    @JsonProperty("open_graph_description")
    @Nullable
    String getOpenGraphDescription();

    @JsonProperty("open_graph_title")
    @Nullable
    String getOpenGraphTitle();

}
