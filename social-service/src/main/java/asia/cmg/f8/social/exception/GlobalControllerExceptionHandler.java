package asia.cmg.f8.social.exception;

import asia.cmg.f8.common.web.errorcode.ErrorCode;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

/**
 * Created by on 10/20/16.
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    private static final String ERROR_MESSAGE = "Exception: {}";

    private static final Logger LOG =
            LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    /**
     * Catch constraint exception.
     *
     * @param exception Exception
     * @return Error Code with detail is List of field name that is invalid
     */
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorCode socialRequestConstraintViolationException(
            final MethodArgumentNotValidException exception) {
        LOG.error(ERROR_MESSAGE, exception.getMessage());
        return ErrorCode.REQUEST_INVALID.withDetail(exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(FieldError::getField)
                .distinct()
                .collect(Collectors.joining(", ")));
    }

    @ExceptionHandler(value = {InvalidFormatException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorCode invalidFormatException(
            final InvalidFormatException exception) {
        LOG.error(ERROR_MESSAGE, exception.getMessage());
        return ErrorCode.REQUEST_INVALID
                .withDetail(exception.getPath()
                        .stream()
                        .map(JsonMappingException.Reference::getFieldName)
                        .distinct()
                        .collect(Collectors.joining(", ")));
    }

}
