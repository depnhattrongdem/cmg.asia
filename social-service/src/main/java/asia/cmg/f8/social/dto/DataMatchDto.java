package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DataMatchDto {
    @JsonProperty(value = "question_id")
    private String questionId;

    @JsonProperty(value = "weight")
    private int weight;

    @JsonProperty(value = "options")
    private List<String> options;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }
}
