package asia.cmg.f8.social.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name = "following",
        indexes = {
                @Index(name = "IDX_FOLLOWED_UID", columnList = "followed_user_id"),
                @Index(name = "IDX_USER_ID", columnList = "user_id")},
        uniqueConstraints = @UniqueConstraint(name = "UK_UID_FOLLOWUID", columnNames = {"user_id", "followed_user_id"})
)
public class Follower {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id", length = 36, nullable = false)
    @JsonProperty("user_id")
    private String userId;

    @Column(name = "followed_user_id", length = 36, nullable = false)
    @JsonProperty("followed_user_id")
    private String followedUserId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "uuid", insertable = false, updatable = false,
            foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @Fetch(FetchMode.JOIN)
    private FollowUserEntity followingUserEntity;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "followed_user_id", referencedColumnName = "uuid", insertable = false, updatable = false,
            foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @Fetch(FetchMode.JOIN)
    private FollowUserEntity followerUserEntity;

    protected Follower() {
    }

    public Follower(String userId, String followedUserId) {
        this.userId = userId;
        this.followedUserId = followedUserId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFollowedUserId() {
        return followedUserId;
    }

    public void setFollowedUserId(String followedUserId) {
        this.followedUserId = followedUserId;
    }

    public FollowUserEntity getFollowingUserEntity() {
        return followingUserEntity;
    }

    public void setFollowingUserEntity(FollowUserEntity followingUserEntity) {
        this.followingUserEntity = followingUserEntity;
    }

    public FollowUserEntity getFollowerUserEntity() {
        return followerUserEntity;
    }

    public void setFollowerUserEntity(FollowUserEntity followerUserEntity) {
        this.followerUserEntity = followerUserEntity;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("userId", userId)
                .append("followedUserId", followedUserId)
                .toString();
    }
}