package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.dto.Counter;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import rx.Observable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Created on 1/19/17.
 */
@FeignClient(value = "counters", url = "${counter.url}", fallback = CounterClientFallbackImpl.class)
public interface CounterClient {

    String COUNTERS = "counters";
    String COUNTER_NAME = "counterName";

    @RequestMapping(method = GET, path = "/counters", produces = APPLICATION_JSON_VALUE)
    Observable<List<Counter>> getCounters(@RequestParam(COUNTERS) Set<String> counterNames);

    @RequestMapping(method = PUT, path = "/counters/{counterName}/increase")
    UserGridResponse<Map<String, Object>> increaseCounter(@PathVariable(COUNTER_NAME) final String name);

    @RequestMapping(method = PUT, path = "/counters/{counterName}/decrease")
    UserGridResponse<Map<String, Object>> decreaseCounter(@PathVariable(COUNTER_NAME) final String name);
}
