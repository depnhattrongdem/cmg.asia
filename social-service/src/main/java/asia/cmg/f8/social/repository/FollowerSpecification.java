package asia.cmg.f8.social.repository;

import asia.cmg.f8.social.entity.FollowUserEntity;
import asia.cmg.f8.social.entity.Follower;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class FollowerSpecification implements Specification<Follower> {
    private String userId;
    private String followedUserId;
    private String userType;
    private String name;

    public FollowerSpecification(String userId, String followedUserId, String userType, String name) {
        this.userId = userId;
        this.followedUserId = followedUserId;
        this.userType = userType;
        this.name = name;
    }

    @Override
    public Predicate toPredicate(Root<Follower> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Predicate predicate = cb.conjunction();

        if (StringUtils.isNotEmpty(userId)) {
            predicate.getExpressions().add(cb.equal(root.get("userId"), userId));
        }
        if (StringUtils.isNotEmpty(followedUserId)) {
            predicate.getExpressions().add(cb.equal(root.get("followedUserId"), followedUserId));
        }

        if (StringUtils.isNotEmpty(userType) || StringUtils.isNotEmpty(name)) {
            Path<FollowUserEntity> path = StringUtils.isNotEmpty(userId)
                    ? root.get("followerUserEntity")
                    : root.get("followingUserEntity");

            if (StringUtils.isNotEmpty(userType)) {
                predicate.getExpressions().add(cb.equal(path.get("userType"), userType));
            }

            if (StringUtils.isNotEmpty(name)) {
                Predicate byName = cb.disjunction();
                String nameSearch = "%" + name + "%";
                byName.getExpressions().add(cb.like(path.get("fullName"), nameSearch));
                byName.getExpressions().add(cb.like(path.get("userName"), nameSearch));
                predicate.getExpressions().add(byName);
            }
        }

        return predicate;
    }
}
