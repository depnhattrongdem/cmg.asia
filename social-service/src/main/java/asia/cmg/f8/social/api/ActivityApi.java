package asia.cmg.f8.social.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import asia.cmg.f8.social.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import asia.cmg.f8.common.dto.QuestionDTO;
import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.spec.user.UserType;
import asia.cmg.f8.social.config.SocialProperties;
import asia.cmg.f8.social.dto.FollowUserDto;
import asia.cmg.f8.social.dto.Post;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.util.SocialConstant;
import asia.cmg.f8.social.util.SocialConverterUtil;
import rx.Observable;

/**
 * Created on 12/26/16.
 */
@RestController
public class ActivityApi {

    private static final Logger LOG = LoggerFactory.getLogger(ActivityApi.class);

    private final SocialPostService postService;

    private final UserService userService;

    private final CounterService counterService;
    
    private final SocialProperties socialProperties;
    
    private final FollowUserService followUserService;

    private final PTIMatchingService ptiMatchingService;
    
    private final UserPtiMatchService userPtiMatchService;

    public ActivityApi(final CounterService counterService,
                       final UserService userService,
                       final SocialPostService postService,
                       final SocialProperties socialProperties,
                       final FollowUserService followUserService,
                       final PTIMatchingService ptiMatchingService,
                       final UserPtiMatchService userPtiMatchService) {
        this.counterService = counterService;
        this.userService = userService;
        this.postService = postService;
        this.socialProperties = socialProperties;
        this.followUserService = followUserService;
        this.ptiMatchingService = ptiMatchingService;
        this.userPtiMatchService = userPtiMatchService;
    }

    @RequestMapping(value = "/social/users/{user_id}/posts", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<PagedResponse<Post>> getListPostOfUserId(
            final Account account,
            @PathVariable("user_id") final String userId,
            @RequestParam(value = "cursor", required = false) final String cursor,
            @RequestParam(value = "limit", required = false) Integer limit) {
    	
    	if (limit == null) {
    		limit = socialProperties.getSocialPageSize();
    	}
        final PagedResponse<ActivityEntity> response = postService.getActivityByUserId(userId, cursor, limit, account);
        if (Objects.isNull(response) || response.isEmpty()) {
            return Observable.just(new PagedResponse<>());
        }
        return populateDataActivity(account, response);
    }

    @RequestMapping(value = "/social/users/me/posts", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<PagedResponse<Post>> getListPostOfCurrentUser(
            final Account account,
            @RequestParam(value = "cursor", required = false) final String cursor,
            @RequestParam(value = "limit", required = false) Integer limit) {
    	
    	if (limit == null) {
    		limit = socialProperties.getSocialPageSize();
    	}
        final String userId = account.uuid();
        final PagedResponse<ActivityEntity> response = postService.getActivityByUserId(userId, cursor, limit, account);
        if (Objects.isNull(response) || response.isEmpty()) {
            return Observable.just(new PagedResponse<>());
        }
        return populateDataActivity(account, response);
    }

//    @RequestMapping(value = "/social/users/feed", method = RequestMethod.GET,
//    		produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<PagedResponse<Post>> loadFeed(
            final Account account,
            @RequestParam(value = "cursor", required = false) final String cursor,
            @RequestParam(value = "limit", required = false) Integer limit) {
    	
    	if (limit == null) {
    		limit = socialProperties.getSocialPageSize();
    	}
    	
        final String userId = account.uuid();
        final PagedResponse<ActivityEntity> pagedActivityResp = postService.getFeedByUserId(userId, cursor, limit, account);
        if (Objects.isNull(pagedActivityResp) ||  pagedActivityResp.isEmpty()) {
            return Observable.just(new PagedResponse<>());
        }
        return populateDataActivity(account, pagedActivityResp);
    }
    
    @RequestMapping(value = "/social/users/feed", method = RequestMethod.GET,
    		produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<PagedResponse<Post>> loadLastestFeed(final Account account,
    														@RequestParam(value = "cursor", required = false) final String cursor,
    														@RequestParam(value = "limit", required = false) Integer limit) {
    	if(limit == null) {
    		limit = socialProperties.getSocialPageSize();
    	}
    	
    	final PagedResponse<ActivityEntity> pagedActivityResp = postService.getLatestFeed(cursor, limit, account);
    	
    	if (Objects.isNull(pagedActivityResp) ||  pagedActivityResp.isEmpty()) {
            return Observable.just(new PagedResponse<>());
        }
    	
    	return populateDataActivity(account, pagedActivityResp);
    }

    /**
     * Populate data for List Activities including:
     * isLiked, numberOfLikes, numberOfComment, picture.
     *
     * @param account           the current account
     * @param pagedActivityResp paged activity response
     * @return List of Posts
     */
    private Observable<PagedResponse<Post>> populateDataActivity(
            final Account account,
            final PagedResponse<ActivityEntity> pagedActivityResp) {

        final Set<String> listActorIds = new HashSet<>();
        final Set<String> listPostUuid = new HashSet<>();
        final Set<String> listPostRequestUuid = new HashSet<>();

        for (final ActivityEntity activity : pagedActivityResp.getEntities()) {
            listActorIds.add(activity.getActor().getId());
            listPostUuid.add(activity.getUuid());
            final String postRequestUuid = activity.getRequestUuid();
            if (postRequestUuid != null) {
                listPostRequestUuid.add(postRequestUuid);
            }
        }
     
        Map<String, QuestionDTO> questionsMap = ptiMatchingService.getQuestionsMap();
        
        return Observable.zip(
                userService.getBasicUserInfoByUUIDs(listActorIds),
                postService.getUserLikedPostStatusByPostId(account.uuid(), listPostUuid),
                counterService.getNumberOfLikesCommentOfPost(listPostUuid, listPostRequestUuid),
                (mapUsers, listLikedPost, likeAndCommentCounter) -> {
                    final PagedResponse<Post> result = new PagedResponse<>();
                    result.setCursor(pagedActivityResp.getCursor());
                    result.setEntities(pagedActivityResp.getEntities()
                            .stream()
                            .map(entity -> {
                                final String postId = entity.getUuid();
                                final String requestUuid = entity.getRequestUuid();
                                final String actorUuid = entity.getActor().getId();
                                final UserEntity basicUserInfo = mapUsers.get(actorUuid);
                                
                                return SocialConverterUtil.convertActivityToPost(
                                        entity,
                                        likeAndCommentCounter.getOrDefault(SocialConstant.getLikeOfPostCounterName(postId), 0),
                                        likeAndCommentCounter.getOrDefault(SocialConstant.getCommentOfPostCounterName(requestUuid), 0),
                                        likeAndCommentCounter.getOrDefault(SocialConstant.getViewPostCouterName(postId), 0),
                                        basicUserInfo == null ? null : basicUserInfo.withUserType(entity.getActor().getUserType()),
                                        listLikedPost.contains(postId),
                                        socialProperties.getWebPortal()
                                                + SocialConstant.POST_DETAIL_PAGE + postId,
                                        entity.getTaggedUuids(),
                                		entity.getActor().getUserType() == UserType.PT ? ptiMatchingService.getDataMatchs(entity.getActor().getId(), questionsMap) : Collections.emptyList(),
                                		entity.getActor().getUserType() == UserType.PT ? userPtiMatchService.getByEuUuidAndPtUuid(account.uuid(), actorUuid) : null);
                            })
                            .collect(Collectors.toList()));
                    return result;
                })
                .doOnError(error -> LOG.error("Populating Activity data occurs Error {}", error))
                .firstOrDefault(new PagedResponse<>());
    }
    
	@RequestMapping(value = "/social/users/following/search", method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
	public PagedResponse<FollowUserDto> searchFollowingUser(final Account account,
			@RequestParam(value = "keyword", required = false) final String keyword,
			@RequestParam(value = "limit", required = false) Integer limit) {

		if (limit == null) {
			limit = socialProperties.getSocialPageSize();
		}

		final List<FollowUserDto> result = followUserService.searchFollowingUser(keyword, account.uuid(), limit);
		final PagedResponse<FollowUserDto> pagedResponse = new PagedResponse<>();
		pagedResponse.setCount(result.size());
		pagedResponse.setEntities(result);
		return pagedResponse;
	}
}
