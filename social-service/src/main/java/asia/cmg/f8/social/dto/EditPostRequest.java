package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

import org.hibernate.validator.constraints.Length;

/**
 * Created on 12/26/16.
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonSerialize(as = EditPostRequest.class)
public class EditPostRequest {
    @JsonProperty("content")
    @Length(max = 60000)
    private String content;

    @JsonProperty("show_open_graph")
    private boolean showOpenGraph;

    @JsonProperty("open_graph_detail")
    private OpenGraphDetailRequest openGraphDetail;
    
	@JsonProperty("tagged_uuids")
	private List<String> taggedUuids;

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public boolean isShowOpenGraph() {
        return showOpenGraph;
    }

    public void setShowOpenGraph(final boolean showOpenGraph) {
        this.showOpenGraph = showOpenGraph;
    }

    public OpenGraphDetailRequest getOpenGraphDetail() {
        return openGraphDetail;
    }

    public void setOpenGraphDetail(final OpenGraphDetailRequest openGraphDetail) {
        this.openGraphDetail = openGraphDetail;
    }

	public List<String> getTaggedUuids() {
		return taggedUuids;
	}

	public void setTaggedUuids(List<String> taggedUuids) {
		this.taggedUuids = taggedUuids;
	}
    
}
