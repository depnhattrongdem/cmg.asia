package asia.cmg.f8.social.entity;

/**
 * Created on 12/29/16.
 */
public enum PostContentType {
    text,
    video,
    image
}
