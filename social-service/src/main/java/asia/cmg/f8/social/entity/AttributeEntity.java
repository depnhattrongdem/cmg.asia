package asia.cmg.f8.social.entity;

/**
 * Created on 12/14/16.
 */
public class AttributeEntity {
    private String category;
    private String key;
    private String language;
    private String value;

    public String getCategory() {
        return category;
    }

    public String getKey() {
        return key;
    }

    public String getLanguage() {
        return language;
    }

    public String getValue() {
        return value;
    }
}
