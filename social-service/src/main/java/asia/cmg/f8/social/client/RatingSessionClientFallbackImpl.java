package asia.cmg.f8.social.client;


import asia.cmg.f8.common.exception.UserGridException;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.AttributeEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.RatingSessionEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Created on 12/7/16.
 */
@Component
public class RatingSessionClientFallbackImpl implements RatingSessionClient {

    @Override
    public PagedResponse<RatingSessionEntity> searchRatingOfUserWithCursor(
            @RequestParam("query") final String query,
            @RequestParam("cursor") final String cursor,
            @RequestParam("limit") final int limit) {
        throw new UserGridException("ERROR_ON_SEARCH_RATING",
                "Usergrid went wrong while executing query: " + query);
    }

    @Override
    public PagedResponse<RatingSessionEntity> searchPagingRatingSessionOfUser(
            @RequestParam("query") final String query,
            @RequestParam("limit") final int limit) {
        throw new UserGridException("ERROR_ON_SEARCH_RATING",
                "Usergrid went wrong while executing query: " + query);
    }

    @Override
    public UserGridResponse<RatingSessionEntity> rateSessionUser(
            @RequestBody final RatingSessionEntity entity) {
        return null;
    }

    @Override
    public UserGridResponse<RatingSessionEntity> updateReasonRatingSession(
            @PathVariable("rating_id") final String ratingSessionUserId,
            @RequestBody final Map<String, List<String>> reasons) {
        return null;
    }

    @Override
    public UserGridResponse<AttributeEntity> getRatingReason(
            @PathVariable(value = "query") final String query) {
        throw new UserGridException("ERROR_ON_GET_RATING_REASON",
                "Usergrid went wrong while executing query: " + query);
    }

	@Override
	public PagedResponse<RatingSessionEntity> getRatingSessions(final String ptUuid, final String session_uuid) {
		return null;
	}
}
