package asia.cmg.f8.social.service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.CommentPostEvent;
import asia.cmg.f8.social.LikeCommentEvent;
import asia.cmg.f8.social.client.SocialCommentClient;
import asia.cmg.f8.social.dto.CreateCommentRequest;
import asia.cmg.f8.social.dto.LikeCommentRequest;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.CommentEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.entity.UserEntity;
import asia.cmg.f8.social.event.EventHandler;
import asia.cmg.f8.social.util.SocialConstant;
import asia.cmg.f8.social.util.SocialConverterUtil;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created on 12/22/16.
 */
@Service
public class SocialCommentService {
    private static final Logger LOG = LoggerFactory.getLogger(SocialCommentService.class);
    private final SocialCommentClient socialCommentClient;
    private final EventHandler eventHandler;
    private static final String GET_LIKE_COMMENT_STATUS_QUERY = "select uuid where %s";
    private static final String GET_COMMENT_BY_POST_QUERY = "select * where post_id = %s order by created asc";


    public SocialCommentService(final SocialCommentClient socialCommentClient,
                                final EventHandler eventHandler) {
        this.socialCommentClient = socialCommentClient;
        this.eventHandler = eventHandler;
    }

    public Optional<CommentEntity> createComment(final Account account,
                                                 final CreateCommentRequest request,
                                                 final UserEntity userEntity,
                                                 final ActivityEntity post) {
        final UserGridResponse<CommentEntity> createdCommentResp =
                socialCommentClient.createComment(SocialConverterUtil
                                .convertCommentToCommentEntity(request, userEntity));
        if (Objects.isNull(createdCommentResp)
                || createdCommentResp.getEntities().isEmpty()) {
            return Optional.empty();
        }
        
		final List<String> taggedUuids = request.getTaggedUuids() != null ? request.getTaggedUuids()
				: new ArrayList<String>();

        final Optional<CommentEntity> createdCommentOptional =
                createdCommentResp.getEntities().stream().findFirst();
        //Fire event increase counter comment
        final CommentPostEvent createCommentPostEvent = new CommentPostEvent();
        createCommentPostEvent.setCountValue(SocialConstant.INCREASE);
        createCommentPostEvent.setEventId(UUID.randomUUID().toString());
        createCommentPostEvent.setSubmittedAt(System.currentTimeMillis());
        createCommentPostEvent.setPostId(createdCommentOptional.get().getPostId());
        createCommentPostEvent.setPostType(String.valueOf(post.getContentType()));
        createCommentPostEvent.setPostOwnerUuid(post.getActor().getId());
        createCommentPostEvent.setUsername(userEntity.getUsername());
        createCommentPostEvent.setUserUuid(userEntity.getUuid());
        createCommentPostEvent.setName(userEntity.getName());
        createCommentPostEvent.setTaggedUuids(taggedUuids.stream().collect(Collectors.toList()));
        eventHandler.publish(createCommentPostEvent);
        LOG.debug("Fired increase counter comment post event");
        return createdCommentOptional;
    }

    public boolean userLikeComment(final Account account,
                                   final LikeCommentRequest request,
                                   final UserEntity userLikedComment,
                                   final CommentEntity currentComment,
                                   final ActivityEntity currentPost) {

        //check user already liked comment
        if (checkUserLikedComment(request.getCommentId(), account)) {
            return false;
        }

        final String userId = account.uuid();
        final UserGridResponse<CommentEntity> commentResponse = socialCommentClient
                .likeComment(userId, request.getCommentId());
        if (Objects.isNull(commentResponse)
                || commentResponse.getEntities().isEmpty()) {
            LOG.info("Something went wrong while like comment of user uuid: {}", userId);
            return false;
        }
        //handles counter like comment event
        final LikeCommentEvent likeCommentEvent = new LikeCommentEvent();
        likeCommentEvent.setCommentId(request.getCommentId());
        likeCommentEvent.setEventId(UUID.randomUUID().toString());
        likeCommentEvent.setSubmittedAt(System.currentTimeMillis());
        likeCommentEvent.setCountValue(SocialConstant.INCREASE);
        likeCommentEvent.setUserUuid(userLikedComment.getUuid());
        likeCommentEvent.setUsername(userLikedComment.getUsername());
        likeCommentEvent.setCommentOwnerUuid(currentComment.getActor().getId());
        likeCommentEvent.setPostOwnerName(currentPost.getActor().getUsername());
        likeCommentEvent.setPostUuid(currentPost.getRequestUuid());
        likeCommentEvent.setPostOwnerUuid(currentPost.getActor().getId());
        eventHandler.publish(likeCommentEvent);
        LOG.debug("Fired event update counter {}",
                SocialConstant
                        .getLikeCommentCounterName(request.getCommentId()));
        return true;
    }

    public boolean userUnLikeComment(final Account account, final LikeCommentRequest request) {

        //check user already liked comment
        if (!checkUserLikedComment(request.getCommentId(), account)) {
            return false;
        }

        final String userId = account.uuid();
        final UserGridResponse<CommentEntity> userUnlikeResponse = socialCommentClient
                .unLikeComment(userId, request.getCommentId());
        if (Objects.isNull(userUnlikeResponse) || userUnlikeResponse.getEntities().isEmpty()) {
            LOG.info("Something went wrong while unlike comment of user uuid: {}", userId);
            return false;
        }
        //handles counter like comment event
        final LikeCommentEvent likeCommentEvent = new LikeCommentEvent();
        likeCommentEvent.setCommentId(request.getCommentId());
        likeCommentEvent.setEventId(UUID.randomUUID().toString());
        likeCommentEvent.setSubmittedAt(System.currentTimeMillis());
        likeCommentEvent.setCountValue(SocialConstant.DECREASE);
        eventHandler.publish(likeCommentEvent);
        LOG.debug("Fired event update counter {}",
                SocialConstant
                        .getLikeCommentCounterName(request.getCommentId()));
        return true;
    }

    public PagedResponse<CommentEntity> getCommentsByPostId(final String postId,
                                                            final String cursor,
                                                            final int limit,
                                                            final Account account) {
        final PagedResponse<CommentEntity> commentResponse = socialCommentClient
                .getCommentByPostId(
                        String.format(GET_COMMENT_BY_POST_QUERY, postId),
                        StringUtils.isNotEmpty(cursor) ? cursor : null,
                        limit);
        if (Objects.isNull(commentResponse) || commentResponse.isEmpty()) {
            LOG.error("user-grid went wrong while retrieving comment of post {}", postId);
            return new PagedResponse<>();
        }
        return commentResponse;
    }

    public Observable<List<String>> filterLikedCommentByCurrentUser(
            final String userId,
            final Collection<String> listCommentIds,
            final Account account) {
        final String query = listCommentIds.stream()
                .map(commentId -> "uuid ='" + commentId + "'")
                .collect(Collectors.joining(" or "));

        final Observable<UserGridResponse<CommentEntity>> userGridResponseObservable =
                socialCommentClient.getUserLikedCommentByQuery(userId,
                        String.format(GET_LIKE_COMMENT_STATUS_QUERY, query));

        return userGridResponseObservable
                .map(commentEntityUserGridResponse -> commentEntityUserGridResponse
                        .getEntities()
                        .stream()
                        .map(CommentEntity::getId)
                        .collect(Collectors.toList()))
                .firstOrDefault(Collections.emptyList());
    }

    public boolean deleteCommentByCommentId(final String commentId,
                                            final Account account) {
        final UserGridResponse<CommentEntity> deletedCommentResp =
                socialCommentClient.deleteComment(commentId);
        if (!deletedCommentResp.getEntities().stream().findFirst().isPresent()) {
            LOG.error("Something error occur while deleting comment id {}", commentId);
            return Boolean.FALSE;
        }

        //Fire event decrease counter comment
        final CommentPostEvent deleteCommentPostEvent = new CommentPostEvent();
        deleteCommentPostEvent.setCountValue(SocialConstant.DECREASE);
        deleteCommentPostEvent.setEventId(UUID.randomUUID().toString());
        deleteCommentPostEvent.setSubmittedAt(System.currentTimeMillis());
        final CommentEntity commenEntity =  deletedCommentResp.getEntities().get(0);
        deleteCommentPostEvent.setPostId(commenEntity.getPostId());
        deleteCommentPostEvent.setUsername(commenEntity.getActor().getUsername());
        deleteCommentPostEvent.setName(StringUtils.EMPTY);
        deleteCommentPostEvent.setPostOwnerUuid(commenEntity.getActor().getId());
        deleteCommentPostEvent.setPostType(StringUtils.EMPTY);
        deleteCommentPostEvent.setUserUuid(commenEntity.getActor().getId());
        deleteCommentPostEvent.setTaggedUuids(Collections.emptyList());
        eventHandler.publish(deleteCommentPostEvent);
        LOG.debug("Fired decrease counter comment event");
        return Boolean.TRUE;
    }

    public Optional<CommentEntity> getCommentById(final String commentId, final Account account) {
        final UserGridResponse<CommentEntity> commentResp =
                socialCommentClient.getCommentById(commentId);
        if (Objects.isNull(commentResp) || commentResp.getEntities().isEmpty()) {
            return Optional.empty();
        }
        return commentResp.getEntities().stream().findFirst();
    }

    private boolean checkUserLikedComment(final String commentId, final Account account) {
        final String query = "uuid = '" + commentId + "'";

        final UserGridResponse<CommentEntity> likedCommentResp =
                socialCommentClient.getLikeCommentByCommentId(
                        account.uuid(),
                        String.format(GET_LIKE_COMMENT_STATUS_QUERY, query));
        return !Objects.isNull(likedCommentResp) && !likedCommentResp.getEntities().isEmpty();
    }


}
