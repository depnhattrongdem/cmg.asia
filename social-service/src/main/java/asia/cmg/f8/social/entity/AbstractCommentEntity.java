package asia.cmg.f8.social.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;

import javax.annotation.Nullable;

/**
 * Created on 12/22/16.
 */
@Value.Immutable
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@JsonSerialize(as = CommentEntity.class)
@JsonDeserialize(builder = CommentEntity.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface AbstractCommentEntity {
    @JsonProperty("uuid")
    @Nullable
    String getId();

    @JsonProperty("post_id")
    @Nullable
    String getPostId();

    @JsonProperty("content")
    @Nullable
    String getContent();

    @JsonProperty("actor")
    @Nullable
    ActorEntity getActor();

    @JsonProperty("created")
    @Nullable
    Long getPublished();

    @JsonProperty("tagged_uuids")
    @Nullable
	List<String> getTaggedUuids();
}
