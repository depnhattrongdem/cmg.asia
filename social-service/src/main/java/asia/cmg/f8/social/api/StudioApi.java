package asia.cmg.f8.social.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import asia.cmg.f8.common.dto.ApiRespObject;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.dto.StudioDto;
import asia.cmg.f8.social.service.StudioService;

@RestController
public class StudioApi {

	private static final Logger LOG = LoggerFactory.getLogger(StudioApi.class);
	
	@Autowired
	private StudioService studioService;
	
	@GetMapping(value = "/mobile/v1/studios", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getActiveStudios(Pageable page) {
		ApiRespObject<Object> apiResponse = new ApiRespObject<Object>();
		apiResponse.setStatus(ErrorCode.SUCCESS);
		
		try {
			Boolean isActive = true;
			List<StudioDto> studios = studioService.getStudios(isActive, page);
			
			apiResponse.setData(studios);
		} catch (Exception e) {
			apiResponse.setStatus(ErrorCode.FAILED.withDetail(e.getMessage()));
			LOG.error(e.getMessage());
		}
		
		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/mobile/v1/studios/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getStudioByUuid(@PathVariable(name="uuid", required = true) final String studio_uuid) {
		ApiRespObject<Object> apiResponse = new ApiRespObject<Object>();
		apiResponse.setStatus(ErrorCode.SUCCESS);
		
		try {
			StudioDto studio = studioService.getStudioByUuid(studio_uuid);
			
			apiResponse.setData(studio);
			
			if(studio == null) {
				apiResponse.setStatus(ErrorCode.FAILED.withDetail("The studio is not exist"));
			}
		} catch (Exception e) {
			apiResponse.setStatus(ErrorCode.FAILED.withDetail(e.getMessage()));
			LOG.error(e.getMessage());
		}
		
		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
	}
}
