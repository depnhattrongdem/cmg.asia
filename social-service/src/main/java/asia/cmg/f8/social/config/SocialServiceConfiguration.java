package asia.cmg.f8.social.config;

import asia.cmg.f8.common.context.EnableUserContext;
import org.springframework.context.annotation.Configuration;

/**
 * Social App configurations
 * Created on 12/15/16.
 */

@Configuration
@EnableUserContext
public class SocialServiceConfiguration {
    
}
