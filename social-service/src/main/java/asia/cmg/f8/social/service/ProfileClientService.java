package asia.cmg.f8.social.service;

import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import asia.cmg.f8.common.dto.ApiRespListObject;
import asia.cmg.f8.common.dto.QuestionDTO;
import asia.cmg.f8.common.web.errorcode.ErrorCode;
import asia.cmg.f8.social.client.ProfileClient;

@Service
public class ProfileClientService {

	@Autowired
	private ProfileClient profileClient;
	
	List<QuestionDTO> getQuestionByUserTypeAndLanguage(final String userType, final String language) {
		try {
			List<QuestionDTO> apiRespone = profileClient.getQuestionByUserTypeAndLanguage(userType, language);
			return apiRespone;
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}
}
