package asia.cmg.f8.social.api;

import asia.cmg.f8.social.config.SocialProperties;
import asia.cmg.f8.social.entity.ActivityEntity;
import asia.cmg.f8.social.entity.PostContentType;
import asia.cmg.f8.social.service.SocialPostService;
import asia.cmg.f8.social.util.SocialConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Created by on 12/29/16.
 */
@Controller
public class PostController {
    private static final Logger LOG = LoggerFactory.getLogger(PostController.class);

    private static final String  ARTICLE = "article";
    private static final String  MOVIE = "video.other";
    private static final String HTTPS = "https:";
    private static final String HTTP = "http:";
    private final SocialPostService postService;
    private final SocialProperties socialProperties;

    public PostController(final SocialPostService postService,
                          final SocialProperties socialProperties) {
        this.postService = postService;
        this.socialProperties = socialProperties;
    }

    @RequestMapping(value = "/posts/{post_id}", method = RequestMethod.GET)
    public ModelAndView postDetail(@PathVariable("post_id") final String postId) {

        final ModelAndView result = new ModelAndView();
        result.setViewName("postDetail");
        result.addObject("pageTitle", socialProperties.getPageTitle());
        result.addObject("webPortalUrl", socialProperties.getWebPortal());
        final String type;
        final Optional<ActivityEntity> activityResp = postService.getPublishedActivityById(postId);
        if (activityResp.isPresent()) {
            final ActivityEntity activityEntity = activityResp.get();

            final String contentType = activityEntity.getContentType().name();
            result.addObject("contentType", contentType);
            if (contentType.equals(PostContentType.image.name())) {
                result.addObject("imageLink", activityEntity.getLinks());
//                result.addObject("imageLinkWithHttp", activityEntity.getLink().replaceFirst(HTTPS, HTTP));
            } else {
                result.addObject("imageLink", null);
            }


            if (contentType.equals(PostContentType.video.name())) {
                result.addObject("videoLink", activityEntity.getLinks());
//                result.addObject("thumbnailVideo", activityEntity.getThumbnailLink());
//                result.addObject("thumbnailVideoWithHttp", activityEntity.getThumbnailLink().replaceFirst(HTTPS, HTTP));
                type = MOVIE;
            } else {
                result.addObject("videoLink", null);
                result.addObject("thumbnailVideo", null);
                type = ARTICLE;
            }
            result.addObject("type",type);
            if (contentType.equals(PostContentType.text.name())) {
                result.addObject("link", null);
            } else {
                result.addObject("link", activityEntity.getLinks());
//                result.addObject("linkWithHttp", activityEntity.getLink().replaceFirst(HTTPS, HTTP));
            }

            result.addObject("sharedLink", getSharedLinkOfPostId(postId));
            result.addObject("description", activityEntity.getText());
            result.addObject("published", convertDateLongToString(activityEntity.getPublished()));

            return result;
        }
        LOG.error("Could not found post Id: {} ", postId);
        result.addObject("message", "Opp! Page could not found");
        return result;
    }
    
    private String convertDateLongToString(final Long date) {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        final Date dateTime = new Date(date);
        return formatter.format(dateTime);
    }

    /**
     * Get shared link of post.
     *
     * @param postId post uuid
     * @return shared link
     * @see SocialProperties, SocialConstant
     */
    private String getSharedLinkOfPostId(final String postId) {
        return socialProperties.getWebPortal() + SocialConstant.POST_DETAIL_PAGE + postId;
    }
}
