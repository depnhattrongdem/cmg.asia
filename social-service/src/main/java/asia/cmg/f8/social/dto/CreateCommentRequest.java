package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Created on 12/22/16.
 */
public class CreateCommentRequest {

    @JsonProperty("post_id")
    @NotBlank
    @NotNull
    private String postId;

    @JsonProperty("content")
    @Length(min = 1, max = 60000)
    @NotBlank
    @NotNull
    private String content;
    
	@JsonProperty("tagged_uuids")
	private List<String> taggedUuids;

    public String getPostId() {
        return postId;
    }

    public void setPostId(final String postId) {
        this.postId = postId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

	public List<String> getTaggedUuids() {
		return taggedUuids;
	}

	public void setTaggedUuids(List<String> taggedUuids) {
		this.taggedUuids = taggedUuids;
	}
    
}
