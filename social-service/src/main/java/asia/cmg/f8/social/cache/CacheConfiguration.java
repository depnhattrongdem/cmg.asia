package asia.cmg.f8.social.cache;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.SimpleCacheErrorHandler;
import org.springframework.cache.interceptor.SimpleCacheResolver;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Configuration;

import com.google.common.cache.CacheBuilder;

import asia.cmg.f8.social.config.SocialProperties;

@EnableCaching
@Configuration
public class CacheConfiguration implements CachingConfigurer {

	@Inject
	private SocialProperties socialProperties;

	@Override
	public CacheManager cacheManager() {
		ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager() {

			@Override
			protected Cache createConcurrentMapCache(final String name) {
				return new RegexKeyConcurrentMapCache(name,
						CacheBuilder.newBuilder()
								.expireAfterWrite(socialProperties.getCacheInMiliSecond(), TimeUnit.MILLISECONDS)
								.maximumSize(socialProperties.getCacheMaximumSize()).build().asMap(),
						false);
			}
		};

		return cacheManager;
	}

	@Override
	public CacheResolver cacheResolver() {
		return new SimpleCacheResolver(cacheManager());
	}

	@Override
	public CacheErrorHandler errorHandler() {
		return new SimpleCacheErrorHandler();
	}

	@Override
	public KeyGenerator keyGenerator() {
		return new SimpleKeyGenerator();
	}

}