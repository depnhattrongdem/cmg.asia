package asia.cmg.f8.social.client;

import asia.cmg.f8.common.exception.UserGridException;
import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.entity.ClubEntity;
import org.springframework.stereotype.Component;

/**
 * Created on 12/6/16.
 */
@Component
public class ClubClientFallbackImpl implements ClubClient {

    @Override
    public UserGridResponse<ClubEntity> getAllClubs() {
        throw new UserGridException("ERROR_ON_LOAD_CLUBS", "Usergrid went wrong while getting all Clubs");
    }

	@Override
	public UserGridResponse<ClubEntity> createClub(ClubEntity clubEntity) {
		throw new UserGridException("ERROR_ON_CREATE_CLUB", "Usergrid went wrong while create a Club");
	}

	@Override
	public UserGridResponse<ClubEntity> updateClub(String uuid, ClubEntity clubEntity) {
		throw new UserGridException("ERROR_ON_UPDATE_CLUB", "Usergrid went wrong while update a Club");
	}

	@Override
	public UserGridResponse<ClubEntity> getOneByUuid(String uuid) {
		throw new UserGridException("ERROR_ON_LOAD_ONE_CLUB", "Usergrid went wrong while getting one Club");
	}
}
