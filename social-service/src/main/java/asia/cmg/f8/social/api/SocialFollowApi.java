package asia.cmg.f8.social.api;

import asia.cmg.f8.social.entity.FollowUserEntity;
import asia.cmg.f8.social.entity.PagedResponse;
import asia.cmg.f8.social.service.FollowService;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(value = "/social/follow", produces = APPLICATION_JSON_UTF8_VALUE)
public class SocialFollowApi {
    private final FollowService followService;

    public SocialFollowApi(final FollowService followService) {
        this.followService = followService;
    }


    @PostMapping(value = "/users/{followingUser}/following/users/{followedUser}")
    public FollowUserEntity createfollowingConnection(@PathVariable("followingUser") UUID followingUserId,
                                          @PathVariable("followedUser") UUID followedUserId) {
        return followService.createFollowConnection(followingUserId.toString(), followedUserId.toString())
                .getFollowerUserEntity();
    }

    @GetMapping(value = "/users/{followingUser}/following/users/{followedUser}")
    public Resource<Boolean> checkFollowingConnection(@PathVariable("followingUser") UUID followingUserId,
                                                      @PathVariable("followedUser") UUID followedUserId) {
        return new Resource<>(followService.checkFollowingConnection(followingUserId.toString(), followedUserId.toString()));
    }

    @GetMapping(value = "/users/{uuid}/following/users")
    public PagedResponse<String> getFollowingConnection(@PathVariable("uuid") UUID userId,
                                                 @RequestParam(value = "limit", required = false, defaultValue = "20") final int size ,
                                                 @RequestParam(value = "cursor", required = false, defaultValue = "0") final int page) {
        return toPageResponse(followService.getFollowingConnection(userId.toString(), page, size));
    }

    @GetMapping(value = "/users/{uuid}/following/users/entities")
    public PagedResponse<FollowUserEntity> getFollowingConnectionEntity(
            @PathVariable("uuid") UUID userId,
            @RequestParam(value = "limit", required = false, defaultValue = "20") int size,
            @RequestParam(value = "cursor", required = false, defaultValue = "0") int page,
            @RequestParam(value = "userType", required = false) String userType,
            @RequestParam(value = "keyword", required = false) String keyword) {
        return toPageResponse(followService.getFollowingConnectionEntity(userId.toString(), page, size, userType, keyword));
    }

    @GetMapping(value = "/users/{uuid}/followers/users")
    public PagedResponse<String> getFollowerConnection(@PathVariable("uuid") UUID userId,
                                                @RequestParam(value = "limit", required = false, defaultValue = "20") final int size ,
                                                @RequestParam(value = "cursor", required = false, defaultValue = "0") final int page) {
        return toPageResponse(followService.getFollowerConnection(userId.toString(), page, size));
    }

    @GetMapping(value = "/users/{uuid}/followers/users/entities")
    public PagedResponse<FollowUserEntity> getFollowerConnectionEntity(@PathVariable("uuid") UUID userId,
                                                                       @RequestParam(value = "limit", required = false, defaultValue = "20") final int size,
                                                                       @RequestParam(value = "cursor", required = false, defaultValue = "0") final int page,
                                                                       @RequestParam(value = "userType", required = false) String userType,
                                                                       @RequestParam(value = "keyword", required = false) String keyword) {
        return toPageResponse(followService.getFollowerConnectionEntity(userId.toString(), page, size, userType, keyword));
    }

    @GetMapping(value = "/users/{uuid}/followers/users/count")
    public Resource<Long> countFollowerConnection(@PathVariable("uuid") UUID userId) {
        return new Resource(followService.countFollowerConnection(userId.toString()));
    }

    @GetMapping(value = "/users/{uuid}/following/users/count")
    public Resource<Long> countFollowingConnection(@PathVariable("uuid") UUID userId) {
        return new Resource(followService.countFollowingConnection(userId.toString()));
    }

    @DeleteMapping("/users/{user_uuid}/following/users/{follower_uuid}")
    public Resource<Boolean> deleteFollowingConnection(@PathVariable("user_uuid") UUID userUuid,
                                                       @PathVariable("follower_uuid") UUID followerUuid) {
        return new Resource(followService.deleteFollowingConnection(userUuid.toString(), followerUuid.toString()));
    }

    private <T> PagedResponse<T> toPageResponse(Page<T> page) {
        PagedResponse pagedResponse = new PagedResponse<T>();
        pagedResponse.setCursor(page.isLast() ? null : String.valueOf(page.nextPageable().getPageNumber()));
        pagedResponse.setCount(page.getTotalPages());
        pagedResponse.setEntities(page.getContent());
        return pagedResponse;
    }
}