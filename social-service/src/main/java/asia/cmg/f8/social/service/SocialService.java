package asia.cmg.f8.social.service;

import asia.cmg.f8.social.client.AnswerClient;
import asia.cmg.f8.social.client.QuestionClient;
import asia.cmg.f8.social.config.UserProfileProperties;
import asia.cmg.f8.social.entity.AnswerEntity;
import asia.cmg.f8.social.entity.QuestionEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Collections;
import java.util.List;

@Component
public class SocialService {

    private final String USER_ANSWERED_QUERY = "select * where owner='%s' and (not deleted = true)";
    private final String GET_QUESTIONS_LANGUAGE = "select * where language='%s' and usedFor='%s' and hide=false order by sequence asc";
    private final Logger LOG = LoggerFactory.getLogger(SocialService.class);

    @Autowired
    private UserProfileProperties profileProperties;

    @Autowired
    private AnswerClient answerClient;

    @Autowired
    private QuestionClient questionClient;

    public List<AnswerEntity> getAnswersByUserUuid(String userUuid) {
    	try {
    		return answerClient.getAnswersByUser(String.format(USER_ANSWERED_QUERY, userUuid),
					profileProperties.getQuestionaire().getAnswerLimit()).getEntities();
		} catch (Exception e) {
			LOG.error("[getAnswersByUserUuid][error: {}]", e.getMessage());
			return Collections.emptyList();
		}
        
    }

    public List<QuestionEntity> getQuestions(QuestionQuery queryType) {
    	try {
    		return questionClient.getQuestions(String.format(GET_QUESTIONS_LANGUAGE, queryType.getLanguage(), queryType.getType()),
					profileProperties.getQuestionaire().getQuestionLimit()).getEntities();
		} catch (Exception e) {
			LOG.error("[getQuestions][error: {}]", e.getMessage());
			return Collections.emptyList();
		}
    }
}
