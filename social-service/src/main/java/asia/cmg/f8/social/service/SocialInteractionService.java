package asia.cmg.f8.social.service;

import asia.cmg.f8.social.LikeCommentEvent;
import asia.cmg.f8.social.client.CounterClient;
import asia.cmg.f8.social.util.SocialConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * Created on 12/23/16.
 */
@Service
public class SocialInteractionService {

    private static final Logger LOG = LoggerFactory.getLogger(SocialInteractionService.class);
    private final CounterClient counterClient;

    public SocialInteractionService(final CounterClient counterClient) {
        this.counterClient = counterClient;
    }

    public void likesPost(final String postId, final Long likedAt) {
        if (Objects.isNull(counterClient.increaseCounter(SocialConstant.getLikeOfPostCounterName(postId)))) {
            LOG.error("Could not like Post {}", postId);
            return;
        }
        LOG.debug("Liked Post {} successfully", postId);
    }

    public void unlikesPost(final String postId, final Long likedAt) {
        if (Objects.isNull(counterClient.decreaseCounter(SocialConstant.getLikeOfPostCounterName(postId)))) {
            LOG.error("Could not like Post {}", postId);
            return;
        }
        LOG.debug("Liked Post {} successfully", postId);
    }

    public void handleLikeCommentEvent(final LikeCommentEvent event) {

        final String commentId = event.getCommentId().toString();
        final String counterName = SocialConstant.getLikeCommentCounterName(commentId);

        final Integer countValue = event.getCountValue();
        if (countValue < 0) {
            counterClient.decreaseCounter(counterName);
            LOG.info("Decreased like comment for counter {} with counterValue = {}", counterName, countValue);
        } else {
            counterClient.increaseCounter(counterName);
            LOG.info("Increased like comment for counter {} with counterValue = {}", counterName, countValue);
        }
    }

    public void commentPost(final String postId, final Long commentedAt) {
        if (Objects.isNull(counterClient.increaseCounter(SocialConstant.getCommentOfPostCounterName(postId)))) {
            LOG.error("Could not increase counter comment of Post {}", postId);
            return;
        }
        LOG.debug("Increased counter comment of Post {} successfully", postId);
    }

    public void deleteCommentOfPost(final String postId, final Long deletedAt) {
        if (Objects.isNull(counterClient.decreaseCounter(SocialConstant.getCommentOfPostCounterName(postId)))) {
            LOG.error("Could not decrease counter comment of Post {}", postId);
            return;
        }
        LOG.debug("Decreased counter comment of Post {} successfully", postId);
    }
    
    public void viewPost(final String postId) {
    	if(StringUtils.isEmpty(postId)) {
    		LOG.info("The postId is null or empty");
    		return;
    	}
    	if (Objects.isNull(counterClient.increaseCounter(SocialConstant.getViewPostCouterName(postId)))) {
            LOG.error("Could not increase counter view of Post {}", postId);
            return;
        }
    	LOG.debug("Increased counter view of Post {} successfully", postId);
    }
}
