package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created on 12/23/16.
 */
public class LikeCommentRequest {

    @JsonProperty("comment_id")
    @NotBlank
    private String commentId;

    @JsonProperty("like")
    @NotNull
    private Boolean like;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(final String commentId) {
        this.commentId = commentId;
    }

    public Boolean getLike() {
        return like;
    }

    public void setLike(final Boolean like) {
        this.like = like;
    }
}
