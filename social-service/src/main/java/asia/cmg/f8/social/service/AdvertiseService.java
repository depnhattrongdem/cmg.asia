package asia.cmg.f8.social.service;

import asia.cmg.f8.social.dto.AdvertiseDto;
import asia.cmg.f8.social.entity.AdvertiseEntity;
import asia.cmg.f8.social.repository.AdvertiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


/**
 * Advertise service
 * @author vinh.nguyen.quang
 *
 */
@Service
public class AdvertiseService {

    @Autowired
    private AdvertiseRepository advertiseRepository;

    /**
     * Create new advertise.
     * @param dto advertise info
     * @return New advertise
     */
    @Transactional
    public AdvertiseDto createAdvertise(AdvertiseDto dto) {

        AdvertiseEntity entity = new AdvertiseEntity();

        entity.setUuid(UUID.randomUUID().toString());

        entity.setImage(dto.getImage());
        entity.setImageVisibility(dto.getImageVisibility());

        entity.setText(dto.getText());
        entity.setTextVisibility(dto.getTextVisibility());

        entity.setVideo(dto.getVideo());
        entity.setVideoVisibility(dto.getVideoVisibility());

        entity.setEndTime(dto.getEndTime());
        entity.setEndTimeVisibility(dto.getEndTimeVisibility());

        entity.setVisibility(dto.getVisibility());

        AdvertiseEntity advertiseEntity = advertiseRepository.save(entity);
        return convertEntity(advertiseEntity);
    }

    /**
     *
     * @param advertiseUuid advertise uuid
     * @param dto AdvertiseDto
     * @return update result
     */
    @Transactional
    public AdvertiseDto updateAdvertise(String advertiseUuid, AdvertiseDto dto) {
        Optional<AdvertiseEntity> entityOpt = advertiseRepository.findAdvertiseByUuid(advertiseUuid);
        if (!entityOpt.isPresent()) {
            throw new IllegalArgumentException("Request advertise is not found");
        }

        AdvertiseEntity entity = entityOpt.get();

        entity.setImage(dto.getImage());
        entity.setImageVisibility(dto.getImageVisibility());

        entity.setVideo(dto.getVideo());
        entity.setVideoVisibility(dto.getVideoVisibility());

        entity.setEndTime(dto.getEndTime());
        entity.setEndTimeVisibility(dto.getEndTimeVisibility());

        entity.setText(dto.getText());
        entity.setTextVisibility(dto.getTextVisibility());
        entity.setVisibility(dto.getVisibility());

        AdvertiseEntity advertiseEntity = advertiseRepository.save(entity);
        return convertEntity(advertiseEntity);
    }

    /**
     * Get all advertises.
     *
     * @return all advertises
     */
    @Transactional(readOnly = true)
    public List<AdvertiseDto> getAllAdvertises() {
        List<AdvertiseEntity> advertises = advertiseRepository.findAll();
        List<AdvertiseDto> result = new ArrayList<>();
        if (!advertises.isEmpty()) {
            for (AdvertiseEntity entity : advertises) {
                AdvertiseDto dto = convertEntity(entity);

                result.add(dto);
            }
        }
        return result;
    }

    /**
     * Get advertise by advertises uuid.
     * @param advertiseUuid advertise uuid
     * @return AdvertiseDto
     */
    @Transactional(readOnly = true)
    public AdvertiseDto getAdvertiseByUuid(String advertiseUuid) {
        Optional<AdvertiseEntity> advertiseOpt = advertiseRepository.findAdvertiseByUuid(advertiseUuid);
        if (advertiseOpt.isPresent()) {
            AdvertiseEntity entity = advertiseOpt.get();

            return convertEntity(entity);
        }
        return null;
    }

    /**
     * Convert entity to dto
     * @param entity
     * @return AdvertiseDto
     */
    private AdvertiseDto convertEntity(AdvertiseEntity entity){
        AdvertiseDto dto = new AdvertiseDto();

        dto.setEndTime(entity.getEndTime());
        dto.setEndTimeVisibility(entity.getEndTimeVisibility());

        dto.setText(entity.getText());
        dto.setTextVisibility(entity.getTextVisibility());

        dto.setVideo(entity.getVideo());
        dto.setVideoVisibility(entity.getVideoVisibility());

        dto.setImage(entity.getImage());
        dto.setImageVisibility(entity.getImageVisibility());

        dto.setUuid(entity.getUuid());
        dto.setVisibility(entity.getVisibility());

        return dto;
    }
}
