package asia.cmg.f8.social.util;

import asia.cmg.f8.social.dto.*;
import asia.cmg.f8.social.entity.*;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 12/21/16.
 */
public final class SocialConverterUtil {

    private SocialConverterUtil() {
    }

    /**
     * Convert ActivityEntity to a Post.
     *
     * @param entity           ActivityEntity
     * @param numberOfLikes    Number of Likes
     * @param numberOfComments Number of Comments
     * @param user             User with name and avatar
     * @return Post
     */
    public static Post convertActivityToPost(final ActivityEntity entity,
                                             final int numberOfLikes,
                                             final int numberOfComments,
                                             final int numberOfViews,
                                             final UserEntity user,
                                             final boolean userLiked,
                                             final String sharedLink,
                                             final List<String> taggedUuids,
                                             List<DataMatchDto> dataMatchDtos,
                                             final UserPtiMatchDTO userPtiMatchDTO) {
        final String postId = TextUtils.isEmpty(entity.getRequestUuid()) ? entity.getUuid() : entity.getRequestUuid();
       
        return Post.builder()
                .postId(postId)
                .content(entity.getText())
                .contentType(entity.getContentType())
                .isShowOpenGraph(entity.isShowOpenGraph())
                .published(entity.getPublished())
                .actor(convertActor(entity.getActor(), user))
                .numberOfComments(numberOfComments)
                .numberOfLikes(numberOfLikes)
                .isLiked(userLiked)
                .sharedLink(sharedLink)
                .thumbnailImageLink(entity.getThumbnailImageLink())
                .requestUuid(entity.getRequestUuid())
                .taggedUuids(taggedUuids)
                .dataMatchs(dataMatchDtos)
                .openGraphDetail(OpenGraphDetail
                        .builder()
                        .image(entity.getOpenGraphImage())
                        .title(entity.getOpenGraphTitle())
                        .description(entity.getOpenGraphDescription())
                        .domain(entity.getOpenGraphDomain())
                        .link(entity.getOpenGraphLink())
                        .build())
                .links(entity.getLinks())
                .userPtiMatch(userPtiMatchDTO)
                .videoDuration(entity.getVideoDuration() == null ? 0 : entity.getVideoDuration())
                .numberOfViews(numberOfViews)
                .build();
    }

    /**
     * Convert a UserEntity to Actor.
     *
     * @param entity UserEntity
     * @return Actor DTO
     */
    public static Actor convertActor(final UserEntity entity) {
        return Actor
                .builder()
                .id(entity.getUuid())
                .username(entity.getUsername())
                .name(entity.getName())
                .picture(entity.getPicture())
                .build();
    }

    /**
     * Convert a ActorEntity to Actor.
     *
     * @param entity actor entity
     * @param user   user entity
     * @return Actor DTO
     */
    public static Actor convertActor(final ActorEntity entity, final UserEntity user) {
        if (user != null) {
            return Actor
                    .builder()
                    .id(entity.getId())
                    .username(entity.getUsername())
                    .name(user.getName())
                    .picture(user.getPicture())
                    .userType(user.getUserType())
                    .level(user.getLevel())
                    .approved(user.getApproved())
                    .build();
        }

        return Actor
                .builder()
                .id(entity.getId())
                .username(entity.getUsername())
                .name(StringUtils.EMPTY)
                .picture(StringUtils.EMPTY)
                .level(StringUtils.EMPTY)
                .build();
    }

    /**
     * Convert a UserEntity to ActorEntity.
     *
     * @param entity UserEntity
     * @return Actor entity
     */
    public static ActorEntity convertActorEntity(final UserEntity entity) {
        return ActorEntity
                .builder()
                .id(entity.getUuid())
                .username(entity.getUsername())
                .userType(entity.getUserType())
                .build();
    }

    /**
     * Convert CreatePostRequest to ActivityEntity that used for persistence.
     *
     * @param request  CreatePostRequest
     * @param userInfo UserEntity
     * @return ActivityEntity
     */
    public static ActivityEntity converterPostRequestToActivityEntity(
            final CreatePostRequest request,
            final UserEntity userInfo) {

        if (Objects.isNull(request.getOpenGraphDetail())) {
            return ActivityEntity.builder()
                    .contentType(request.getContentType())
                    .isShowOpenGraph(request.isShowOpenGraph())
                    .text(request.getContent())
                    .thumbnailImageLink(request.getThumbnailImageLink())
                    .actor(convertActorEntity(userInfo))
                    .verb(ActivityVerbType.post)
                    .status(PostStatusType.published)
                    .ownerId(userInfo.getUuid())
                    .requestUuid(request.getRequestUuid())
                    .taggedUuids(request.getTaggedUuids())
                    .links(request.getLinks())
                    .videoDuration(request.getVideoDuration())
                    .build();
        }

        return ActivityEntity.builder()
                .contentType(request.getContentType())
                .isShowOpenGraph(request.isShowOpenGraph())
                .text(request.getContent())
                .thumbnailImageLink(request.getThumbnailImageLink())

                .openGraphImage(request.getOpenGraphDetail().getImage())
                .openGraphDescription(request.getOpenGraphDetail().getDescription())
                .openGraphTitle(request.getOpenGraphDetail().getTitle())
                .openGraphDomain(request.getOpenGraphDetail().getDomain())
                .openGraphLink(request.getOpenGraphDetail().getLink())

                .actor(convertActorEntity(userInfo))
                .verb(ActivityVerbType.post)
                .status(PostStatusType.published)
                .ownerId(userInfo.getUuid())
                .requestUuid(request.getRequestUuid())
                .taggedUuids(request.getTaggedUuids())
                .links(request.getLinks())
                .videoDuration(request.getVideoDuration())
                .build();
    }

    /**
     * Convert CommentEntity To Comment DTO.
     *
     * @param entity           CommentEntity
     * @param numberOfLikes    number of likes
     * @param userInfo         user information
     * @param userLikedComment user liked comment or not
     * @return Comment DTO
     */
    public static Comment convertCommentEntityToComment(final CommentEntity entity,
                                                        final int numberOfLikes,
                                                        final UserEntity userInfo,
                                                        final Boolean userLikedComment) {
        return Comment.builder()
                .actor(convertActor(userInfo))
                .commentId(entity.getId())
                .postId(entity.getPostId())
                .content(entity.getContent())
                .numberOfLike(numberOfLikes)
                .published(entity.getPublished())
                .liked(userLikedComment)
                .taggedUuids(entity.getTaggedUuids()).build();

    }

    /**
     * Convert CommentEntity To Comment DTO.
     *
     * @param entity           CommentEntity
     * @param numberOfLikes    number of likes
     * @param userLikedComment user liked comment or not
     * @param user             user with name and avatar
     * @return Comment DTO
     */
    public static Comment convertCommentEntityToComment(final CommentEntity entity,
                                                        final int numberOfLikes,
                                                        final Boolean userLikedComment,
                                                        final UserEntity user,
                                                        final List<String> taggedUuids) {
        return Comment.builder()
                .actor(convertActor(entity.getActor(), user))
                .commentId(entity.getId())
                .postId(entity.getPostId())
                .content(Optional.ofNullable(entity.getContent()).orElse(""))
                .numberOfLike(numberOfLikes)
                .published(entity.getPublished())
                .liked(userLikedComment)
                .taggedUuids(taggedUuids).build();
    }

    /**
     * Convert create comment request  CommentEntity for persistence.
     *
     * @param commentRequest request
     * @param userInfo       user information
     * @return CommentEntity
     */
    public static CommentEntity convertCommentToCommentEntity(
            final CreateCommentRequest commentRequest,
            final UserEntity userInfo) {
        return CommentEntity.builder()
                .content(commentRequest.getContent())
                .actor(convertActorEntity(userInfo))
                .postId(commentRequest.getPostId())
                .taggedUuids(commentRequest.getTaggedUuids())
                .build();
    }

}
