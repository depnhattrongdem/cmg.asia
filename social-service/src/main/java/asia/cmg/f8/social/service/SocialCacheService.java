package asia.cmg.f8.social.service;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.social.client.SocialPostClient;
import asia.cmg.f8.social.config.SocialProperties;
import asia.cmg.f8.social.entity.*;
import asia.cmg.f8.social.repository.FollowerRepository;
import asia.cmg.f8.social.repository.ReportAbuseRepository;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SocialCacheService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SocialCacheService.class);

	private static final String SOCIAL_ACTIVITY_CACHE_NAME = "ACTIVITY_FEED";

	private static final String CURRENT_USER_QUERY_POST_OF_USER = "select * where verb = '" + ActivityVerbType.post
			+ "' " + "and ((owner_id = '%1$s' and not (status = '" + PostStatusType.deleted + "')) "
			+ "       or (not (owner_id = '%1$s') and status = '" + PostStatusType.published + "')) "
			+ "order by published desc";

	private static final String USER_QUERY_PUBLISHED_ACTIVITY = "select * where verb = 'post' "
			+ "and owner_id='%1$s' and status = '" + PostStatusType.published + "' "
			+ "order by published desc";
	
	private static final String LATEST_QUERY_PUBLISHED_ACTIVITY = "select * where verb = 'post' "
			+ "and status = '" + PostStatusType.published + "' "
			+ "order by published desc";

	private static final int LOAD_MAX_ITEM = 1000;
	private int loadPageSize;

	private SocialPostClient postClient;
	
	private final FollowerRepository followerRepository;
	private final ReportAbuseRepository reportAbuseRepository;

	@Autowired
	private SocialCacheService selfProxy;


	public SocialCacheService(final SocialPostClient postClient,
							  FollowerRepository followerRepository,
							  ReportAbuseRepository reportAbuseRepository,
							  SocialProperties socialProperties) {
		this.postClient = postClient;
		this.followerRepository = followerRepository;
		this.reportAbuseRepository = reportAbuseRepository;
		this.loadPageSize = socialProperties.getSocialPageSize();
	}

	public PagedResponse<ActivityEntity> getFeedByUserId(final String userId, final String cursor, final int limit, final Account account) {
		List<String> followerIds = followerRepository.findByUserId(userId, new PageRequest(0, Integer.MAX_VALUE))
				.map(Follower::getFollowedUserId).getContent();

		Map<String, PagedResponse<ActivityEntity>> activityMap = Maps.newHashMap();
		int pageNumber = getCursor(cursor);

		for (String followerId : followerIds) {
			PagedResponse<ActivityEntity> activities = findAllActivityByUserId(followerId, (pageNumber + 1) * limit, true);
			activityMap.put(followerId, activities);
		}
		activityMap.put(account.uuid(), findAllActivityByUserId(account.uuid(), (pageNumber + 1) * limit, false));

		PagedResponse<ActivityEntity> activities = mergePagedActivityEntities(activityMap, pageNumber, limit);
		return filterReportPost(account.uuid(), activities);
	}
	
	public PagedResponse<ActivityEntity> getLatestFeed(final String cursor, final int limit, final Account account) {
		Map<String, PagedResponse<ActivityEntity>> activityMap = Maps.newHashMap();
		int pageNumber = getCursor(cursor);
		
		activityMap.put(account.uuid(), findLatestActivity(account.uuid(), (pageNumber + 1) * limit));
		PagedResponse<ActivityEntity> activities = mergePagedActivityEntities(activityMap, pageNumber, limit);
		return filterReportPost(account.uuid(), activities);
	}
	
    public PagedResponse<ActivityEntity> getActivityByUserId(final String userId, final String cursor, final int limit, final Account account) {
		int pageNumber = getCursor(cursor);
		boolean publishedPostOnly = !StringUtils.equals(account.uuid(), userId);
		PagedResponse<ActivityEntity> result = findAllActivityByUserId(userId, (pageNumber + 1) * limit, publishedPostOnly);

		PagedResponse<ActivityEntity> activities = mergePagedActivityEntities(ImmutableMap.of(userId, result), pageNumber, limit);
		return filterReportPost(account.uuid(), activities);
	}

	private PagedResponse<ActivityEntity> findAllActivityByUserId(String userId, int numberOfItem, boolean publishedOnly) {
		List<ActivityEntity> result = Lists.newArrayList();
		String lastCursor = null;
        for (int i = 0; i < LOAD_MAX_ITEM / loadPageSize; i++) {
			PagedResponse<ActivityEntity> partialResult = selfProxy.fetchPagedActivityByUserId(userId, lastCursor, loadPageSize, publishedOnly);
			result.addAll(partialResult.getEntities());
			lastCursor = partialResult.getCursor();
			if (partialResult.getCursor() == null || result.size() >= numberOfItem) {
				break;
			}
		}

		return toPagedResponse(result, lastCursor);
	}
	
	private PagedResponse<ActivityEntity> findLatestActivity(String userId, int numberOfItem) {
		List<ActivityEntity> result = Lists.newArrayList();
		String lastCursor = null;
        for (int i = 0; i < LOAD_MAX_ITEM / loadPageSize; i++) {
			PagedResponse<ActivityEntity> partialResult = selfProxy.fetchPagedLatestActivities(userId, lastCursor, loadPageSize);
			result.addAll(partialResult.getEntities());
			lastCursor = partialResult.getCursor();
			if (partialResult.getCursor() == null || result.size() >= numberOfItem) {
				break;
			}
		}

		return toPagedResponse(result, lastCursor);
	}

	@Cacheable(cacheNames = SOCIAL_ACTIVITY_CACHE_NAME, key = "#userId + #cursor + #pageSize + #publishedOnly")
	public PagedResponse<ActivityEntity> fetchPagedActivityByUserId(String userId, String cursor, int pageSize, boolean publishedOnly) {
		String query = publishedOnly ? USER_QUERY_PUBLISHED_ACTIVITY : CURRENT_USER_QUERY_POST_OF_USER;
		LOGGER.info(String.format("fetch userId=%s, cursor=%s, pagesize=%s, published=%s", userId, cursor, pageSize, publishedOnly));
		PagedResponse<ActivityEntity> result = postClient.getActivityByUserId(userId,
				String.format(query, userId), cursor, pageSize);
		if (result == null) {
			return toPagedResponse(null, null);
		}
		return result;
	}
	
	@Cacheable(cacheNames = SOCIAL_ACTIVITY_CACHE_NAME, key = "#userId + #cursor + #pageSize")
	public PagedResponse<ActivityEntity> fetchPagedLatestActivities(String userId, String cursor, int pageSize) {
		String query = LATEST_QUERY_PUBLISHED_ACTIVITY;
		PagedResponse<ActivityEntity> result = postClient.getActivitiesByQuery(query, cursor, pageSize);
		if (result == null) {
			return toPagedResponse(null, null);
		}
		return result;
	}

	@CacheEvict(cacheNames = SOCIAL_ACTIVITY_CACHE_NAME, key = "'regex:'+#userId+'.*'")
	public void invalidateCacheForUser(String userId) {
		// Do nothing
	}

	private PagedResponse<ActivityEntity> filterReportPost(String userId, PagedResponse<ActivityEntity> activities) {
		if (activities == null || CollectionUtils.isEmpty(activities.getEntities())) {
			return toPagedResponse(null, null);
		}
		List<String> reportPosts = reportAbuseRepository.getUserReports(userId);
		List<ActivityEntity> activitiesWithoutReport = activities.getEntities().stream()
				.filter(activityEntity -> !reportPosts.contains(activityEntity.getRequestUuid())
						&& !reportPosts.contains(activityEntity.getUuid()))
				.collect(Collectors.toList());
		activities.setEntities(activitiesWithoutReport);
		return activities;
	}

	private int getCursor(String cursor) {
		if (StringUtils.isEmpty(cursor)) {
			return 0;
		}
		Preconditions.checkArgument(NumberUtils.isNumber(cursor), String.format("Cursor='%s' is not valid", cursor));
		return Integer.valueOf(cursor);
	}

	private static PagedResponse<ActivityEntity> mergePagedActivityEntities(Map<String, PagedResponse<ActivityEntity>> activitiesMap, int pageNumber, int pageSize) {
		List<ActivityEntity> result = activitiesMap.values().stream()
				.filter(activityEntityPagedResponse -> Objects.nonNull(activityEntityPagedResponse.getEntities()))
				.flatMap(activityEntityPagedResponse -> activityEntityPagedResponse.getEntities().stream())
				.sorted(Comparator.comparingLong(ActivityEntity::getPublished)
						.reversed())
				.skip(pageNumber * pageSize)
				.limit(pageSize)
				.collect(Collectors.toList());

		return toPagedResponse(result, result.size() < pageSize ? null : String.valueOf(++pageNumber));
	}

	private static PagedResponse<ActivityEntity> toPagedResponse(List<ActivityEntity> entities, String cursor) {
		PagedResponse<ActivityEntity> pagedResponse = new PagedResponse<>();
		pagedResponse.setEntities(entities != null ? entities : Collections.emptyList());
		pagedResponse.setCursor(cursor);
		pagedResponse.setCount(entities != null ? entities.size() : 0);
		return pagedResponse;
	}
}
