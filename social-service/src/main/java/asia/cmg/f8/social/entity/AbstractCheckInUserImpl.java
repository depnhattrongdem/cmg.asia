package asia.cmg.f8.social.entity;

import asia.cmg.f8.common.spec.view.CheckInUser;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.immutables.value.Value;

import javax.annotation.Nullable;

/**
 * Created on 12/7/16.
 */
@Value.Immutable
@JsonInclude(JsonInclude.Include.ALWAYS)
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@SuppressWarnings({"CheckReturnValue", "PMD"})
public abstract class AbstractCheckInUserImpl implements CheckInUser {
    @JsonProperty("userType")
    @Nullable
    abstract String getUserType();

    @JsonProperty("email")
    @Nullable
    abstract String getEmail();

    @JsonProperty("userRole")
    @Nullable
    abstract String getUserRole();
}
