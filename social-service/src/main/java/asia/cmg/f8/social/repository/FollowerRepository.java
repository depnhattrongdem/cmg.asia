package asia.cmg.f8.social.repository;

import asia.cmg.f8.social.entity.Follower;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FollowerRepository extends JpaSpecificationExecutor<Follower>, JpaRepository<Follower, Long> {

    Follower findByUserIdAndFollowedUserId(String userId, String followedUserId);

    Page<Follower> findByUserId(String userId, Pageable pageable);

    Page<Follower> findByFollowedUserId(String userId, Pageable pageable);

    Long countByUserId(String userId);

    Long countByFollowedUserId(String userId);

    Long removeByUserIdAndFollowedUserId(String userId, String followedUserId);

}
