package asia.cmg.f8.social.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created on 2/20/17.
 */
public interface UserRegistrationEventStream {

    String REGISTRATION_EVENT_INPUT_CHANNEL = "userRegistrationEventInput";

    @Input(REGISTRATION_EVENT_INPUT_CHANNEL)
    SubscribableChannel userRegistrationInputChannel();
}
