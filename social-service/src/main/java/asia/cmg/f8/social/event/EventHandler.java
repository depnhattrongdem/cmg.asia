package asia.cmg.f8.social.event;

import asia.cmg.f8.common.profile.ChangeUserInfoEvent;
import asia.cmg.f8.social.CommentPostEvent;
import asia.cmg.f8.social.LikeCommentEvent;
import asia.cmg.f8.social.LikePostEvent;
import asia.cmg.f8.social.RatingSessionEvent;
import asia.cmg.f8.social.SocialPostCreatedEvent;
import asia.cmg.f8.social.ViewPostEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.stereotype.Component;

//import asia.cmg.f8.social.SocialPostCreatedEvent;

/**
 * Created by 12/12/16.
 */
@Component
@EnableBinding({EventStream.class, PostEventStream.class, UserRegistrationEventStream.class})
public class EventHandler {
    @Autowired
    @Qualifier("ratingSessionEventConverter")
    private MessageConverter ratingSessionEventConverter;

    @Autowired
    @Qualifier("changeUserInfoEventConverter")
    private MessageConverter changUserInfoEventConverter;

    @Autowired
    @Qualifier("likePostEventConverter")
    private MessageConverter likePostEventConverter;

    @Autowired
    @Qualifier("commentPostEventConverter")
    private MessageConverter commentPostEventConverter;

    @Autowired
    @Qualifier("likeCommentEventConverter")
    private MessageConverter likeCommentEventConverter;

    @Autowired
    @Qualifier("userCreatedPostEventConverter")
    private MessageConverter userCreatedPostConverter;
    
    @Autowired
    @Qualifier("viewPostEventConverter")
    private MessageConverter viewPostEventConverter;

    private final EventStream eventStream;

    private final PostEventStream postEventStream;

    public EventHandler(final EventStream eventStream,
                        final PostEventStream postEventStream) {
        this.eventStream = eventStream;
        this.postEventStream = postEventStream;
    }

    public void publish(final RatingSessionEvent event) {
        eventStream.ratingSessionCompleted()
                .send(ratingSessionEventConverter.toMessage(event, null));
    }

    public void publish(final ChangeUserInfoEvent event) {
        eventStream.changeUserInfo().send(changUserInfoEventConverter.toMessage(event, null));
    }

    public void publish(final LikePostEvent event) {
        eventStream.likePost().send(likePostEventConverter.toMessage(event, null));
    }

    public void publish(final LikeCommentEvent event) {
        eventStream.likeComment().send(likeCommentEventConverter.toMessage(event, null));
    }

    public void publish(final CommentPostEvent event) {
        postEventStream.commentPost().send(commentPostEventConverter.toMessage(event, null));
    }

    public void publish(final SocialPostCreatedEvent event) {
        eventStream.createdPost().send(userCreatedPostConverter.toMessage(event, null));
    }
    
    public void publish(final ViewPostEvent event) {
		eventStream.viewPost().send(viewPostEventConverter.toMessage(event, null));
	}
}
