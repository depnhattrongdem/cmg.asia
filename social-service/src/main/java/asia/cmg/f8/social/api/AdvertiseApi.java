package asia.cmg.f8.social.api;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.common.security.annotation.RequiredAdminRole;
import asia.cmg.f8.social.dto.AdvertiseDto;
import asia.cmg.f8.social.service.AdvertiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class AdvertiseApi {

    private static final String UUID = "uuid";

    @Autowired
    private AdvertiseService advertiseService;

    /**
     * Create new Advertise
     *
     * @param dto AdvertiseDto
     * @return
     */
    @RequestMapping(value = "/social/advertises", method = RequestMethod.POST,
            consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    @RequiredAdminRole
    public ResponseEntity<AdvertiseDto> createSubscriptionType(@RequestBody @Valid AdvertiseDto dto) {
        return new ResponseEntity(advertiseService.createAdvertise(dto), HttpStatus.OK);
    }

    /**
     * Update Advertise
     * @param advertiseUuid
     * @param dto
     * @return
     */
    @RequestMapping(value = "/social/advertises/{uuid}", method = RequestMethod.PUT,
            consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    @RequiredAdminRole
    public ResponseEntity<AdvertiseDto> updateAdvertise(@PathVariable(UUID) String advertiseUuid,
                                                                      @RequestBody @Valid AdvertiseDto dto) {
        return new ResponseEntity(advertiseService.updateAdvertise(advertiseUuid, dto), HttpStatus.OK);
    }

    /**
     * Get list advertise
     *
     * @return List<AdvertiseDto>
     */
    @RequestMapping(value = "/social/advertises", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<AdvertiseDto>> findAllAdvertises(final Account account) {
        return new ResponseEntity(advertiseService.getAllAdvertises(), HttpStatus.OK);
    }

    /**
     * Get detail advertise
     *
     * @param advertiseUuid
     * @return AdvertiseDto
     */
    @RequestMapping(value = "/social/advertises/{uuid}", method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<AdvertiseDto> getSubscriptionType(final Account account, @PathVariable(UUID) String advertiseUuid) {
        return new ResponseEntity(advertiseService.getAdvertiseByUuid(advertiseUuid), HttpStatus.OK);
    }
}
