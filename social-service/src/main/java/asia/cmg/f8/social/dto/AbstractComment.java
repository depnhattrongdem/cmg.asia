package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import javax.annotation.Nullable;

import org.immutables.value.Value;

/**
 * Created on 12/22/16.
 */
@Value.Immutable
@Value.Style(
        typeImmutable = "*",
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        passAnnotations = {JsonIgnoreProperties.class, JsonInclude.class})
@SuppressWarnings("CheckReturnValue")
public interface AbstractComment {
    @JsonProperty("comment_id")
    String getCommentId();

    @JsonProperty("post_id")
    String getPostId();

    @JsonProperty("content")
    String getContent();

    @JsonProperty("actor")
    Actor getActor();

    @JsonProperty("published")
    Long getPublished();

    @JsonProperty("num_of_likes")
    Integer getNumberOfLike();

    @JsonProperty("liked")
    Boolean getLiked();
    
    @JsonProperty("tagged_uuids")
    @Nullable
	List<String> getTaggedUuids();
}
