package asia.cmg.f8.social.dto;

import asia.cmg.f8.social.entity.PostContentType;
import asia.cmg.f8.social.entity.ResourceLink;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

import org.hibernate.validator.constraints.Length;

/**
 * Created on 12/21/16.
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonSerialize(as = CreatePostRequest.class)
public class CreatePostRequest {

    @JsonProperty("content")
    @Length(max = 60000)
    private String content;

    @JsonProperty("content_type")
    private PostContentType contentType;

    @JsonProperty("link")
    private String link;
    
    @JsonProperty("links")
    private List<ResourceLink> links;

	@JsonProperty("thumbnail_image_link")
	private String thumbnailImageLink;

	@JsonProperty("thumbnail_link")
	private String thumbnailLink;
	
	@JsonProperty("show_open_graph")
	private boolean showOpenGraph;

	@JsonProperty("open_graph_detail")
	private OpenGraphDetailRequest openGraphDetail;

	@JsonProperty("request_uuid")
	private String requestUuid;
	
	@JsonProperty("tagged_uuids")
	private List<String> taggedUuids;
	
	@JsonProperty("video_duration")
	private Integer videoDuration;
	
    public Integer getVideoDuration() {
		return videoDuration;
	}

	public void setVideoDuration(Integer videoDuration) {
		this.videoDuration = videoDuration;
	}

	public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public PostContentType getContentType() {
        return contentType;
    }

    public void setContentType(final PostContentType contentType) {
        this.contentType = contentType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(final String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public boolean isShowOpenGraph() {
        return showOpenGraph;
    }

    public void setShowOpenGraph(final boolean showOpenGraph) {
        this.showOpenGraph = showOpenGraph;
    }

    public OpenGraphDetailRequest getOpenGraphDetail() {
        return openGraphDetail;
    }

    public void setOpenGraphDetail(final OpenGraphDetailRequest openGraphDetail) {
        this.openGraphDetail = openGraphDetail;
    }

    public String getThumbnailImageLink() {
        return thumbnailImageLink;
    }

    public void setThumbnailImageLink(final String thumbnailImageLink) {
        this.thumbnailImageLink = thumbnailImageLink;
    }

	public String getRequestUuid() {
		return requestUuid;
	}

	public void setRequestUuid(final String requestUuid) {
		this.requestUuid = requestUuid;
	}

	public List<String> getTaggedUuids() {
		return taggedUuids;
	}

	public void setTaggedUuids(List<String> taggedUuids) {
		this.taggedUuids = taggedUuids;
	}

	public List<ResourceLink> getLinks() {
		return links;
	}

	public void setLinks(List<ResourceLink> links) {
		this.links = links;
	}
}
