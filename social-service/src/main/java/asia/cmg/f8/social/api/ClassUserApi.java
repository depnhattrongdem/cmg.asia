package asia.cmg.f8.social.api;

import asia.cmg.f8.common.security.Account;
import asia.cmg.f8.social.entity.ClassUserEntity;
import asia.cmg.f8.social.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rx.Observable;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class ClassUserApi {
    private final UserService userService;

    public ClassUserApi(final UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(
            value = "/classes/users", method = RequestMethod.GET,
            produces = APPLICATION_JSON_UTF8_VALUE)
    public Observable<List<ClassUserEntity>> getClassUsers(final Account account) {
        return userService.getClassUsers(account);
    }
}