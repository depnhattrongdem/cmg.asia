package asia.cmg.f8.social.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdvertiseDto {
    @JsonProperty(value = "image", required = true)
    private String image;

    @JsonProperty(value = "image_visibility", required = true)
    private Boolean imageVisibility;

    @JsonProperty(value = "text", required = true)
    private String text;

    @JsonProperty(value = "text_visibility", required = true)
    private Boolean textVisibility;

    @JsonProperty(value = "video", required = true)
    private String video;

    @JsonProperty(value = "video_visibility", required = true)
    private Boolean videoVisibility;

    @JsonProperty(value = "end_time", required = true)
    private Long endTime;

    @JsonProperty(value = "end_time_visibility", required = true)
    private Boolean endTimeVisibility;

    @JsonProperty(value = "uuid")
    private String uuid;

    @JsonProperty(value = "visibility", required = true)
    private Boolean visibility;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getImageVisibility() {
        return imageVisibility;
    }

    public void setImageVisibility(Boolean imageVisibility) {
        this.imageVisibility = imageVisibility;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getTextVisibility() {
        return textVisibility;
    }

    public void setTextVisibility(Boolean textVisibility) {
        this.textVisibility = textVisibility;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Boolean getVideoVisibility() {
        return videoVisibility;
    }

    public void setVideoVisibility(Boolean videoVisibility) {
        this.videoVisibility = videoVisibility;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Boolean getEndTimeVisibility() {
        return endTimeVisibility;
    }

    public void setEndTimeVisibility(Boolean endTimeVisibility) {
        this.endTimeVisibility = endTimeVisibility;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }
}
