package asia.cmg.f8.social.client;

import asia.cmg.f8.common.util.UserGridResponse;
import asia.cmg.f8.social.dto.Counter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import rx.Observable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 1/19/17.
 */
@Component
public class CounterClientFallbackImpl implements CounterClient {

    @Override
    public Observable<List<Counter>> getCounters(
            @RequestParam(COUNTERS) final Set<String> counterNames) {
        return null;
    }

    @Override
    public UserGridResponse<Map<String, Object>> increaseCounter(
            @PathVariable(COUNTER_NAME) final String name) {
        return null;
    }

    @Override
    public UserGridResponse<Map<String, Object>> decreaseCounter(
            @PathVariable(COUNTER_NAME) final String name) {
        return null;
    }
}
