#!/bin/bash

CMD="${1:-start}"

if [[ $CMD = "init" ]]; then
  echo "Initialize Feight application..."
  docker-compose -f system-modules.yml -f initial-data-setup.yml up -d --build --force-recreate cassandra elasticsearch usergrid zookeeper kafka usergrid-portal usergrid-setup
elif [[ $CMD = "start" ]]; then
  echo "Starting Feight application..."
  docker-compose -f application-modules.yml up -d --build --force-recreate apiSpec
elif [[ $CMD = "clean" ]]; then
  echo "Removing container ..."
  docker rm -f $(docker ps -aq)
elif [[ $CMD = "stop" ]]; then
  echo "Stop development containers..."
  docker-compose -f application-modules.yml stop apiSpec
fi
