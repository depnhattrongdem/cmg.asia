#!/bin/bash

CMD="${1:-branch}"
OPTION="${2}"

if [[ $CMD = "branch" ]]; then
  git submodule foreach 'git branch'
elif [[ $CMD = "checkout" ]]; then
  git submodule foreach "git checkout ${OPTION}"
elif [[ $CMD = "pull" ]]; then  
  git submodule foreach "git pull"
elif [[ $CMD = "merge" ]]; then
  git submodule foreach "git merge ${OPTION}"
elif [[ $CMD = "status" ]]; then
  git submodule foreach "git status -s"
elif [[ $CMD = "push" ]]; then
  git submodule foreach "git push ${OPTION}"
elif [[ $CMD = "reset" ]]; then
  git submodule foreach "git reset --hard"
fi