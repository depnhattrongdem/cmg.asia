package asia.cmg.f8.schedule.jobs;

import asia.cmg.f8.schedule.client.SessionClient;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class UpcomingSessionPTPTReminderJob implements Job {
    private static final Logger LOG = LoggerFactory.getLogger(UpcomingSessionPTPTReminderJob.class);
    @Inject
    private SessionClient sessionClient;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOG.info("*** Start UpcomingSessionPTPTReminderJob *** ");
        try {
            sessionClient.remindPTUpcommingPTSessionTrigger(15);
        } catch (final Exception ex) {
            LOG.error("UpcomingSessionPTPTReminderJob has error ", ex);
        }
        LOG.info("*** End UpcomingSessionPTPTReminderJob *** ");
    }
}
