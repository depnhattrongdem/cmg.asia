package asia.cmg.f8.schedule.jobs;

import asia.cmg.f8.schedule.client.SessionClient;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class UpcomingClubAcessEUReminderJob implements Job {
    private static final Logger LOG = LoggerFactory.getLogger(UpcomingClubAcessEUReminderJob.class);
    @Inject
    private SessionClient sessionClient;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOG.info("*** Start UpcomingClubAcessEUReminderJob *** ");
        try {
            sessionClient.remindEUUpcommingClubAccessTrigger();
        } catch (final Exception ex) {
            LOG.error("UpcomingClubAcessEUReminderJob has error ", ex);
        }
        LOG.info("*** End UpcomingClubAcessEUReminderJob *** ");
    }
}
