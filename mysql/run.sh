docker rm -f mysql
docker build -t f8/mysql .
docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 f8/mysql