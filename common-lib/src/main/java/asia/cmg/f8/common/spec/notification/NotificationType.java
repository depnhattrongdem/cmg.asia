package asia.cmg.f8.common.spec.notification;

public enum NotificationType {
	LIKE_POST("post"),
	COMMENT_POST("post"),
	LIKE_COMMENT("post"),
	FOLLOW("profile"),
	CREATE_POST("post"),
	TRANSFER_SESSION("session"),
	CHANGE_SESSION_STATUS("session"),
	ORDER_SUBSCRIPTION("order"),
	ORDER_CREDIT("order"),
	APPROVE_DOC("document"),
	START_SESSION("session"),
	ORDER_COMLETED("order"),
	CHAT("chat"),
	LISA_ADMIN(""),
	PURCHASE_PACKAGE("order"),
	UPGRADE_WALLET("order"),
	EXPIRE_PACKAGE("order"),
	CLASS_STARTING_REMIND("session"),
	CLASS_EXPIRE_REMIND("session"),
	SESSION_PT_CONFIRM("session"),
	SESSION_PT_CONFIRMED("session"),
	SESSION_PT_REJECTED("session"),
	SESSION_PT_CANCELLED("session"),
	SESSION_CLIENT_CONFIRM("session"),
	SESSION_CLIENT_CONFIRMED("session"),
	SESSION_CLIENT_REJECTED("session"),
	SESSION_CLIENT_CANCELLED("session"),
	SESSION_CONFIRMED_EU("session"),
	SESSION_PT_EU_REMINDING("session"),
	SESSION_PT_PT_REMINDING("session"),
	CLUB_ACCESS_EU_REMINDING("session"),
	SESSION_PT_CHECK_IN_PT_REMINDING("session");
	
	private final String type;

	NotificationType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}
}
